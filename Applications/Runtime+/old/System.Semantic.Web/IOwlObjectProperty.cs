
using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents a OWL Node of type owl:ObjectProperty
	/// </summary>
	public interface IOwlObjectProperty : IOwlProperty
	{

	}
}