
using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents a OWL Node of type owl:DatatypeProperty
	/// </summary>
	public interface IOwlDatatypeProperty : IOwlProperty
	{

	}
}