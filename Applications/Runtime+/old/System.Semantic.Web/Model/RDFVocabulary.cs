﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;

namespace System.Web.Semantic.Model
{

    /// <summary>
    /// RDFVocabulary is an helper for handy usage of supported RDF vocabularies.
    /// </summary>
    public static class RDFVocabulary {

        #region Basic

        #region RDF
        /// <summary>
        /// RDF represents the RDF vocabulary.
        /// </summary>
        public static class RDF {

            #region Properties
            /// <summary>
            /// rdf
            /// </summary>
            public static String PREFIX {
                get { return "rdf"; }
            }

            /// <summary>
            /// xmlns:rdf
            /// </summary>
            public static String BASE_URI {
                get { return "http://www.w3.org/1999/02/22-rdf-syntax-ns#"; }
            }

            /// <summary>
            /// rdf:Bag
            /// </summary>
            public static RDFResource BAG {
                get { return new RDFResource(RDF.BASE_URI + "Bag"); }
            }

            /// <summary>
            /// rdf:Seq
            /// </summary>
            public static RDFResource SEQ  {
                get { return new RDFResource(RDF.BASE_URI + "Seq"); }
            }

            /// <summary>
            /// rdf:Alt
            /// </summary>
            public static RDFResource ALT {
                get { return new RDFResource(RDF.BASE_URI + "Alt"); }
            }

            /// <summary>
            /// rdf:Statement
            /// </summary>
            public static RDFResource STATEMENT {
                get { return new RDFResource(RDF.BASE_URI + "Statement"); }
            }

            /// <summary>
            /// rdf:Property
            /// </summary>
            public static RDFResource PROPERTY {
                get { return new RDFResource(RDF.BASE_URI + "Property"); }
            }

            /// <summary>
            /// rdf:XMLLiteral
            /// </summary>
            public static RDFResource XML_LITERAL {
                get { return new RDFResource(RDF.BASE_URI + "XMLLiteral"); }
            }

            /// <summary>
            /// rdf:List
            /// </summary>
            public static RDFResource LIST {
                get { return new RDFResource(RDF.BASE_URI + "List"); }
            }

            /// <summary>
            /// rdf:nil
            /// </summary>
            public static RDFResource NIL {
                get { return new RDFResource(RDF.BASE_URI + "nil"); }
            }

            /// <summary>
            /// rdf:li
            /// </summary>
            public static RDFResource LI {
                get { return new RDFResource(RDF.BASE_URI + "li"); }
            }

            /// <summary>
            /// rdf:subject
            /// </summary>
            public static RDFResource SUBJECT {
                get { return new RDFResource(RDF.BASE_URI + "subject"); }
            }

            /// <summary>
            /// rdf:predicate
            /// </summary>
            public static RDFResource PREDICATE {
                get { return new RDFResource(RDF.BASE_URI + "predicate"); }
            }

            /// <summary>
            /// rdf:object
            /// </summary>
            public static RDFResource OBJECT {
                get { return new RDFResource(RDF.BASE_URI + "object"); }
            }

            /// <summary>
            /// rdf:type
            /// </summary>
            public static RDFResource TYPE {
                get { return new RDFResource(RDF.BASE_URI + "type"); }
            }

            /// <summary>
            /// rdf:value
            /// </summary>
            public static RDFResource VALUE {
                get { return new RDFResource(RDF.BASE_URI + "value"); }
            }

            /// <summary>
            /// rdf:first
            /// </summary>
            public static RDFResource FIRST {
                get { return new RDFResource(RDF.BASE_URI + "first"); }
            }

            /// <summary>
            /// rdf:rest
            /// </summary>
            public static RDFResource REST {
                get { return new RDFResource(RDF.BASE_URI + "rest"); }
            }
            #endregion

        }
        #endregion

        #region RDFS
        /// <summary>
        /// RDFS represents the RDFS vocabulary.
        /// </summary>
        public static class RDFS  {

            #region Properties
            /// <summary>
            /// rdfs
            /// </summary>
            public static String PREFIX {
                get { return "rdfs"; }
            }

            /// <summary>
            /// xmlns:rdfs
            /// </summary>
            public static String BASE_URI {
                get { return "http://www.w3.org/2000/01/rdf-schema#"; }
            }

            /// <summary>
            /// rdfs:Resource
            /// </summary>
            public static RDFResource RESOURCE {
                get { return new RDFResource(RDFS.BASE_URI + "Resource"); }
            }

            /// <summary>
            /// rdfs:Class
            /// </summary>
            public static RDFResource CLASS {
                get { return new RDFResource(RDFS.BASE_URI + "Class"); }
            }

            /// <summary>
            /// rdfs:Literal
            /// </summary>
            public static RDFResource LITERAL {
                get { return new RDFResource(RDFS.BASE_URI + "Literal"); }
            }

            /// <summary>
            /// rdfs:Container
            /// </summary>
            public static RDFResource CONTAINER {
                get { return new RDFResource(RDFS.BASE_URI + "Container"); }
            }

            /// <summary>
            /// rdfs:Datatype
            /// </summary>
            public static RDFResource DATATYPE {
                get { return new RDFResource(RDFS.BASE_URI + "Datatype"); }
            }

            /// <summary>
            /// rdfs:ContainerMembershipProperty
            /// </summary>
            public static RDFResource CONTAINER_MEMBERSHIP_PROPERTY {
                get { return new RDFResource(RDFS.BASE_URI + "ContainerMembershipProperty"); }
            }

            /// <summary>
            /// rdfs:range
            /// </summary>
            public static RDFResource RANGE {
                get { return new RDFResource(RDFS.BASE_URI + "range"); }
            }

            /// <summary>
            /// rdfs:domain
            /// </summary>
            public static RDFResource DOMAIN {
                get { return new RDFResource(RDFS.BASE_URI + "domain"); }
            }

            /// <summary>
            /// rdfs:subClassOf
            /// </summary>
            public static RDFResource SUB_CLASS_OF {
                get { return new RDFResource(RDFS.BASE_URI + "subClassOf"); }
            }

            /// <summary>
            /// rdfs:subPropertyOf
            /// </summary>
            public static RDFResource SUB_PROPERTY_OF {
                get { return new RDFResource(RDFS.BASE_URI + "subPropertyOf"); }
            }

            /// <summary>
            /// rdfs:label
            /// </summary>
            public static RDFResource LABEL {
                get { return new RDFResource(RDFS.BASE_URI + "label"); }
            }

            /// <summary>
            /// rdfs:comment
            /// </summary>
            public static RDFResource COMMENT {
                get { return new RDFResource(RDFS.BASE_URI + "comment"); }
            }

            /// <summary>
            /// rdfs:member
            /// </summary>
            public static RDFResource MEMBER {
                get { return new RDFResource(RDFS.BASE_URI + "member"); }
            }

            /// <summary>
            /// rdfs:seeAlso
            /// </summary>
            public static RDFResource SEE_ALSO {
                get { return new RDFResource(RDFS.BASE_URI + "seeAlso"); }
            }

            /// <summary>
            /// rdfs:isDefinedBy
            /// </summary>
            public static RDFResource IS_DEFINED_BY {
                get { return new RDFResource(RDFS.BASE_URI + "isDefinedBy"); }
            }
            #endregion

        }
        #endregion

        #region XSD
        /// <summary>
        /// XSD represents the XSD vocabulary.
        /// </summary>
        public static class XSD {

            #region Properties
            /// <summary>
            /// xsd
            /// </summary>
            public static String PREFIX {
                get { return "xsd"; }
            }

            /// <summary>
            /// xmlns:xsd
            /// </summary>
            public static String BASE_URI {
                get { return "http://www.w3.org/2001/XMLSchema#"; }
            }

            /// <summary>
            /// xsd:string
            /// </summary>
            public static RDFResource STRING {
                get { return new RDFResource(XSD.BASE_URI + "string"); }
            }

            /// <summary>
            /// xsd:boolean
            /// </summary>
            public static RDFResource BOOLEAN {
                get { return new RDFResource(XSD.BASE_URI + "boolean"); }
            }

            /// <summary>
            /// xsd:decimal
            /// </summary>
            public static RDFResource DECIMAL {
                get { return new RDFResource(XSD.BASE_URI + "decimal"); }
            }

            /// <summary>
            /// xsd:float
            /// </summary>
            public static RDFResource FLOAT {
                get { return new RDFResource(XSD.BASE_URI + "float"); }
            }

            /// <summary>
            /// xsd:double
            /// </summary>
            public static RDFResource DOUBLE {
                get { return new RDFResource(XSD.BASE_URI + "double"); }
            }

            /// <summary>
            /// xsd:positiveInteger
            /// </summary>
            public static RDFResource POSITIVE_INTEGER {
                get { return new RDFResource(XSD.BASE_URI + "positiveInteger"); }
            }

            /// <summary>
            /// xsd:negativeInteger
            /// </summary>
            public static RDFResource NEGATIVE_INTEGER {
                get { return new RDFResource(XSD.BASE_URI + "negativeInteger"); }
            }

            /// <summary>
            /// xsd:nonPositiveInteger
            /// </summary>
            public static RDFResource NON_POSITIVE_INTEGER {
                get { return new RDFResource(XSD.BASE_URI + "nonPositiveInteger"); }
            }

            /// <summary>
            /// xsd:nonNegativeInteger
            /// </summary>
            public static RDFResource NON_NEGATIVE_INTEGER {
                get { return new RDFResource(XSD.BASE_URI + "nonNegativeInteger"); }
            }

            /// <summary>
            /// xsd:integer
            /// </summary>
            public static RDFResource INTEGER {
                get { return new RDFResource(XSD.BASE_URI + "integer"); }
            }

            /// <summary>
            /// xsd:long
            /// </summary>
            public static RDFResource LONG {
                get { return new RDFResource(XSD.BASE_URI + "long"); }
            }

            /// <summary>
            /// xsd:unsignedLong
            /// </summary>
            public static RDFResource UNSIGNED_LONG {
                get { return new RDFResource(XSD.BASE_URI + "unsignedLong"); }
            }

            /// <summary>
            /// xsd:int
            /// </summary>
            public static RDFResource INT {
                get { return new RDFResource(XSD.BASE_URI + "int"); }
            }

            /// <summary>
            /// xsd:unsignedInt
            /// </summary>
            public static RDFResource UNSIGNED_INT {
                get { return new RDFResource(XSD.BASE_URI + "unsignedInt"); }
            }

            /// <summary>
            /// xsd:short
            /// </summary>
            public static RDFResource SHORT {
                get { return new RDFResource(XSD.BASE_URI + "short"); }
            }

            /// <summary>
            /// xsd:unsignedShort
            /// </summary>
            public static RDFResource UNSIGNED_SHORT {
                get { return new RDFResource(XSD.BASE_URI + "unsignedShort"); }
            }

            /// <summary>
            /// xsd:byte
            /// </summary>
            public static RDFResource BYTE {
                get { return new RDFResource(XSD.BASE_URI + "byte"); }
            }

            /// <summary>
            /// xsd:unsignedByte
            /// </summary>
            public static RDFResource UNSIGNED_BYTE {
                get { return new RDFResource(XSD.BASE_URI + "unsignedByte"); }
            }

            /// <summary>
            /// xsd:duration
            /// </summary>
            public static RDFResource DURATION {
                get { return new RDFResource(XSD.BASE_URI + "duration"); }
            }

            /// <summary>
            /// xsd:dateTime
            /// </summary>
            public static RDFResource DATETIME {
                get { return new RDFResource(XSD.BASE_URI + "dateTime"); }
            }

            /// <summary>
            /// xsd:time
            /// </summary>
            public static RDFResource TIME {
                get { return new RDFResource(XSD.BASE_URI + "time"); }
            }

            /// <summary>
            /// xsd:date
            /// </summary>
            public static RDFResource DATE {
                get { return new RDFResource(XSD.BASE_URI + "date"); }
            }

            /// <summary>
            /// xsd:gYearMonth
            /// </summary>
            public static RDFResource G_YEAR_MONTH {
                get { return new RDFResource(XSD.BASE_URI + "gYearMonth"); }
            }

            /// <summary>
            /// xsd:gYear
            /// </summary>
            public static RDFResource G_YEAR {
                get { return new RDFResource(XSD.BASE_URI + "gYear"); }
            }

            /// <summary>
            /// xsd:gMonth
            /// </summary>
            public static RDFResource G_MONTH {
                get { return new RDFResource(XSD.BASE_URI + "gMonth"); }
            }

            /// <summary>
            /// xsd:gMonthDay
            /// </summary>
            public static RDFResource G_MONTH_DAY {
                get { return new RDFResource(XSD.BASE_URI + "gMonthDay"); }
            }

            /// <summary>
            /// xsd:gDay
            /// </summary>
            public static RDFResource G_DAY {
                get { return new RDFResource(XSD.BASE_URI + "gDay"); }
            }

            /// <summary>
            /// xsd:hexBinary
            /// </summary>
            public static RDFResource HEX_BINARY {
                get { return new RDFResource(XSD.BASE_URI + "hexBinary"); }
            }

            /// <summary>
            /// xsd:base64Binary
            /// </summary>
            public static RDFResource BASE64_BINARY {
                get { return new RDFResource(XSD.BASE_URI + "base64Binary"); }
            }

            /// <summary>
            /// xsd:anyURI
            /// </summary>
            public static RDFResource ANY_URI {
                get { return new RDFResource(XSD.BASE_URI + "anyURI"); }
            }

            /// <summary>
            /// xsd:QName
            /// </summary>
            public static RDFResource QNAME {
                get { return new RDFResource(XSD.BASE_URI + "QName"); }
            }

            /// <summary>
            /// xsd:NOTATION
            /// </summary>
            public static RDFResource NOTATION {
                get { return new RDFResource(XSD.BASE_URI + "NOTATION"); }
            }

            /// <summary>
            /// xsd:language
            /// </summary>
            public static RDFResource LANGUAGE {
                get { return new RDFResource(XSD.BASE_URI + "language"); }
            }

            /// <summary>
            /// xsd:normalizedString
            /// </summary>
            public static RDFResource NORMALIZED_STRING {
                get { return new RDFResource(XSD.BASE_URI + "normalizedString"); }
            }

            /// <summary>
            /// xsd:token
            /// </summary>
            public static RDFResource TOKEN {
                get { return new RDFResource(XSD.BASE_URI + "token"); }
            }

            /// <summary>
            /// xsd:NMToken
            /// </summary>
            public static RDFResource NMTOKEN {
                get { return new RDFResource(XSD.BASE_URI + "NMToken"); }
            }

            /// <summary>
            /// xsd:Name
            /// </summary>
            public static RDFResource NAME {
                get { return new RDFResource(XSD.BASE_URI + "Name"); }
            }

            /// <summary>
            /// xsd:NCName
            /// </summary>
            public static RDFResource NCNAME {
                get { return new RDFResource(XSD.BASE_URI + "NCName"); }
            }
            #endregion

        }
        #endregion

        #region OWL
        /// <summary>
        /// OWL represents the OWL vocabulary.
        /// </summary>
        public static class OWL {

            #region Properties
            /// <summary>
            /// owl
            /// </summary>
            public static String PREFIX {
                get { return "owl"; }
            }

            /// <summary>
            /// xmlns:owl
            /// </summary>
            public static String BASE_URI {
                get { return "http://www.w3.org/2002/07/owl#"; }
            }

            /// <summary>
            /// owl:Ontology
            /// </summary>
            public static RDFResource ONTOLOGY {
                get { return new RDFResource(OWL.BASE_URI + "Ontology"); }
            }

            /// <summary>
            /// owl:imports
            /// </summary>
            public static RDFResource IMPORTS {
                get { return new RDFResource(OWL.BASE_URI + "imports"); }
            }

            /// <summary>
            /// owl:Class
            /// </summary>
            public static RDFResource CLASS {
                get { return new RDFResource(OWL.BASE_URI + "Class"); }
            }

            /// <summary>
            /// owl:Individual
            /// </summary>
            public static RDFResource INDIVIDUAL {
                get { return new RDFResource(OWL.BASE_URI + "Individual"); }
            }

            /// <summary>
            /// owl:Thing
            /// </summary>
            public static RDFResource THING {
                get { return new RDFResource(OWL.BASE_URI + "Thing"); }
            }

            /// <summary>
            /// owl:Nothing
            /// </summary>
            public static RDFResource NOTHING {
                get { return new RDFResource(OWL.BASE_URI + "Nothing"); }
            }

            /// <summary>
            /// owl:Restriction
            /// </summary>
            public static RDFResource RESTRICTION {
                get { return new RDFResource(OWL.BASE_URI + "Restriction"); }
            }

            /// <summary>
            /// owl:onProperty
            /// </summary>
            public static RDFResource ON_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "onProperty"); }
            }

            /// <summary>
            /// owl:equivalentClass
            /// </summary>
            public static RDFResource EQUIVALENT_CLASS {
                get { return new RDFResource(OWL.BASE_URI + "equivalentClass"); }
            }

            /// <summary>
            /// owl:DeprecatedClass
            /// </summary>
            public static RDFResource DEPRECATED_CLASS {
                get { return new RDFResource(OWL.BASE_URI + "DeprecatedClass"); }
            }

            /// <summary>
            /// owl:equivalentProperty
            /// </summary>
            public static RDFResource EQUIVALENT_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "equivalentProperty"); }
            }

            /// <summary>
            /// owl:DeprecatedProperty
            /// </summary>
            public static RDFResource DEPRECATED_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "DeprecatedProperty"); }
            }

            /// <summary>
            /// owl:inverseOf
            /// </summary>
            public static RDFResource INVERSE_OF {
                get { return new RDFResource(OWL.BASE_URI + "inverseOf"); }
            }

            /// <summary>
            /// owl:DatatypeProperty
            /// </summary>
            public static RDFResource DATATYPE_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "DatatypeProperty"); }
            }

            /// <summary>
            /// owl:ObjectProperty
            /// </summary>
            public static RDFResource OBJECT_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "ObjectProperty"); }
            }

            /// <summary>
            /// owl:TransitiveProperty
            /// </summary>
            public static RDFResource TRANSITIVE_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "TransitiveProperty"); }
            }

            /// <summary>
            /// owl:SymmetricProperty
            /// </summary>
            public static RDFResource SYMMETRIC_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "SymmetricProperty"); }
            }

            /// <summary>
            /// owl:FunctionalProperty
            /// </summary>
            public static RDFResource FUNCTIONAL_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "FunctionalProperty"); }
            }

            /// <summary>
            /// owl:InverseFunctionalProperty
            /// </summary>
            public static RDFResource INVERSE_FUNCTIONAL_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "InverseFunctionalProperty"); }
            }

            /// <summary>
            /// owl:AnnotationProperty
            /// </summary>
            public static RDFResource ANNOTATION_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "AnnotationProperty"); }
            }

            /// <summary>
            /// owl:OntologyProperty
            /// </summary>
            public static RDFResource ONTOLOGY_PROPERTY {
                get { return new RDFResource(OWL.BASE_URI + "OntologyProperty"); }
            }

            /// <summary>
            /// owl:allValuesFrom
            /// </summary>
            public static RDFResource ALL_VALUES_FROM {
                get { return new RDFResource(OWL.BASE_URI + "allValuesFrom"); }
            }

            /// <summary>
            /// owl:someValuesFrom
            /// </summary>
            public static RDFResource SOME_VALUES_FROM {
                get { return new RDFResource(OWL.BASE_URI + "someValuesFrom"); }
            }

            /// <summary>
            /// owl:hasValue
            /// </summary>
            public static RDFResource HAS_VALUE {
                get { return new RDFResource(OWL.BASE_URI + "hasValue"); }
            }

            /// <summary>
            /// owl:minCardinality
            /// </summary>
            public static RDFResource MIN_CARDINALITY {
                get { return new RDFResource(OWL.BASE_URI + "minCardinality"); }
            }

            /// <summary>
            /// owl:maxCardinality
            /// </summary>
            public static RDFResource MAX_CARDINALITY {
                get { return new RDFResource(OWL.BASE_URI + "maxCardinality"); }
            }

            /// <summary>
            /// owl:cardinality
            /// </summary>
            public static RDFResource CARDINALITY {
                get { return new RDFResource(OWL.BASE_URI + "cardinality"); }
            }

            /// <summary>
            /// owl:sameAs
            /// </summary>
            public static RDFResource SAME_AS {
                get { return new RDFResource(OWL.BASE_URI + "sameAs"); }
            }

            /// <summary>
            /// owl:differentFrom
            /// </summary>
            public static RDFResource DIFFERENT_FROM {
                get { return new RDFResource(OWL.BASE_URI + "differentFrom"); }
            }

            /// <summary>
            /// owl:intersectionOf
            /// </summary>
            public static RDFResource INTERSECTION_OF {
                get { return new RDFResource(OWL.BASE_URI + "intersectionOf"); }
            }

            /// <summary>
            /// owl:unionOf
            /// </summary>
            public static RDFResource UNION_OF {
                get { return new RDFResource(OWL.BASE_URI + "unionOf"); }
            }
			
			/// <summary>
            /// owl:complementOf
            /// </summary>
            public static RDFResource COMPLEMENT_OF {
                get { return new RDFResource(OWL.BASE_URI + "complementOf"); }
            }

            /// <summary>
            /// owl:oneOf
            /// </summary>
            public static RDFResource ONE_OF {
                get { return new RDFResource(OWL.BASE_URI + "oneOf"); }
            }

            /// <summary>
            /// owl:DataRange
            /// </summary>
            public static RDFResource DATA_RANGE {
                get { return new RDFResource(OWL.BASE_URI + "DataRange"); }
            }

            /// <summary>
            /// owl:backwardCompatibleWith
            /// </summary>
            public static RDFResource BACKWARD_COMPATIBLE_WITH {
                get { return new RDFResource(OWL.BASE_URI + "backwardCompatibleWith"); }
            }

            /// <summary>
            /// owl:incompatibleWith
            /// </summary>
            public static RDFResource INCOMPATIBLE_WITH {
                get { return new RDFResource(OWL.BASE_URI + "incompatibleWith"); }
            }

            /// <summary>
            /// owl:disjointWith
            /// </summary>
            public static RDFResource DISJOINT_WITH {
                get { return new RDFResource(OWL.BASE_URI + "disjointWith"); }
            }

            /// <summary>
            /// owl:priorVersion
            /// </summary>
            public static RDFResource PRIOR_VERSION {
                get { return new RDFResource(OWL.BASE_URI + "priorVersion"); }
            }

            /// <summary>
            /// owl:versionInfo
            /// </summary>
            public static RDFResource VERSION_INFO {
                get { return new RDFResource(OWL.BASE_URI + "versionInfo"); }
            }
            #endregion

        }
        #endregion

        #region XML
        /// <summary>
        /// XML represents the XML vocabulary.
        /// </summary>
        public static class XML {

            #region Properties
            /// <summary>
            /// xml
            /// </summary>
            public static String PREFIX {
                get { return "xml"; }
            }

            /// <summary>
            /// xmlns:xml
            /// </summary>
            public static String BASE_URI {
                get { return "http://www.w3.org/XML/1998/namespace#"; }
            }

            /// <summary>
            /// xml:lang
            /// </summary>
            public static RDFResource LANG {
                get { return new RDFResource(XML.BASE_URI + "lang"); }
            }

            /// <summary>
            /// xml:base
            /// </summary>
            public static RDFResource BASE {
                get { return new RDFResource(XML.BASE_URI + "base"); }
            }
            #endregion

        }
        #endregion

        #endregion

        #region Extended

        #region DC
        /// <summary>
        /// DC represents the Dublin Core vocabulary.
        /// </summary>
        public static class DC {

            #region Properties
            /// <summary>
            /// dc
            /// </summary>
            public static String PREFIX {
                get { return "dc"; }
            }

            /// <summary>
            /// xmlns:dc
            /// </summary>
            public static String BASE_URI {
                get { return "http://purl.org/dc/elements/1.1/"; }
            }

            /// <summary>
            /// dc:contributor
            /// </summary>
            public static RDFResource CONTRIBUTOR {
                get { return new RDFResource(DC.BASE_URI + "contributor"); }
            }

            /// <summary>
            /// dc:coverage
            /// </summary>
            public static RDFResource COVERAGE {
                get { return new RDFResource(DC.BASE_URI + "coverage"); }
            }

            /// <summary>
            /// dc:creator
            /// </summary>
            public static RDFResource CREATOR {
                get { return new RDFResource(DC.BASE_URI + "creator"); }
            }

            /// <summary>
            /// dc:date
            /// </summary>
            public static RDFResource DATE {
                get { return new RDFResource(DC.BASE_URI + "date"); }
            }

            /// <summary>
            /// dc:description
            /// </summary>
            public static RDFResource DESCRIPTION {
                get { return new RDFResource(DC.BASE_URI + "description"); }
            }

            /// <summary>
            /// dc:format
            /// </summary>
            public static RDFResource FORMAT {
                get { return new RDFResource(DC.BASE_URI + "format"); }
            }

            /// <summary>
            /// dc:identifier
            /// </summary>
            public static RDFResource IDENTIFIER {
                get { return new RDFResource(DC.BASE_URI + "identifier"); }
            }

            /// <summary>
            /// dc:language
            /// </summary>
            public static RDFResource LANGUAGE {
                get { return new RDFResource(DC.BASE_URI + "language"); }
            }

            /// <summary>
            /// dc:publisher
            /// </summary>
            public static RDFResource PUBLISHER {
                get { return new RDFResource(DC.BASE_URI + "publisher"); }
            }

            /// <summary>
            /// dc:relation
            /// </summary>
            public static RDFResource RELATION {
                get { return new RDFResource(DC.BASE_URI + "relation"); }
            }

            /// <summary>
            /// dc:rights
            /// </summary>
            public static RDFResource RIGHTS {
                get { return new RDFResource(DC.BASE_URI + "rights"); }
            }

            /// <summary>
            /// dc:source
            /// </summary>
            public static RDFResource SOURCE {
                get { return new RDFResource(DC.BASE_URI + "source"); }
            }

            /// <summary>
            /// dc:subject
            /// </summary>
            public static RDFResource SUBJECT {
                get { return new RDFResource(DC.BASE_URI + "subject"); }
            }

            /// <summary>
            /// dc:title
            /// </summary>
            public static RDFResource TITLE {
                get { return new RDFResource(DC.BASE_URI + "title"); }
            }

            /// <summary>
            /// dc:type
            /// </summary>
            public static RDFResource TYPE {
                get { return new RDFResource(DC.BASE_URI + "type"); }
            }
            #endregion

            #region Extended Properties

            #region DC_TERMS
            public static class DC_TERMS {

                #region Properties
                /// <summary>
                /// dcterms
                /// </summary>
                public static String PREFIX {
                    get { return "dcterms"; }
                }

                /// <summary>
                /// xmlns:dcterms
                /// </summary>
                public static String BASE_URI {
                    get { return "http://purl.org/dc/terms/"; }
                }

                /// <summary>
                /// dcterms:abstract
                /// </summary>
                public static RDFResource ABSTRACT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "abstract"); }
                }

                /// <summary>
                /// dcterms:accessRights
                /// </summary>
                public static RDFResource ACCESS_RIGHTS {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "accessRights"); }
                }

                /// <summary>
                /// dcterms:accrualMethod
                /// </summary>
                public static RDFResource ACCRUAL_METHOD {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "accrualMethod"); }
                }

                /// <summary>
                /// dcterms:accrualPeriodicity
                /// </summary>
                public static RDFResource ACCRUAL_PERIODICITY {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "accrualPeriodicity"); }
                }

                /// <summary>
                /// dcterms:accrualPolicy
                /// </summary>
                public static RDFResource ACCRUAL_POLICY {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "accrualPolicy"); }
                }

                /// <summary>
                /// dcterms:Agent
                /// </summary>
                public static RDFResource AGENT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "Agent"); }
                }

                /// <summary>
                /// dcterms:AgentClass
                /// </summary>
                public static RDFResource AGENT_CLASS {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "AgentClass"); }
                }

                /// <summary>
                /// dcterms:alternative
                /// </summary>
                public static RDFResource ALTERNATIVE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "alternative"); }
                }

                /// <summary>
                /// dcterms:audience
                /// </summary>
                public static RDFResource AUDIENCE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "audience"); }
                }

                /// <summary>
                /// dcterms:available
                /// </summary>
                public static RDFResource AVAILABLE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "available"); }
                }

                /// <summary>
                /// dcterms:bibliographicCitation
                /// </summary>
                public static RDFResource BIBLIOGRAPHIC_CITATION {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "bibliographicCitation"); }
                }

                /// <summary>
                /// dcterms:BibliographicResource
                /// </summary>
                public static RDFResource BIBLIOGRAPHIC_RESOURCE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "BibliographicResource"); }
                }                

                /// <summary>
                /// dcterms:conformsTo
                /// </summary>
                public static RDFResource CONFORMS_TO {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "conformsTo"); }
                }

                /// <summary>
                /// dcterms:contributor
                /// </summary>
                public static RDFResource CONTRIBUTOR {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "contributor"); }
                }

                /// <summary>
                /// dcterms:coverage
                /// </summary>
                public static RDFResource COVERAGE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "coverage"); }
                }

                /// <summary>
                /// dcterms:created
                /// </summary>
                public static RDFResource CREATED {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "created"); }
                }

                /// <summary>
                /// dcterms:creator
                /// </summary>
                public static RDFResource CREATOR {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "creator"); }
                }

                /// <summary>
                /// dcterms:date
                /// </summary>
                public static RDFResource DATE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "date"); }
                }

                /// <summary>
                /// dcterms:dateAccepted
                /// </summary>
                public static RDFResource DATE_ACCEPTED {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "dateAccepted"); }
                }

                /// <summary>
                /// dcterms:dateCopyrighted
                /// </summary>
                public static RDFResource DATE_COPYRIGHTED {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "dateCopyrighted"); }
                }

                /// <summary>
                /// dcterms:dateSubmitted
                /// </summary>
                public static RDFResource DATE_SUBMITTED {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "dateSubmitted"); }
                }

                /// <summary>
                /// dcterms:description
                /// </summary>
                public static RDFResource DESCRIPTION {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "description"); }
                }

                /// <summary>
                /// dcterms:educationLevel
                /// </summary>
                public static RDFResource EDUCATION_LEVEL {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "educationLevel"); }
                }

                /// <summary>
                /// dcterms:extent
                /// </summary>
                public static RDFResource EXTENT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "extent"); }
                }

                /// <summary>
                /// dcterms:FileFormat
                /// </summary>
                public static RDFResource FILE_FORMAT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "FileFormat"); }
                }

                /// <summary>
                /// dcterms:format
                /// </summary>
                public static RDFResource FORMAT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "format"); }
                }

                /// <summary>
                /// dcterms:Frequency
                /// </summary>
                public static RDFResource FREQUENCY {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "Frequency"); }
                }

                /// <summary>
                /// dcterms:hasFormat
                /// </summary>
                public static RDFResource HAS_FORMAT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "hasFormat"); }
                }

                /// <summary>
                /// dcterms:hasPart
                /// </summary>
                public static RDFResource HAS_PART {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "hasPart"); }
                }

                /// <summary>
                /// dcterms:hasVersion
                /// </summary>
                public static RDFResource HAS_VERSION {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "hasVersion"); }
                }

                /// <summary>
                /// dcterms:identifier
                /// </summary>
                public static RDFResource IDENTIFIER {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "identifier"); }
                }

                /// <summary>
                /// dcterms:instructionalMethod
                /// </summary>
                public static RDFResource INSTRUCTIONAL_METHOD {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "instructionalMethod"); }
                }

                /// <summary>
                /// dcterms:isFormatOf
                /// </summary>
                public static RDFResource IS_FORMAT_OF {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "isFormatOf"); }
                }

                /// <summary>
                /// dcterms:isPartOf
                /// </summary>
                public static RDFResource IS_PART_OF {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "isPartOf"); }
                }

                /// <summary>
                /// dcterms:isReferencedBy
                /// </summary>
                public static RDFResource IS_REFERENCED_BY {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "isReferencedBy"); }
                }

                /// <summary>
                /// dcterms:isReplacedBy
                /// </summary>
                public static RDFResource IS_REPLACED_BY {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "isReplacedBy"); }
                }

                /// <summary>
                /// dcterms:isRequiredBy
                /// </summary>
                public static RDFResource IS_REQUIRED_BY {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "isRequiredBy"); }
                }

                /// <summary>
                /// dcterms:issued
                /// </summary>
                public static RDFResource ISSUED {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "issued"); }
                }

                /// <summary>
                /// dcterms:isVersionOf
                /// </summary>
                public static RDFResource IS_VERSION_OF {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "isVersionOf"); }
                }

                /// <summary>
                /// dcterms:Jurisdiction
                /// </summary>
                public static RDFResource JURISDICTION {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "Jurisdiction"); }
                }

                /// <summary>
                /// dcterms:language
                /// </summary>
                public static RDFResource LANGUAGE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "language"); }
                }

                /// <summary>
                /// dcterms:license
                /// </summary>
                public static RDFResource LICENSE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "license"); }
                }

                /// <summary>
                /// dcterms:LicenseDocument
                /// </summary>
                public static RDFResource LICENSE_DOCUMENT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "LicenseDocument"); }
                }

                /// <summary>
                /// dcterms:LinguisticSystem
                /// </summary>
                public static RDFResource LINGUISTIC_SYSTEM {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "LinguisticSystem"); }
                }

                /// <summary>
                /// dcterms:Location
                /// </summary>
                public static RDFResource LOCATION {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "Location"); }
                }

                /// <summary>
                /// dcterms:LocationPeriodOrJurisdiction
                /// </summary>
                public static RDFResource LOCATION_PERIOD_OR_JURISDICTION {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "LocationPeriodOrJurisdiction"); }
                }

                /// <summary>
                /// dcterms:mediator
                /// </summary>
                public static RDFResource MEDIATOR {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "mediator"); }
                }

                /// <summary>
                /// dcterms:MediaType
                /// </summary>
                public static RDFResource MEDIA_TYPE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "MediaType"); }
                }

                /// <summary>
                /// dcterms:MediaTypeOrExtent
                /// </summary>
                public static RDFResource MEDIA_TYPE_OR_EXTENT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "MediaTypeOrExtent"); }
                }

                /// <summary>
                /// dcterms:medium
                /// </summary>
                public static RDFResource MEDIUM {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "medium"); }
                }

                /// <summary>
                /// dcterms:MethodOfAccrual
                /// </summary>
                public static RDFResource METHOD_OF_ACCRUAL {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "MethodOfAccrual"); }
                }

                /// <summary>
                /// dcterms:MethodOfInstruction
                /// </summary>
                public static RDFResource METHOD_OF_INSTRUCTION {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "MethodOfInstruction"); }
                }

                /// <summary>
                /// dcterms:modified
                /// </summary>
                public static RDFResource MODIFIED {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "modified"); }
                }

                /// <summary>
                /// dcterms:PeriodOfTime
                /// </summary>
                public static RDFResource PERIOD_OF_TIME {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "PeriodOfTime"); }
                }

                /// <summary>
                /// dcterms:PhysicalMedium
                /// </summary>
                public static RDFResource PHYSICAL_MEDIUM {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "PhysicalMedium"); }
                }

                /// <summary>
                /// dcterms:PhysicalResource
                /// </summary>
                public static RDFResource PHYSICAL_RESOURCE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "PhysicalResource"); }
                }

                /// <summary>
                /// dcterms:Policy
                /// </summary>
                public static RDFResource POLICY {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "Policy"); }
                }

                /// <summary>
                /// dcterms:provenance
                /// </summary>
                public static RDFResource PROVENANCE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "provenance"); }
                }

                /// <summary>
                /// dcterms:ProvenanceStatement
                /// </summary>
                public static RDFResource PROVENANCE_STATEMENT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "ProvenanceStatement"); }
                }

                /// <summary>
                /// dcterms:publisher
                /// </summary>
                public static RDFResource PUBLISHER {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "publisher"); }
                }

                /// <summary>
                /// dcterms:references
                /// </summary>
                public static RDFResource REFERENCES {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "references"); }
                }

                /// <summary>
                /// dcterms:relation
                /// </summary>
                public static RDFResource RELATION {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "relation"); }
                }

                /// <summary>
                /// dcterms:replaces
                /// </summary>
                public static RDFResource REPLACES {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "replaces"); }
                }

                /// <summary>
                /// dcterms:requires
                /// </summary>
                public static RDFResource REQUIRES {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "requires"); }
                }

                /// <summary>
                /// dcterms:rights
                /// </summary>
                public static RDFResource RIGHTS {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "rights"); }
                }

                /// <summary>
                /// dcterms:RightsStatement
                /// </summary>
                public static RDFResource RIGHTS_STATEMENT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "RightsStatement"); }
                }

                /// <summary>
                /// dcterms:rightsHolder
                /// </summary>
                public static RDFResource RIGHTS_HOLDER {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "rightsHolder"); }
                }

                /// <summary>
                /// dcterms:SizeOrDuration
                /// </summary>
                public static RDFResource SIZE_OR_DURATION {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "SizeOrDuration"); }
                }

                /// <summary>
                /// dcterms:source
                /// </summary>
                public static RDFResource SOURCE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "source"); }
                }

                /// <summary>
                /// dcterms:spatial
                /// </summary>
                public static RDFResource SPATIAL {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "spatial"); }
                }

                /// <summary>
                /// dcterms:Standard
                /// </summary>
                public static RDFResource STANDARD {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "Standard"); }
                }

                /// <summary>
                /// dcterms:subject
                /// </summary>
                public static RDFResource SUBJECT {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "subject"); }
                }

                /// <summary>
                /// dcterms:tableOfContents
                /// </summary>
                public static RDFResource TABLE_OF_CONTENTS {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "tableOfContents"); }
                }

                /// <summary>
                /// dcterms:temporal
                /// </summary>
                public static RDFResource TEMPORAL {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "temporal"); }
                }

                /// <summary>
                /// dcterms:title
                /// </summary>
                public static RDFResource TITLE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "title"); }
                }

                /// <summary>
                /// dcterms:type
                /// </summary>
                public static RDFResource TYPE {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "type"); }
                }

                /// <summary>
                /// dcterms:valid
                /// </summary>
                public static RDFResource VALID {
                    get { return new RDFResource(DC_TERMS.BASE_URI + "valid"); }
                }
                #endregion

            }
            #endregion

            #endregion

        }
        #endregion

        #region FOAF
        /// <summary>
        /// FOAF represents the Friend-of-a-Friend vocabulary.
        /// </summary>
        public static class FOAF {

            #region Properties
            /// <summary>
            /// foaf
            /// </summary>
            public static String PREFIX {
                get { return "foaf"; }
            }

            /// <summary>
            /// xmlns:foaf
            /// </summary>
            public static String BASE_URI {
                get { return "http://xmlns.com/foaf/0.1/"; }
            }

            /// <summary>
            /// foaf:Agent
            /// </summary>
            public static RDFResource AGENT {
                get { return new RDFResource(FOAF.BASE_URI + "Agent"); }
            }

            /// <summary>
            /// foaf:Person
            /// </summary>
            public static RDFResource PERSON {
                get { return new RDFResource(FOAF.BASE_URI + "Person"); }
            }

            /// <summary>
            /// foaf:name
            /// </summary>
            public static RDFResource NAME {
                get { return new RDFResource(FOAF.BASE_URI + "name"); }
            }

            /// <summary>
            /// foaf:title
            /// </summary>
            public static RDFResource TITLE {
                get { return new RDFResource(FOAF.BASE_URI + "title"); }
            }

            /// <summary>
            /// foaf:img
            /// </summary>
            public static RDFResource IMG {
                get { return new RDFResource(FOAF.BASE_URI + "img"); }
            }

            /// <summary>
            /// foaf:depiction
            /// </summary>
            public static RDFResource DEPICTION {
                get { return new RDFResource(FOAF.BASE_URI + "depiction"); }
            }

            /// <summary>
            /// foaf:depicts
            /// </summary>
            public static RDFResource DEPICTS {
                get { return new RDFResource(FOAF.BASE_URI + "depicts"); }
            }

            /// <summary>
            /// foaf:familyName
            /// </summary>
            public static RDFResource FAMILY_NAME {
                get { return new RDFResource(FOAF.BASE_URI + "familyName"); }
            }

            /// <summary>
            /// foaf:givenName
            /// </summary>
            public static RDFResource GIVEN_NAME {
                get { return new RDFResource(FOAF.BASE_URI + "givenName"); }
            }

            /// <summary>
            /// foaf:knows
            /// </summary>
            public static RDFResource KNOWS {
                get { return new RDFResource(FOAF.BASE_URI + "knows"); }
            }

            /// <summary>
            /// foaf:based_near
            /// </summary>
            public static RDFResource BASED_NEAR {
                get { return new RDFResource(FOAF.BASE_URI + "based_near"); }
            }

            /// <summary>
            /// foaf:age
            /// </summary>
            public static RDFResource AGE {
                get { return new RDFResource(FOAF.BASE_URI + "age"); }
            }

            /// <summary>
            /// foaf:made
            /// </summary>
            public static RDFResource MADE {
                get { return new RDFResource(FOAF.BASE_URI + "made"); }
            }

            /// <summary>
            /// foaf:maker
            /// </summary>
            public static RDFResource MAKER {
                get { return new RDFResource(FOAF.BASE_URI + "maker"); }
            }

            /// <summary>
            /// foaf:primaryTopic
            /// </summary>
            public static RDFResource PRIMARY_TOPIC {
                get { return new RDFResource(FOAF.BASE_URI + "primaryTopic"); }
            }

            /// <summary>
            /// foaf:isPrimaryTopicOf
            /// </summary>
            public static RDFResource IS_PRIMARY_TOPIC_OF {
                get { return new RDFResource(FOAF.BASE_URI + "isPrimaryTopicOf"); }
            }

            /// <summary>
            /// foaf:Project
            /// </summary>
            public static RDFResource PROJECT {
                get { return new RDFResource(FOAF.BASE_URI + "Project"); }
            }

            /// <summary>
            /// foaf:Organization
            /// </summary>
            public static RDFResource ORGANIZATION {
                get { return new RDFResource(FOAF.BASE_URI + "Organization"); }
            }

            /// <summary>
            /// foaf:Group
            /// </summary>
            public static RDFResource GROUP {
                get { return new RDFResource(FOAF.BASE_URI + "Group"); }
            }

            /// <summary>
            /// foaf:Document
            /// </summary>
            public static RDFResource DOCUMENT {
                get { return new RDFResource(FOAF.BASE_URI + "Document"); }
            }

            /// <summary>
            /// foaf:Image
            /// </summary>
            public static RDFResource IMAGE {
                get { return new RDFResource(FOAF.BASE_URI + "Image"); }
            }

            /// <summary>
            /// foaf:member
            /// </summary>
            public static RDFResource MEMBER {
                get { return new RDFResource(FOAF.BASE_URI + "member"); }
            }
			
			/// <summary>
            /// foaf:focus
            /// </summary>
            public static RDFResource FOCUS {
                get { return new RDFResource(FOAF.BASE_URI + "focus"); }
            }
			
			/// <summary>
            /// foaf:nick
            /// </summary>
            public static RDFResource NICK {
                get { return new RDFResource(FOAF.BASE_URI + "nick"); }
            }
			
			/// <summary>
            /// foaf:mbox
            /// </summary>
            public static RDFResource MBOX {
                get { return new RDFResource(FOAF.BASE_URI + "mbox"); }
            }
			
			/// <summary>
            /// foaf:homepage
            /// </summary>
            public static RDFResource HOMEPAGE {
                get { return new RDFResource(FOAF.BASE_URI + "homepage"); }
            }
			
			/// <summary>
            /// foaf:weblog
            /// </summary>
            public static RDFResource WEBLOG {
                get { return new RDFResource(FOAF.BASE_URI + "weblog"); }
            }
			
			/// <summary>
            /// foaf:openid
            /// </summary>
            public static RDFResource OPEN_ID {
                get { return new RDFResource(FOAF.BASE_URI + "openid"); }
            }
			
			/// <summary>
            /// foaf:jabberID
            /// </summary>
            public static RDFResource JABBER_ID {
                get { return new RDFResource(FOAF.BASE_URI + "jabberID"); }
            }
			
			/// <summary>
            /// foaf:mbox_sha1sum
            /// </summary>
            public static RDFResource MBOX_SHA1SUM {
                get { return new RDFResource(FOAF.BASE_URI + "mbox_sha1sum"); }
            }
			
			/// <summary>
            /// foaf:interest
            /// </summary>
            public static RDFResource INTEREST {
                get { return new RDFResource(FOAF.BASE_URI + "interest"); }
            }
			
			/// <summary>
            /// foaf:topic_interest
            /// </summary>
            public static RDFResource TOPIC_INTEREST {
                get { return new RDFResource(FOAF.BASE_URI + "topic_interest"); }
            }
			
			/// <summary>
            /// foaf:topic
            /// </summary>
            public static RDFResource TOPIC {
                get { return new RDFResource(FOAF.BASE_URI + "topic"); }
            }
			
			/// <summary>
            /// foaf:page
            /// </summary>
            public static RDFResource PAGE {
                get { return new RDFResource(FOAF.BASE_URI + "page"); }
            }
			
			/// <summary>
            /// foaf:workplaceHomepage
            /// </summary>
            public static RDFResource WORKPLACE_HOMEPAGE {
                get { return new RDFResource(FOAF.BASE_URI + "workplaceHomepage"); }
            }
			
			/// <summary>
            /// foaf:workinfoHomepage
            /// </summary>
            public static RDFResource WORKINFO_HOMEPAGE {
                get { return new RDFResource(FOAF.BASE_URI + "workinfoHomepage"); }
            }
			
			/// <summary>
            /// foaf:schoolHomepage
            /// </summary>
            public static RDFResource SCHOOL_HOMEPAGE {
                get { return new RDFResource(FOAF.BASE_URI + "schoolHomepage"); }
            }
			
			/// <summary>
            /// foaf:publications
            /// </summary>
            public static RDFResource PUBLICATIONS {
                get { return new RDFResource(FOAF.BASE_URI + "publications"); }
            }
			
			/// <summary>
            /// foaf:currentProject
            /// </summary>
            public static RDFResource CURRENT_PROJECT {
                get { return new RDFResource(FOAF.BASE_URI + "currentProject"); }
            }
			
			/// <summary>
            /// foaf:pastProject
            /// </summary>
            public static RDFResource PAST_PROJECT {
                get { return new RDFResource(FOAF.BASE_URI + "pastProject"); }
            }
			
			/// <summary>
            /// foaf:account
            /// </summary>
            public static RDFResource ACCOUNT {
                get { return new RDFResource(FOAF.BASE_URI + "account"); }
            }
			
			/// <summary>
            /// foaf:OnlineAccount
            /// </summary>
            public static RDFResource ONLINE_ACCOUNT {
                get { return new RDFResource(FOAF.BASE_URI + "OnlineAccount"); }
            }
			
			/// <summary>
            /// foaf:accountName
            /// </summary>
            public static RDFResource ACCOUNT_NAME {
                get { return new RDFResource(FOAF.BASE_URI + "accountName"); }
            }
			
			/// <summary>
            /// foaf:accountServiceHomepage
            /// </summary>
            public static RDFResource ACCOUNT_SERVICE_HOMEPAGE {
                get { return new RDFResource(FOAF.BASE_URI + "accountServiceHomepage"); }
            }
			
			/// <summary>
            /// foaf:PersonalProfileDocument
            /// </summary>
            public static RDFResource PERSONAL_PROFILE_DOCUMENT {
                get { return new RDFResource(FOAF.BASE_URI + "PersonalProfileDocument"); }
            }
			
			/// <summary>
            /// foaf:tipjar
            /// </summary>
            public static RDFResource TIPJAR {
                get { return new RDFResource(FOAF.BASE_URI + "tipjar"); }
            }
			
			/// <summary>
            /// foaf:sha1
            /// </summary>
            public static RDFResource SHA1 {
                get { return new RDFResource(FOAF.BASE_URI + "sha1"); }
            }
			
			/// <summary>
            /// foaf:thumbnail
            /// </summary>
            public static RDFResource THUMBNAIL {
                get { return new RDFResource(FOAF.BASE_URI + "thumbnail"); }
            }
			
			/// <summary>
            /// foaf:logo
            /// </summary>
            public static RDFResource LOGO {
                get { return new RDFResource(FOAF.BASE_URI + "logo"); }
            }
			
			/// <summary>
            /// foaf:phone
            /// </summary>
            public static RDFResource PHONE {
                get { return new RDFResource(FOAF.BASE_URI + "phone"); }
            }
			
			/// <summary>
            /// foaf:status
            /// </summary>
            public static RDFResource STATUS {
                get { return new RDFResource(FOAF.BASE_URI + "status"); }
            }
			
			/// <summary>
            /// foaf:gender
            /// </summary>
            public static RDFResource GENDER {
                get { return new RDFResource(FOAF.BASE_URI + "gender"); }
            }
			
			/// <summary>
            /// foaf:birthday
            /// </summary>
            public static RDFResource BIRTHDAY {
                get { return new RDFResource(FOAF.BASE_URI + "birthday"); }
            }
            #endregion

        }
        #endregion

        #region GEO
        /// <summary>
        /// GEO represents the W3C GEO vocabulary.
        /// </summary>
        public static class GEO {

            #region Properties
            /// <summary>
            /// geo
            /// </summary>
            public static String PREFIX {
                get { return "geo"; }
            }

            /// <summary>
            /// xmlns:geo
            /// </summary>
            public static String BASE_URI {
                get { return "http://www.w3.org/2003/01/geo/wgs84_pos#"; }
            }

            /// <summary>
            /// geo:lat
            /// </summary>
            public static RDFResource LAT {
                get { return new RDFResource(GEO.BASE_URI + "lat"); }
            }

            /// <summary>
            /// geo:long
            /// </summary>
            public static RDFResource LONG {
                get { return new RDFResource(GEO.BASE_URI + "long"); }
            }

            /// <summary>
            /// geo:lat_long
            /// </summary>
            public static RDFResource LAT_LONG {
                get { return new RDFResource(GEO.BASE_URI + "lat_long"); }
            }

            /// <summary>
            /// geo:alt
            /// </summary>
            public static RDFResource ALT {
                get { return new RDFResource(GEO.BASE_URI + "alt"); }
            }

            /// <summary>
            /// geo:Point
            /// </summary>
            public static RDFResource POINT {
                get { return new RDFResource(GEO.BASE_URI + "Point"); }
            }

            /// <summary>
            /// geo:SpatialThing
            /// </summary>
            public static RDFResource SPATIAL_THING {
                get { return new RDFResource(GEO.BASE_URI + "SpatialThing"); }
            }

            /// <summary>
            /// geo:location
            /// </summary>
            public static RDFResource LOCATION {
                get { return new RDFResource(GEO.BASE_URI + "location"); }
            }
            #endregion

        }
        #endregion

        #region SKOS
        /// <summary>
        /// SKOS represents the W3C SKOS vocabulary.
        /// </summary>
        public static class SKOS {

            #region Properties
            /// <summary>
            /// skos
            /// </summary>
            public static String PREFIX {
                get { return "skos"; }
            }

            /// <summary>
            /// xmlns:skos
            /// </summary>
            public static String BASE_URI {
                get { return "http://www.w3.org/2004/02/skos/core#"; }
            }

            /// <summary>
            /// skos:Concept
            /// </summary>
            public static RDFResource CONCEPT {
                get { return new RDFResource(SKOS.BASE_URI + "Concept"); }
            }

            /// <summary>
            /// skos:ConceptScheme
            /// </summary>
            public static RDFResource CONCEPT_SCHEME {
                get { return new RDFResource(SKOS.BASE_URI + "ConceptScheme"); }
            }

            /// <summary>
            /// skos:inScheme
            /// </summary>
            public static RDFResource IN_SCHEME {
                get { return new RDFResource(SKOS.BASE_URI + "inScheme"); }
            }

            /// <summary>
            /// skos:hasTopConcept
            /// </summary>
            public static RDFResource HAS_TOP_CONCEPT {
                get { return new RDFResource(SKOS.BASE_URI + "hasTopConcept"); }
            }

            /// <summary>
            /// skos:topConceptOf
            /// </summary>
            public static RDFResource TOP_CONCEPT_OF {
                get { return new RDFResource(SKOS.BASE_URI + "topConceptOf"); }
            }

            /// <summary>
            /// skos:altLabel
            /// </summary>
            public static RDFResource ALT_LABEL {
                get { return new RDFResource(SKOS.BASE_URI + "altLabel"); }
            }

            /// <summary>
            /// skos:hiddenLabel
            /// </summary>
            public static RDFResource HIDDEN_LABEL {
                get { return new RDFResource(SKOS.BASE_URI + "hiddenLabel"); }
            }

            /// <summary>
            /// skos:prefLabel
            /// </summary>
            public static RDFResource PREF_LABEL {
                get { return new RDFResource(SKOS.BASE_URI + "prefLabel"); }
            }

            /// <summary>
            /// skos:notation
            /// </summary>
            public static RDFResource NOTATION {
                get { return new RDFResource(SKOS.BASE_URI + "notation"); }
            }

            /// <summary>
            /// skos:changeNote
            /// </summary>
            public static RDFResource CHANGE_NOTE {
                get { return new RDFResource(SKOS.BASE_URI + "changeNote"); }
            }

            /// <summary>
            /// skos:definition
            /// </summary>
            public static RDFResource DEFINITION {
                get { return new RDFResource(SKOS.BASE_URI + "definition"); }
            }

            /// <summary>
            /// skos:example
            /// </summary>
            public static RDFResource EXAMPLE {
                get { return new RDFResource(SKOS.BASE_URI + "example"); }
            }

            /// <summary>
            /// skos:editorialNote
            /// </summary>
            public static RDFResource EDITORIAL_NOTE {
                get { return new RDFResource(SKOS.BASE_URI + "editorialNote"); }
            }

            /// <summary>
            /// skos:historyNote
            /// </summary>
            public static RDFResource HISTORY_NOTE {
                get { return new RDFResource(SKOS.BASE_URI + "historyNote"); }
            }

            /// <summary>
            /// skos:note
            /// </summary>
            public static RDFResource NOTE {
                get { return new RDFResource(SKOS.BASE_URI + "note"); }
            }

            /// <summary>
            /// skos:scopeNote
            /// </summary>
            public static RDFResource SCOPE_NOTE {
                get { return new RDFResource(SKOS.BASE_URI + "scopeNote"); }
            }

            /// <summary>
            /// skos:broader
            /// </summary>
            public static RDFResource BROADER {
                get { return new RDFResource(SKOS.BASE_URI + "broader"); }
            }

            /// <summary>
            /// skos:broaderTransitive
            /// </summary>
            public static RDFResource BROADER_TRANSITIVE {
                get { return new RDFResource(SKOS.BASE_URI + "broaderTransitive"); }
            }

            /// <summary>
            /// skos:narrower
            /// </summary>
            public static RDFResource NARROWER {
                get { return new RDFResource(SKOS.BASE_URI + "narrower"); }
            }

            /// <summary>
            /// skos:narrowerTransitive
            /// </summary>
            public static RDFResource NARROWER_TRANSITIVE {
                get { return new RDFResource(SKOS.BASE_URI + "narrowerTransitive"); }
            }

            /// <summary>
            /// skos:related
            /// </summary>
            public static RDFResource RELATED {
                get { return new RDFResource(SKOS.BASE_URI + "related"); }
            }

            /// <summary>
            /// skos:semanticRelation
            /// </summary>
            public static RDFResource SEMANTIC_RELATION {
                get { return new RDFResource(SKOS.BASE_URI + "semanticRelation"); }
            }

            /// <summary>
            /// skos:subject
            /// </summary>
            public static RDFResource SUBJECT {
                get { return new RDFResource(SKOS.BASE_URI + "subject"); }
            }

            /// <summary>
            /// skos:Collection
            /// </summary>
            public static RDFResource COLLECTION {
                get { return new RDFResource(SKOS.BASE_URI + "Collection"); }
            }

            /// <summary>
            /// skos:OrderedCollection
            /// </summary>
            public static RDFResource ORDERED_COLLECTION {
                get { return new RDFResource(SKOS.BASE_URI + "OrderedCollection"); }
            }

            /// <summary>
            /// skos:member
            /// </summary>
            public static RDFResource MEMBER {
                get { return new RDFResource(SKOS.BASE_URI + "member"); }
            }

            /// <summary>
            /// skos:memberList
            /// </summary>
            public static RDFResource MEMBER_LIST {
                get { return new RDFResource(SKOS.BASE_URI + "memberList"); }
            }

            /// <summary>
            /// skos:broadMatch
            /// </summary>
            public static RDFResource BROAD_MATCH {
                get { return new RDFResource(SKOS.BASE_URI + "broadMatch"); }
            }

            /// <summary>
            /// skos:closeMatch
            /// </summary>
            public static RDFResource CLOSE_MATCH {
                get { return new RDFResource(SKOS.BASE_URI + "closeMatch"); }
            }

            /// <summary>
            /// skos:narrowMatch
            /// </summary>
            public static RDFResource NARROW_MATCH {
                get { return new RDFResource(SKOS.BASE_URI + "narrowMatch"); }
            }

            /// <summary>
            /// skos:relatedMatch
            /// </summary>
            public static RDFResource RELATED_MATCH {
                get { return new RDFResource(SKOS.BASE_URI + "relatedMatch"); }
            }

            /// <summary>
            /// skos:exactMatch
            /// </summary>
            public static RDFResource EXACT_MATCH {
                get { return new RDFResource(SKOS.BASE_URI + "exactMatch"); }
            }

            /// <summary>
            /// skos:mappingRelation
            /// </summary>
            public static RDFResource MAPPING_RELATION {
                get { return new RDFResource(SKOS.BASE_URI + "mappingRelation"); }
            }
            #endregion

        }
        #endregion

        #region RSS
        /// <summary>
        /// RSS represents the RSS 1.0 vocabulary.
        /// </summary>
        public static class RSS {

            #region Properties
            /// <summary>
            /// rss
            /// </summary>
            public static String PREFIX {
                get { return "rss"; }
            }

            /// <summary>
            /// xmlns:rss
            /// </summary>
            public static String BASE_URI {
                get { return "http://purl.org/rss/1.0/"; }
            }

            /// <summary>
            /// rss:channel
            /// </summary>
            public static String CHANNEL {
                get { return RSS.BASE_URI + "channel"; }
            }

            /// <summary>
            /// rss:title
            /// </summary>
            public static String TITLE {
                get { return RSS.BASE_URI + "title"; }
            }

            /// <summary>
            /// rss:link
            /// </summary>
            public static String LINK {
                get { return RSS.BASE_URI + "link"; }
            }

            /// <summary>
            /// rss:description
            /// </summary>
            public static String DESCRIPTION {
                get { return RSS.BASE_URI + "description"; }
            }

            /// <summary>
            /// rss:image
            /// </summary>
            public static String IMAGE {
                get { return RSS.BASE_URI + "image"; }
            }

            /// <summary>
            /// rss:items
            /// </summary>
            public static String ITEMS {
                get { return RSS.BASE_URI + "items"; }
            }

            /// <summary>
            /// rss:textinput
            /// </summary>
            public static String TEXT_INPUT {
                get { return RSS.BASE_URI + "textinput"; }
            }

            /// <summary>
            /// rss:item
            /// </summary>
            public static String ITEM {
                get { return RSS.BASE_URI + "item"; }
            }

            /// <summary>
            /// rss:name
            /// </summary>
            public static String NAME {
                get { return RSS.BASE_URI + "name"; }
            }

            /// <summary>
            /// rss:url
            /// </summary>
            public static String URL {
                get { return RSS.BASE_URI + "url"; }
            }
            #endregion

        }
        #endregion

		#region DBPEDIA
        /// <summary>
        /// DBPEDIA represents the DBPEDIA vocabulary.
        /// </summary>
        public static class DBPEDIA {

            #region Properties
            /// <summary>
            /// dbpedia
            /// </summary>
            public static String PREFIX {
                get { return "dbpedia"; }
            }

            /// <summary>
            /// xmlns:dbpedia
            /// </summary>
            public static String BASE_URI {
                get { return "http://dbpedia.org/"; }
            }

            /// <summary>
            /// dbpedia:resource
            /// </summary>
            public static RDFResource RESOURCE {
                get { return new RDFResource(DBPEDIA.BASE_URI + "resource"); }
            }

			/// <summary>
            /// dbpedia:class
            /// </summary>
            public static RDFResource CLASS {
                get { return new RDFResource(DBPEDIA.BASE_URI + "class"); }
            }
			
            /// <summary>
            /// dbpedia:ontology
            /// </summary>
            public static RDFResource ONTOLOGY {
                get { return new RDFResource(DBPEDIA.BASE_URI + "ontology"); }
            }
			
			/// <summary>
            /// dbpedia:property
            /// </summary>
            public static RDFResource PROPERTY {
                get { return new RDFResource(DBPEDIA.BASE_URI + "property"); }
            }
            #endregion

        }
        #endregion

        #region OG
        /// <summary>
        /// OG represents the Open Graph Protocol Vocabulary
        /// </summary>
        public static class OG {

            #region Properties
            /// <summary>
            /// og
            /// </summary>
            public static String PREFIX {
                get { return "og"; }
            }

            /// <summary>
            /// xmlns:og
            /// </summary>
            public static String BASE_URI {
                get { return "http://ogp.me/ns#"; }
            }

            /// <summary>
            /// og:title
            /// </summary>
            public static RDFResource TITLE {
                get { return new RDFResource(OG.BASE_URI + "title"); }
            }

            /// <summary>
            /// og:type
            /// </summary>
            public static RDFResource TYPE {
                get { return new RDFResource(OG.BASE_URI + "type"); }
            }

            /// <summary>
            /// og:url
            /// </summary>
            public static RDFResource URL {
                get { return new RDFResource(OG.BASE_URI + "url"); }
            }

            /// <summary>
            /// og:image
            /// </summary>
            public static RDFResource IMAGE {
                get { return new RDFResource(OG.BASE_URI + "image"); }
            }

            /// <summary>
            /// og:image:secure_url
            /// </summary>
            public static RDFResource IMAGE_SECURE_URL {
                get { return new RDFResource(OG.BASE_URI + "image:secure_url"); }
            }

            /// <summary>
            /// og:image:type
            /// </summary>
            public static RDFResource IMAGE_TYPE {
                get { return new RDFResource(OG.BASE_URI + "image:type"); }
            }

            /// <summary>
            /// og:image:width
            /// </summary>
            public static RDFResource IMAGE_WIDTH {
                get { return new RDFResource(OG.BASE_URI + "image:width"); }
            }

            /// <summary>
            /// og:image:height
            /// </summary>
            public static RDFResource IMAGE_HEIGHT {
                get { return new RDFResource(OG.BASE_URI + "image:height"); }
            }

            /// <summary>
            /// og:audio
            /// </summary>
            public static RDFResource AUDIO {
                get { return new RDFResource(OG.BASE_URI + "audio"); }
            }

            /// <summary>
            /// og:audio:secure_url
            /// </summary>
            public static RDFResource AUDIO_SECURE_URL {
                get { return new RDFResource(OG.BASE_URI + "audio:secure_url"); }
            }

            /// <summary>
            /// og:audio:type
            /// </summary>
            public static RDFResource AUDIO_TYPE {
                get { return new RDFResource(OG.BASE_URI + "audio:type"); }
            }

            /// <summary>
            /// og:description
            /// </summary>
            public static RDFResource DESCRIPTION {
                get { return new RDFResource(OG.BASE_URI + "description"); }
            }

            /// <summary>
            /// og:determiner
            /// </summary>
            public static RDFResource DETERMINER {
                get { return new RDFResource(OG.BASE_URI + "determiner"); }
            }

            /// <summary>
            /// og:locale
            /// </summary>
            public static RDFResource LOCALE {
                get { return new RDFResource(OG.BASE_URI + "locale"); }
            }

            /// <summary>
            /// og:locale:alternate
            /// </summary>
            public static RDFResource LOCALE_ALTERNATE {
                get { return new RDFResource(OG.BASE_URI + "locale:alternate"); }
            }

            /// <summary>
            /// og:site_name
            /// </summary>
            public static RDFResource SITE_NAME {
                get { return new RDFResource(OG.BASE_URI + "site_name"); }
            }

            /// <summary>
            /// og:video
            /// </summary>
            public static RDFResource VIDEO {
                get { return new RDFResource(OG.BASE_URI + "video"); }
            }

            /// <summary>
            /// og:video:secure_url
            /// </summary>
            public static RDFResource VIDEO_SECURE_URL {
                get { return new RDFResource(OG.BASE_URI + "video:secure_url"); }
            }

            /// <summary>
            /// og:video:type
            /// </summary>
            public static RDFResource VIDEO_TYPE {
                get { return new RDFResource(OG.BASE_URI + "video:type"); }
            }

            /// <summary>
            /// og:video:width
            /// </summary>
            public static RDFResource VIDEO_WIDTH {
                get { return new RDFResource(OG.BASE_URI + "video:width"); }
            }

            /// <summary>
            /// og:video:height
            /// </summary>
            public static RDFResource VIDEO_HEIGHT {
                get { return new RDFResource(OG.BASE_URI + "video:height"); }
            }
            #endregion

            #region Extended Properties

            #region OG_MUSIC
            /// <summary>
            /// OG_MUSIC represents music-related concepts in Open Graph Protocol Vocabulary
            /// </summary>
            public static class OG_MUSIC {

                #region Properties
                /// <summary>
                /// ogm
                /// </summary>
                public static String PREFIX {
                    get { return "ogm"; }
                }

                /// <summary>
                /// xmlns:ogm
                /// </summary>
                public static String BASE_URI {
                    get { return "http://ogp.me/ns/music#"; }
                }

                /// <summary>
                /// ogm:song
                /// </summary>
                public static RDFResource SONG {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "song"); }
                }

                /// <summary>
                /// ogm:song:disc
                /// </summary>
                public static RDFResource SONG_DISC {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "song:disc"); }
                }

                /// <summary>
                /// ogm:song:track
                /// </summary>
                public static RDFResource SONG_TRACK {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "song:track"); }
                }

                /// <summary>
                /// ogm:duration
                /// </summary>
                public static RDFResource DURATION {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "duration"); }
                }

                /// <summary>
                /// ogm:album
                /// </summary>
                public static RDFResource ALBUM {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "album"); }
                }

                /// <summary>
                /// ogm:album:disc
                /// </summary>
                public static RDFResource ALBUM_DISC {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "album:disc"); }
                }

                /// <summary>
                /// ogm:album:track
                /// </summary>
                public static RDFResource ALBUM_TRACK {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "album:track"); }
                }

                /// <summary>
                /// ogm:musician
                /// </summary>
                public static RDFResource MUSICIAN {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "musician"); }
                }

                /// <summary>
                /// ogm:release_date
                /// </summary>
                public static RDFResource RELEASE_DATE {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "release_date"); }
                }

                /// <summary>
                /// ogm:playlist
                /// </summary>
                public static RDFResource PLAYLIST {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "playlist"); }
                }

                /// <summary>
                /// ogm:creator
                /// </summary>
                public static RDFResource CREATOR {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "creator"); }
                }

                /// <summary>
                /// ogm:radio_station
                /// </summary>
                public static RDFResource RADIO_STATION {
                    get { return new RDFResource(OG_MUSIC.BASE_URI + "radio_station"); }
                }
                #endregion

            }
            #endregion

            #region OG_VIDEO
            /// <summary>
            /// OG_VIDEO represents video-related concepts in Open Graph Protocol Vocabulary
            /// </summary>
            public static class OG_VIDEO {

                #region Properties
                /// <summary>
                /// ogv
                /// </summary>
                public static String PREFIX {
                    get { return "ogv"; }
                }

                /// <summary>
                /// xmlns:ogv
                /// </summary>
                public static String BASE_URI {
                    get { return "http://ogp.me/ns/video#"; }
                }

                /// <summary>
                /// ogv:movie
                /// </summary>
                public static RDFResource MOVIE {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "movie"); }
                }

                /// <summary>
                /// ogv:actor
                /// </summary>
                public static RDFResource ACTOR {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "actor"); }
                }

                /// <summary>
                /// ogv:actor:role
                /// </summary>
                public static RDFResource ACTOR_ROLE {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "actor:role"); }
                }

                /// <summary>
                /// ogv:director
                /// </summary>
                public static RDFResource DIRECTOR {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "director"); }
                }

                /// <summary>
                /// ogv:writer
                /// </summary>
                public static RDFResource WRITER {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "writer"); }
                }

                /// <summary>
                /// ogv:duration
                /// </summary>
                public static RDFResource DURATION {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "duration"); }
                }

                /// <summary>
                /// ogv:release_date
                /// </summary>
                public static RDFResource RELEASE_DATE {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "release_date"); }
                }

                /// <summary>
                /// ogv:tag
                /// </summary>
                public static RDFResource TAG {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "tag"); }
                }

                /// <summary>
                /// ogv:episode
                /// </summary>
                public static RDFResource EPISODE {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "episode"); }
                }

                /// <summary>
                /// ogv:series
                /// </summary>
                public static RDFResource SERIES {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "series"); }
                }

                /// <summary>
                /// ogv:tv_show
                /// </summary>
                public static RDFResource TV_SHOW {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "tv_show"); }
                }

                /// <summary>
                /// ogv:other
                /// </summary>
                public static RDFResource OTHER {
                    get { return new RDFResource(OG_VIDEO.BASE_URI + "other"); }
                }
                #endregion

            }
            #endregion

            #region OG_ARTICLE
            /// <summary>
            /// OG_ARTICLE represents article-related concepts in Open Graph Protocol Vocabulary
            /// </summary>
            public static class OG_ARTICLE {

                #region Properties
                /// <summary>
                /// oga
                /// </summary>
                public static String PREFIX {
                    get { return "oga"; }
                }

                /// <summary>
                /// xmlns:oga
                /// </summary>
                public static String BASE_URI {
                    get { return "http://ogp.me/ns/article#"; }
                }

                /// <summary>
                /// oga:published_time
                /// </summary>
                public static RDFResource PUBLISHED_TIME {
                    get { return new RDFResource(OG_ARTICLE.BASE_URI + "published_time"); }
                }

                /// <summary>
                /// oga:modified_time
                /// </summary>
                public static RDFResource MODIFIED_TIME {
                    get { return new RDFResource(OG_ARTICLE.BASE_URI + "modified_time"); }
                }

                /// <summary>
                /// oga:expiration_time
                /// </summary>
                public static RDFResource EXPIRATION_TIME {
                    get { return new RDFResource(OG_ARTICLE.BASE_URI + "expiration_time"); }
                }

                /// <summary>
                /// oga:author
                /// </summary>
                public static RDFResource AUTHOR {
                    get { return new RDFResource(OG_ARTICLE.BASE_URI + "author"); }
                }

                /// <summary>
                /// oga:section
                /// </summary>
                public static RDFResource SECTION {
                    get { return new RDFResource(OG_ARTICLE.BASE_URI + "section"); }
                }

                /// <summary>
                /// oga:tag
                /// </summary>
                public static RDFResource TAG {
                    get { return new RDFResource(OG_ARTICLE.BASE_URI + "tag"); }
                }
                #endregion

            }
            #endregion

            #region OG_BOOK
            /// <summary>
            /// OG_BOOK represents book-related concepts in Open Graph Protocol Vocabulary
            /// </summary>
            public static class OG_BOOK {

                #region Properties
                /// <summary>
                /// ogb
                /// </summary>
                public static String PREFIX {
                    get { return "ogb"; }
                }

                /// <summary>
                /// xmlns:ogb
                /// </summary>
                public static String BASE_URI {
                    get { return "http://ogp.me/ns/book#"; }
                }

                /// <summary>
                /// ogb:author
                /// </summary>
                public static RDFResource AUTHOR {
                    get { return new RDFResource(OG_BOOK.BASE_URI + "author"); }
                }

                /// <summary>
                /// ogb:isbn
                /// </summary>
                public static RDFResource ISBN {
                    get { return new RDFResource(OG_BOOK.BASE_URI + "isbn"); }
                }

                /// <summary>
                /// ogb:release_date
                /// </summary>
                public static RDFResource RELEASE_DATE {
                    get { return new RDFResource(OG_BOOK.BASE_URI + "release_date"); }
                }

                /// <summary>
                /// ogb:tag
                /// </summary>
                public static RDFResource TAG {
                    get { return new RDFResource(OG_BOOK.BASE_URI + "tag"); }
                }
                #endregion

            }
            #endregion

            #region OG_PROFILE
            /// <summary>
            /// OG_PROFILE represents profile-related concepts in Open Graph Protocol Vocabulary
            /// </summary>
            public static class OG_PROFILE {

                #region Properties
                /// <summary>
                /// ogp
                /// </summary>
                public static String PREFIX {
                    get { return "ogp"; }
                }

                /// <summary>
                /// xmlns:ogp
                /// </summary>
                public static String BASE_URI {
                    get { return "http://ogp.me/ns/profile#"; }
                }

                /// <summary>
                /// ogp:first_name
                /// </summary>
                public static RDFResource FIRST_NAME {
                    get { return new RDFResource(OG_PROFILE.BASE_URI + "first_name"); }
                }

                /// <summary>
                /// ogp:last_name
                /// </summary>
                public static RDFResource LAST_NAME {
                    get { return new RDFResource(OG_PROFILE.BASE_URI + "last_name"); }
                }

                /// <summary>
                /// ogp:username
                /// </summary>
                public static RDFResource USERNAME {
                    get { return new RDFResource(OG_PROFILE.BASE_URI + "username"); }
                }

                /// <summary>
                /// ogp:gender
                /// </summary>
                public static RDFResource GENDER {
                    get { return new RDFResource(OG_PROFILE.BASE_URI + "gender"); }
                }
                #endregion

            }
            #endregion

            #region OG_WEBSITE
            /// <summary>
            /// OG_WEBSITE represents website-related concepts in Open Graph Protocol Vocabulary
            /// </summary>
            public static class OG_WEBSITE {

                #region Properties
                /// <summary>
                /// ogw
                /// </summary>
                public static String PREFIX {
                    get { return "ogw"; }
                }

                /// <summary>
                /// xmlns:ogw
                /// </summary>
                public static String BASE_URI {
                    get { return "http://ogp.me/ns/website#"; }
                }
                #endregion

            }
            #endregion

            #endregion

        }
        #endregion

        #endregion

    }

}