

using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents a OWL Node of type owl:Class
	/// </summary>
	public interface IOwlClass : IOwlResource
	{
        
	}
}
