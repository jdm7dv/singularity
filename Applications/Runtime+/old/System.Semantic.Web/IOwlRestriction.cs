
using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents a OWL Node of type owl:Restriction
	/// </summary>
	public interface IOwlRestriction : IOwlClass
	{

	}
}