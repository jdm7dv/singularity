
using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents a OWL Node of type owl:Property
	/// </summary>
	public interface IOwlProperty : IOwlResource
	{

	}
}