
using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents a OWL Node of type owl:Individual
	/// </summary>
	public interface IOwlIndividual : IOwlResource
	{

	}
}