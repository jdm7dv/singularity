
using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents a OWL Node of type owl:Datatype
	/// </summary>
	public interface IOwlDatatype : IOwlResource
	{

	}
}