

using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents an OWL List.
	/// </summary>
	public interface IOwlCollection : IOwlResource
	{

	}
}
