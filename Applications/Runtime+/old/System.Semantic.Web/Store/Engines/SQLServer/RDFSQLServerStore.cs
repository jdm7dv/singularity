﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web.Semantic.Model;

namespace System.Web.Semantic.Store
{

    /// <summary>
    /// RDFSQLServerStore represents a store backed on SQL Server engine
    /// </summary>
    public class RDFSQLServerStore: RDFStore {

        #region Properties
        /// <summary>
        /// Connection to the SQL Server database
        /// </summary>
        public SqlConnection Connection { get; internal set; }

        /// <summary>
        /// Dictionary of commands to manipulate the SQL Server database
        /// </summary>
        private Dictionary<String, SqlCommand> Commands { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build a local SQL Server store instance ("LOCALHOST\\SQLEXPRESS")
        /// </summary>
        public RDFSQLServerStore(String sqlServerDatabase): this("LOCALHOST\\SQLEXPRESS", sqlServerDatabase) { }

        /// <summary>
        /// Default-ctor to build a SQL Server store instance
        /// </summary>
        public RDFSQLServerStore(String sqlServerInstance, String sqlServerDatabase) {
            if(sqlServerInstance != null && sqlServerInstance.Trim() != String.Empty){
                sqlServerInstance     = sqlServerInstance.Trim();
                if(sqlServerDatabase != null && sqlServerDatabase.Trim() != String.Empty) {
                    sqlServerDatabase = sqlServerDatabase.Trim();

                    //Initialize store structures to SQL Server values
                    this.StoreType    = "SQLSERVER";
                    this.Connection   = new SqlConnection(@"Server=" + sqlServerInstance + ";Database=" + sqlServerDatabase + ";Integrated Security=true;");
                    this.Commands     = new Dictionary<String, SqlCommand>();
                    this.StoreID      = RDFModelUtilities.CreateHash(this.ToString());                       

                    #region PrepareCommands
                    //SELECT
                    this.Commands.Add("SELECT",   new SqlCommand());
                        this.Commands["SELECT"].Connection    = this.Connection;
                        this.Commands["SELECT"].CommandType   = CommandType.Text;
                        this.Commands["SELECT"].CommandText   = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //INSERT
                    this.Commands.Add("INSERT",   new SqlCommand());
                        this.Commands["INSERT"].Connection    = this.Connection;
                        this.Commands["INSERT"].CommandType   = CommandType.Text;
                        this.Commands["INSERT"].CommandText = "IF NOT EXISTS(SELECT 1 FROM dbo.Quadruples WHERE QuadrupleID = @QID) BEGIN INSERT INTO dbo.Quadruples(QuadrupleID, TripleFlavor, Context, ContextID, Subject, SubjectID, Predicate, PredicateID, Object, ObjectID) VALUES (@QID, @TFV, @CTX, @CTXID, @SUBJ, @SUBJID, @PRED, @PREDID, @OBJ, @OBJID) END";
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("TFV",    SqlDbType.Int));
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("CTX",    SqlDbType.VarChar, -1));
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("CTXID",  SqlDbType.BigInt));
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("SUBJ",   SqlDbType.VarChar, -1));
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("SUBJID", SqlDbType.BigInt));
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("PRED",   SqlDbType.VarChar, -1));
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("PREDID", SqlDbType.BigInt));
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("OBJ",    SqlDbType.VarChar, -1));
                        this.Commands["INSERT"].Parameters.Add(new SqlParameter("OBJID",  SqlDbType.BigInt));
                    //DELETE
                    this.Commands.Add("DELETE",   new SqlCommand());
                        this.Commands["DELETE"].CommandType   = CommandType.Text;
                        this.Commands["DELETE"].Connection    = this.Connection;
                        this.Commands["DELETE"].CommandText   = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                        this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                    //DIAGNOSTICS
                    this.Commands.Add("DIAGNOSTICS", new SqlCommand());
                        this.Commands["DIAGNOSTICS"].Connection  = this.Connection;
                        this.Commands["DIAGNOSTICS"].CommandType = CommandType.Text;
                        this.Commands["DIAGNOSTICS"].CommandText = "SELECT COUNT(*) FROM sys.tables WHERE name='Quadruples' AND type_desc='USER_TABLE';";
                    //PREPARE
                    this.Commands.Add("PREPARE", new SqlCommand());
                        this.Commands["PREPARE"].Connection   = this.Connection;
                        this.Commands["PREPARE"].CommandType  = CommandType.Text;
                        this.Commands["PREPARE"].CommandText = "CREATE TABLE dbo.Quadruples (QuadrupleID BIGINT PRIMARY KEY NOT NULL, TripleFlavor INTEGER NOT NULL, Context VARCHAR(MAX) NOT NULL, ContextID BIGINT NOT NULL, Subject VARCHAR(MAX) NOT NULL, SubjectID BIGINT NOT NULL, Predicate VARCHAR(MAX) NOT NULL, PredicateID BIGINT NOT NULL, Object VARCHAR(MAX) NOT NULL, ObjectID BIGINT NOT NULL);";
                        this.Commands["PREPARE"].CommandText += "CREATE NONCLUSTERED INDEX IDX_TripleFlavor ON dbo.Quadruples(TripleFlavor ASC) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];";
                        this.Commands["PREPARE"].CommandText += "CREATE NONCLUSTERED INDEX IDX_ContextID    ON dbo.Quadruples(ContextID ASC)    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];";
                        this.Commands["PREPARE"].CommandText += "CREATE NONCLUSTERED INDEX IDX_SubjectID    ON dbo.Quadruples(SubjectID ASC)    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];";
                        this.Commands["PREPARE"].CommandText += "CREATE NONCLUSTERED INDEX IDX_PredicateID  ON dbo.Quadruples(PredicateID ASC)  WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];";
                        this.Commands["PREPARE"].CommandText += "CREATE NONCLUSTERED INDEX IDX_ObjectID     ON dbo.Quadruples(ObjectID ASC)     WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];";
                    #endregion

                    #region Diagnostics
                    this.PrepareStore();
                    #endregion

                }
                else {
                    throw new RDFStoreException("Cannot connect to SQL Server store because given \"sqlServerDatabase\" parameter is null or empty");
                }
            }
            else {
                throw new RDFStoreException("Cannot connect to SQL Server store because given \"sqlServerInstance\" parameter is null or empty");
            }
        }
        #endregion

        #region Interfaces
        /// <summary>
        /// Gives the string representation of the SQL Server store 
        /// </summary>
        public override String ToString() {
            return base.ToString() + "|CONNECTION=" + this.Connection.ConnectionString;
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Merges the given graph into the store, avoiding duplicate insertions
        /// </summary>
        public override RDFStore MergeGraph(RDFGraph graph) {
            if (graph != null) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Open transaction
                    this.Commands["INSERT"].Transaction = this.Commands["INSERT"].Connection.BeginTransaction();
                    //Prepare command
                    this.Commands["INSERT"].Prepare();
                    //Iterate graph triples
                    RDFContext graphCtx     = new RDFContext(graph.Context);
                    RDFQuadruple quadruple  = null;
                    foreach(RDFTriple t in graph) {
                        if (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                            quadruple       = new RDFQuadruple(graphCtx, (RDFResource)t.Subject, (RDFResource)t.Predicate, (RDFResource)t.Object);
                        }
                        else {
                            quadruple       = new RDFQuadruple(graphCtx, (RDFResource)t.Subject, (RDFResource)t.Predicate, (RDFLiteral)t.Object);
                        }
                        //Valorize parameters
                        this.Commands["INSERT"].Parameters["QID"].Value       = quadruple.QuadrupleID;
                        this.Commands["INSERT"].Parameters["TFV"].Value       = quadruple.TripleFlavor;
                        this.Commands["INSERT"].Parameters["CTX"].Value       = quadruple.Context.ToString();
                        this.Commands["INSERT"].Parameters["CTXID"].Value     = quadruple.Context.PatternMemberID;
                        this.Commands["INSERT"].Parameters["SUBJ"].Value      = quadruple.Subject.ToString();
                        this.Commands["INSERT"].Parameters["SUBJID"].Value    = quadruple.Subject.PatternMemberID;
                        this.Commands["INSERT"].Parameters["PRED"].Value      = quadruple.Predicate.ToString();
                        this.Commands["INSERT"].Parameters["PREDID"].Value    = quadruple.Predicate.PatternMemberID;
                        this.Commands["INSERT"].Parameters["OBJ"].Value       = quadruple.Object.ToString();
                        this.Commands["INSERT"].Parameters["OBJID"].Value     = quadruple.Object.PatternMemberID;
                        //Execute command
                        this.Commands["INSERT"].ExecuteNonQuery();
                    }
                    //Close transaction
                    this.Commands["INSERT"].Transaction.Commit();
                    //Close connection
                    this.Connection.Close();
                }
                catch (Exception ex) {
                    //Close transaction
                    this.Commands["INSERT"].Transaction.Rollback();
                    //Close connection
                    this.Connection.Close();
                    //Propagate exception
                    throw new RDFStoreException("Cannot insert data into SQL Server store because: " + ex.Message, ex);
                }
            }
            return this;
        }

        /// <summary>
        /// Adds the given quadruple to the store, avoiding duplicate insertions
        /// </summary>
        public override RDFStore AddQuadruple(RDFQuadruple quadruple) {
            if (quadruple != null) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Open transaction
                    this.Commands["INSERT"].Transaction                        = this.Commands["INSERT"].Connection.BeginTransaction();
                    //Prepare command
                    this.Commands["INSERT"].Prepare();
                    //Valorize parameters
                    this.Commands["INSERT"].Parameters["QID"].Value            = quadruple.QuadrupleID;
                    this.Commands["INSERT"].Parameters["TFV"].Value            = quadruple.TripleFlavor;
                    this.Commands["INSERT"].Parameters["CTX"].Value            = quadruple.Context.ToString();
                    this.Commands["INSERT"].Parameters["CTXID"].Value          = quadruple.Context.PatternMemberID;
                    this.Commands["INSERT"].Parameters["SUBJ"].Value           = quadruple.Subject.ToString();
                    this.Commands["INSERT"].Parameters["SUBJID"].Value         = quadruple.Subject.PatternMemberID;
                    this.Commands["INSERT"].Parameters["PRED"].Value           = quadruple.Predicate.ToString();
                    this.Commands["INSERT"].Parameters["PREDID"].Value         = quadruple.Predicate.PatternMemberID;
                    this.Commands["INSERT"].Parameters["OBJ"].Value            = quadruple.Object.ToString();
                    this.Commands["INSERT"].Parameters["OBJID"].Value          = quadruple.Object.PatternMemberID;
                    //Execute command
                    this.Commands["INSERT"].ExecuteNonQuery();
                    //Close transaction
                    this.Commands["INSERT"].Transaction.Commit();
                    //Close connection
                    this.Connection.Close();
                }
                catch (Exception ex) {
                    //Close transaction
                    this.Commands["INSERT"].Transaction.Rollback();
                    //Close connection
                    this.Connection.Close();
                    //Propagate exception
                    throw new RDFStoreException("Cannot insert data into SQL Server store because: " + ex.Message, ex);
                }
            }
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Removes the given quadruples from the store
        /// </summary>
        public override RDFStore RemoveQuadruple(RDFQuadruple quadruple) {
            if (quadruple != null) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Open transaction
                    this.Commands["DELETE"].Transaction                 = this.Commands["DELETE"].Connection.BeginTransaction();
                    //Prepare command
                    this.Commands["DELETE"].Prepare();
                    //Valorize parameters
                    this.Commands["DELETE"].Parameters["QID"].Value     = quadruple.QuadrupleID;
                    //Execute command
                    this.Commands["DELETE"].ExecuteNonQuery();
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Commit();
                    //Close connection
                    this.Connection.Close();
                }
                catch (Exception ex) {
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Rollback();
                    //Close connection
                    this.Connection.Close();
                    //Propagate exception
                    throw new RDFStoreException("Cannot delete data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return this;
        }

        /// <summary>
        /// Removes the quadruples with the given context
        /// </summary>
        public override RDFStore RemoveQuadruplesByContext(RDFContext contextResource) {
            if (contextResource   != null) {
                try {
                    //Modify command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE ContextID = @CTXID";
                    this.Commands["DELETE"].Parameters.Clear();
                    //Open connection
                    this.Connection.Open();
                    //Open transaction
                    this.Commands["DELETE"].Transaction                 = this.Commands["DELETE"].Connection.BeginTransaction();
                    //Prepare command
                    this.Commands["DELETE"].Prepare();
                    //Valorize parameters
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("CTXID",  SqlDbType.BigInt));
                    this.Commands["DELETE"].Parameters["CTXID"].Value   = contextResource.PatternMemberID;
                    //Execute command
                    this.Commands["DELETE"].ExecuteNonQuery();
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Commit();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt)); 
                }
                catch (Exception ex) {
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Rollback();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                    //Propagate exception
                    throw new RDFStoreException("Cannot delete data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return this;
        }

        /// <summary>
        /// Removes the quadruples with the given subject
        /// </summary>
        public override RDFStore RemoveQuadruplesBySubject(RDFResource subjectResource) {
            if (subjectResource   != null) {
                try {
                    //Modify command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE SubjectID = @SUBJID";
                    this.Commands["DELETE"].Parameters.Clear();
                    //Open connection
                    this.Connection.Open();
                    //Open transaction
                    this.Commands["DELETE"].Transaction                 = this.Commands["DELETE"].Connection.BeginTransaction();
                    //Prepare command
                    this.Commands["DELETE"].Prepare();
                    //Valorize parameters
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("SUBJID",  SqlDbType.BigInt));
                    this.Commands["DELETE"].Parameters["SUBJID"].Value  = subjectResource.PatternMemberID;
                    //Execute command
                    this.Commands["DELETE"].ExecuteNonQuery();
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Commit();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                }
                catch (Exception ex) {
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Rollback();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                    //Propagate exception
                    throw new RDFStoreException("Cannot delete data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return this;
        }

        /// <summary>
        /// Removes the quadruples with the given (non-blank) predicate
        /// </summary>
        public override RDFStore RemoveQuadruplesByPredicate(RDFResource predicateResource) {
            if (predicateResource != null && !predicateResource.IsBlank) {
                try {
                    //Modify command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE PredicateID = @PREDID";
                    this.Commands["DELETE"].Parameters.Clear();
                    //Open connection
                    this.Connection.Open();
                    //Open transaction
                    this.Commands["DELETE"].Transaction                 = this.Commands["DELETE"].Connection.BeginTransaction();
                    //Prepare command
                    this.Commands["DELETE"].Prepare();
                    //Valorize parameters
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("PREDID",  SqlDbType.BigInt));
                    this.Commands["DELETE"].Parameters["PREDID"].Value  = predicateResource.PatternMemberID;
                    //Execute command
                    this.Commands["DELETE"].ExecuteNonQuery();
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Commit();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                }
                catch (Exception ex) {
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Rollback();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                    //Propagate exception
                    throw new RDFStoreException("Cannot delete data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return this;
        }

        /// <summary>
        /// Removes the quadruples with the given resource as object
        /// </summary>
        public override RDFStore RemoveQuadruplesByObject(RDFResource objectResource) {
            if (objectResource    != null) {
                try {
                    //Modify command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE TripleFlavor = 1 AND ObjectID = @OBJID";
                    this.Commands["DELETE"].Parameters.Clear();
                    //Open connection
                    this.Connection.Open();
                    //Open transaction
                    this.Commands["DELETE"].Transaction                 = this.Commands["DELETE"].Connection.BeginTransaction();
                    //Prepare command
                    this.Commands["DELETE"].Prepare();
                    //Valorize parameters
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("OBJID",  SqlDbType.BigInt));
                    this.Commands["DELETE"].Parameters["OBJID"].Value   = objectResource.PatternMemberID;
                    //Execute command
                    this.Commands["DELETE"].ExecuteNonQuery();
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Commit();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                }
                catch (Exception ex) {
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Rollback();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                    //Propagate exception
                    throw new RDFStoreException("Cannot delete data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return this;
        }

        /// <summary>
        /// Removes the quadruples with the given literal as object
        /// </summary>
        public override RDFStore RemoveQuadruplesByLiteral(RDFLiteral literalObject) {
            if (literalObject     != null) {
                try {
                    //Modify command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE TripleFlavor = 2 AND ObjectID = @OBJID";
                    this.Commands["DELETE"].Parameters.Clear();
                    //Open connection
                    this.Connection.Open();
                    //Open transaction
                    this.Commands["DELETE"].Transaction                 = this.Commands["DELETE"].Connection.BeginTransaction();
                    //Prepare command
                    this.Commands["DELETE"].Prepare();
                    //Valorize parameters
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("OBJID",  SqlDbType.BigInt));
                    this.Commands["DELETE"].Parameters["OBJID"].Value   = literalObject.PatternMemberID;
                    //Execute command
                    this.Commands["DELETE"].ExecuteNonQuery();
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Commit();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                }
                catch (Exception ex) {
                    //Close transaction
                    this.Commands["DELETE"].Transaction.Rollback();
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["DELETE"].CommandText                 = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                    this.Commands["DELETE"].Parameters.Clear();
                    this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID",    SqlDbType.BigInt));
                    //Propagate exception
                    throw new RDFStoreException("Cannot delete data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return this;
        }

        /// <summary>
        /// Clears the quadruples of the store
        /// </summary>
        public override RDFStore ClearQuadruples() {
            try {
                //Modify command
                this.Commands["DELETE"].CommandText = "DELETE FROM dbo.Quadruples";
                this.Commands["DELETE"].Parameters.Clear();
                //Open connection
                this.Connection.Open();
                //Open transaction
                this.Commands["DELETE"].Transaction = this.Commands["DELETE"].Connection.BeginTransaction();
                //Execute command
                this.Commands["DELETE"].ExecuteNonQuery();
                //Close transaction
                this.Commands["DELETE"].Transaction.Commit();
                //Close connection
                this.Connection.Close();
                //Restore command
                this.Commands["DELETE"].CommandText = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID", SqlDbType.BigInt)); 
            }
            catch (Exception ex) {
                //Close transaction
                this.Commands["DELETE"].Transaction.Rollback();
                //Close connection
                this.Connection.Close();
                //Restore command
                this.Commands["DELETE"].CommandText = "DELETE FROM dbo.Quadruples WHERE QuadrupleID = @QID";
                this.Commands["DELETE"].Parameters.Add(new SqlParameter("QID", SqlDbType.BigInt));
                //Propagate exception
                throw new RDFStoreException("Cannot delete data from SQL Server store because: " + ex.Message, ex);
            }
            return this;
        }
        #endregion

        #region Select
        /// <summary>
        /// Checks if the store contains the given quadruple
        /// </summary>
        public override Boolean ContainsQuadruple(RDFQuadruple quadruple) {
            RDFMemoryStore result = new RDFMemoryStore();
            if (quadruple        != null) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Modify command
                    this.Commands["SELECT"].CommandText              += " WHERE QuadrupleID = @QID";
                    //Add parameters
                    this.Commands["SELECT"].Parameters.Add(new SqlParameter("QID", SqlDbType.BigInt));
                    //Prepare command
                    this.Commands["SELECT"].Prepare();
                    //Valorize parameters
                    this.Commands["SELECT"].Parameters["QID"].Value   = quadruple.QuadrupleID;
                    //Execute command
                    using (SqlDataReader fetchedQuadruples            = this.Commands["SELECT"].ExecuteReader()) {
                        if (fetchedQuadruples.HasRows) {
                            //Iterate fetched quadruples
                            while (fetchedQuadruples.Read()) {
                                //Add the fetched quadruple to the result
                                result.AddQuadruple(RDFStoreUtilities.ParseQuadruple(fetchedQuadruples));
                            }
                        }
                    }
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText               = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                }
                catch (Exception ex) {
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText               = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                    //Propagate exception
                    throw new RDFStoreException("Cannot read data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return (result.QuadruplesCount > 0);
        }

        /// <summary>
        /// Gets a store containing all quadruples
        /// </summary>
        public override RDFStore SelectAllQuadruples() {
            RDFMemoryStore result                         = new RDFMemoryStore();
            try {
                //Open connection
                this.Connection.Open();
                //Execute command
                using (SqlDataReader fetchedQuadruples    = this.Commands["SELECT"].ExecuteReader()) {
                    if (fetchedQuadruples.HasRows) {
                        //Iterate fetched quadruples
                        while (fetchedQuadruples.Read()) {
                            //Add the fetched quadruple to the result
                            result.AddQuadruple(RDFStoreUtilities.ParseQuadruple(fetchedQuadruples));
                        }
                    }
                }
                //Close connection
                this.Connection.Close();
            }
            catch (Exception ex) {
                //Close connection
                this.Connection.Close();
                //Propagate exception
                throw new RDFStoreException("Cannot read data from SQL Server store because: " + ex.Message, ex);
            }
            return result;
        }

        /// <summary>
        /// Gets a memory store containing quadruples with the specified context
        /// </summary>
        public override RDFStore SelectQuadruplesByContext(RDFContext contextResource) {
            RDFMemoryStore result = new RDFMemoryStore();
            if (contextResource  != null) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Modify command
                    this.Commands["SELECT"].CommandText              += " WHERE ContextID = @CTXID";
                    //Add parameters
                    this.Commands["SELECT"].Parameters.Add(new SqlParameter("CTXID", SqlDbType.BigInt));
                    //Prepare command
                    this.Commands["SELECT"].Prepare();
                    //Valorize parameters
                    this.Commands["SELECT"].Parameters["CTXID"].Value = contextResource.PatternMemberID;
                    //Execute command
                    using (SqlDataReader fetchedQuadruples            = this.Commands["SELECT"].ExecuteReader()) {
                        if (fetchedQuadruples.HasRows) {
                            //Iterate fetched quadruples
                            while (fetchedQuadruples.Read()) {
                                //Add the fetched quadruple to the result
                                result.AddQuadruple(RDFStoreUtilities.ParseQuadruple(fetchedQuadruples));
                            }
                        }
                    }
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText               = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                }
                catch (Exception ex) {
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText               = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                    //Propagate exception
                    throw new RDFStoreException("Cannot read data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a memory store containing quadruples with the specified subject
        /// </summary>
        public override RDFStore SelectQuadruplesBySubject(RDFResource subjectResource) {
            RDFMemoryStore result = new RDFMemoryStore();
            if (subjectResource  != null) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Modify command
                    this.Commands["SELECT"].CommandText               += " WHERE SubjectID = @SUBJID";
                    //Add parameters
                    this.Commands["SELECT"].Parameters.Add(new SqlParameter("SUBJID", SqlDbType.BigInt));
                    //Prepare command
                    this.Commands["SELECT"].Prepare();
                    //Valorize parameters
                    this.Commands["SELECT"].Parameters["SUBJID"].Value = subjectResource.PatternMemberID;
                    //Execute command
                    using (SqlDataReader fetchedQuadruples             = this.Commands["SELECT"].ExecuteReader()) {
                        if (fetchedQuadruples.HasRows) {
                            //Iterate fetched quadruples
                            while (fetchedQuadruples.Read()) {
								//Add the fetched quadruple to the result
                                result.AddQuadruple(RDFStoreUtilities.ParseQuadruple(fetchedQuadruples));
                            }
                        }
                    }
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText                = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                }
                catch (Exception ex) {
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText                = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                    //Propagate exception
                    throw new RDFStoreException("Cannot read data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a memory store containing quadruples with the specified predicate
        /// </summary>
        public override RDFStore SelectQuadruplesByPredicate(RDFResource predicateResource) {
            RDFMemoryStore result  = new RDFMemoryStore();
            if (predicateResource != null) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Modify command
                    this.Commands["SELECT"].CommandText               += " WHERE PredicateID = @PREDID";
                    //Add parameters
                    this.Commands["SELECT"].Parameters.Add(new SqlParameter("PREDID", SqlDbType.BigInt));
                    //Prepare command
                    this.Commands["SELECT"].Prepare();
                    //Valorize parameters
                    this.Commands["SELECT"].Parameters["PREDID"].Value = predicateResource.PatternMemberID;
                    //Execute command
                    using (SqlDataReader fetchedQuadruples             = this.Commands["SELECT"].ExecuteReader()) {
                        if (fetchedQuadruples.HasRows) {
                            //Iterate fetched quadruples
                            while (fetchedQuadruples.Read()) {
                                //Add the fetched quadruple to the result
                                result.AddQuadruple(RDFStoreUtilities.ParseQuadruple(fetchedQuadruples));
                            }
                        }
                    }
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText                = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                }
                catch (Exception ex) {
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText                = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                    //Propagate exception
                    throw new RDFStoreException("Cannot read data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a memory store containing quadruples with the specified object
        /// </summary>
        public override RDFStore SelectQuadruplesByObject(RDFResource objectResource) {
            RDFMemoryStore result = new RDFMemoryStore();
            if (objectResource   != null) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Modify command
                    this.Commands["SELECT"].CommandText              += " WHERE TripleFlavor = 1 AND ObjectID = @OBJID";
                    //Add parameters
                    this.Commands["SELECT"].Parameters.Add(new SqlParameter("OBJID", SqlDbType.BigInt));
                    //Prepare command
                    this.Commands["SELECT"].Prepare();
                    //Valorize parameters
                    this.Commands["SELECT"].Parameters["OBJID"].Value = objectResource.PatternMemberID;
                    //Execute command
                    using (SqlDataReader fetchedQuadruples            = this.Commands["SELECT"].ExecuteReader()) {
                        if (fetchedQuadruples.HasRows) {
                            //Iterate fetched quadruples
                            while (fetchedQuadruples.Read()) {
                                //Add the fetched quadruple to the result
                                result.AddQuadruple(RDFStoreUtilities.ParseQuadruple(fetchedQuadruples));
                            }
                        }
                    }
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText               = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                }
                catch (Exception ex) {
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText               = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                    //Propagate exception
                    throw new RDFStoreException("Cannot read data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a memory store containing quadruples with the specified literal
        /// </summary>
        public override RDFStore SelectQuadruplesByLiteral(RDFLiteral objectLiteral) {
            RDFMemoryStore result = new RDFMemoryStore();
            if (objectLiteral    != null) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Modify command
                    this.Commands["SELECT"].CommandText              += " WHERE TripleFlavor = 2 AND ObjectID = @OBJID";
                    //Add parameters
                    this.Commands["SELECT"].Parameters.Add(new SqlParameter("OBJID", SqlDbType.BigInt));
                    //Prepare command
                    this.Commands["SELECT"].Prepare();
                    //Valorize parameters
                    this.Commands["SELECT"].Parameters["OBJID"].Value = objectLiteral.PatternMemberID;
                    //Execute command
                    using (SqlDataReader fetchedQuadruples            = this.Commands["SELECT"].ExecuteReader()) {
                        if (fetchedQuadruples.HasRows) {
                            //Iterate fetched quadruples
                            while (fetchedQuadruples.Read()) {
                                //Add the fetched quadruple to the result
                                result.AddQuadruple(RDFStoreUtilities.ParseQuadruple(fetchedQuadruples));
                            }
                        }
                    }
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText               = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                }
                catch (Exception ex) {
                    //Close connection
                    this.Connection.Close();
                    //Restore command
                    this.Commands["SELECT"].CommandText               = "SELECT TripleFlavor, Context, Subject, Predicate, Object FROM dbo.Quadruples";
                    //Restore parameters
                    this.Commands["SELECT"].Parameters.Clear();
                    //Propagate exception
                    throw new RDFStoreException("Cannot read data from SQL Server store because: " + ex.Message, ex);
                }
            }
            return result;
        }
        #endregion

		#region Diagnostics
        /// <summary>
        /// Performs the preliminary diagnostics controls on the underlying SQL Server database
        /// </summary>
        private RDFStoreEnums.RDFStoreSQLErrors Diagnostics() {
            try {
                //Open connection
                this.Connection.Open();
                //Execute command
                Int32 result = Int32.Parse(this.Commands["DIAGNOSTICS"].ExecuteScalar().ToString());
                //Close connection
                this.Connection.Close();
                //Return the diagnostics state
                if (result  == 0) {
                    return RDFStoreEnums.RDFStoreSQLErrors.QuadruplesTableNotFound;
                }
				return RDFStoreEnums.RDFStoreSQLErrors.NoErrors;
            }
            catch {
                //Close connection
                this.Connection.Close();
                //Return the diagnostics state
                return RDFStoreEnums.RDFStoreSQLErrors.InvalidDataSource;
            }
        }

        /// <summary>
        /// Prepares the underlying SQL Server database for the use by RDFSharp
        /// </summary>
        private void PrepareStore() {
            RDFStoreEnums.RDFStoreSQLErrors connCheck = this.Diagnostics();

            //Prepare the database only if diagnostics has detected the missing of "Quadruples" table in the store
            if (connCheck      == RDFStoreEnums.RDFStoreSQLErrors.QuadruplesTableNotFound) {
                try {
                    //Open connection
                    this.Connection.Open();
                    //Open transaction
                    this.Commands["PREPARE"].Transaction = this.Commands["PREPARE"].Connection.BeginTransaction();
                    //Execute command
                    this.Commands["PREPARE"].ExecuteNonQuery();
                    //Close transaction
                    this.Commands["PREPARE"].Transaction.Commit();
                    //Close connection
                    this.Connection.Close();
                }
                catch (Exception ex) {
                    //Close transaction
                    this.Commands["PREPARE"].Transaction.Rollback();
                    //Close connection
                    this.Connection.Close();
                    //Propagate exception
                    throw new RDFStoreException("Cannot prepare SQL Server store because: " + ex.Message, ex);
                }

            }

            //Otherwise, an exception must be thrown because it has not been possible to connect to the instance/database
            else if (connCheck == RDFStoreEnums.RDFStoreSQLErrors.InvalidDataSource) {
                throw new RDFStoreException("Cannot prepare SQL Server store because unable to connect to the instance, or to open the database.");
            }

        }
        #endregion		
		
        #endregion

    }

}