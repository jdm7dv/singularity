﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Data;
using System.Web.Semantic.Model;

namespace System.Web.Semantic.Store
{

    /// <summary>
    /// RDFStoreUtilities is a collector of reusable utility methods for RDF store management
    /// </summary>
    public static class RDFStoreUtilities {

        #region Methods
        /// <summary>
        /// Parses the current quadruple of the data reader 
        /// </summary>
        public static RDFQuadruple ParseQuadruple(IDataReader fetchedQuadruples) {

            if (fetchedQuadruples != null) { 
                RDFContext qContext       = new RDFContext(fetchedQuadruples["Context"].ToString());
                RDFResource qSubject      = new RDFResource(fetchedQuadruples["Subject"].ToString());
                RDFResource qPredicate    = new RDFResource(fetchedQuadruples["Predicate"].ToString());

                //SPO-flavour quadruple
                if (fetchedQuadruples["TripleFlavor"].ToString() == "1") {
                    RDFResource qObject   = new RDFResource(fetchedQuadruples["Object"].ToString());
                    return new RDFQuadruple(qContext, qSubject, qPredicate, qObject);
                }

                //SPL-flavour quadruple
                String literal            = fetchedQuadruples["Object"].ToString();

                //PlainLiteral
                if (!literal.Contains("^^") || literal.EndsWith("^^") || RDFModelUtilities.GetUriFromString(literal.Substring(literal.LastIndexOf("^^", StringComparison.Ordinal) + 2)) == null) {
                    RDFPlainLiteral pLit  = null;
                    if (literal.Contains("@")) {
                        if (!literal.EndsWith("@")) {
                            Int32 lastAmp = literal.LastIndexOf('@');
                            pLit          = new RDFPlainLiteral(literal.Substring(0, lastAmp), literal.Substring(lastAmp + 1));
                        }
                        else {
                            pLit          = new RDFPlainLiteral(literal);
                        }
                    }
                    else {
                        pLit              = new RDFPlainLiteral(literal);
                    }
                    return new RDFQuadruple(qContext, qSubject, qPredicate, pLit);
                }

                //TypedLiteral
                String tLitValue          = literal.Substring(0, literal.LastIndexOf("^^", StringComparison.Ordinal));
                String tLitDatatype       = literal.Substring(literal.LastIndexOf("^^", StringComparison.Ordinal) + 2);
                RDFDatatype dt            = RDFModelUtilities.GetDatatypeFromString(tLitDatatype);
                RDFTypedLiteral tLit      = new RDFTypedLiteral(tLitValue, dt);
                return new RDFQuadruple(qContext, qSubject, qPredicate, tLit);
            }
            throw new RDFStoreException("Cannot parse quadruple because given \"fetchedQuadruples\" parameter is null.");

        }
        #endregion

    }

}