
using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents a OWL Node of type owl:Ontology
	/// </summary>
	public interface IOwlOntology : IOwlResource
	{

	}
}