﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics {

    /// <summary>
    /// RDFOntologyValidator applies a set of RDFS/OWL-DL analysis rules on a given ontology, 
    /// with the goal of finding error and inconsistency evidences affecting its model and data.
    /// </summary>
    public class RDFOntologyValidator {

        #region Properties
        /// <summary>
        /// List of rules which are applied by the ontology validator
        /// </summary>
        internal List<RDFOntologyValidatorRule> Rules { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an ontology validator with predefined set of RDFS/OWL-DL rules
        /// </summary>
        public RDFOntologyValidator() {
            this.Rules = new List<RDFOntologyValidatorRule>() { 
            
                //OWL_DL_Vocabulary_Completeness
                new RDFOntologyValidatorRule(
                    "OWL_DL_Vocabulary_Cmpleteness", 
                    "This rule checks for completeness of vocabulary of classes, properties and facts",
                    RDFSemanticsUtilities.OWL_DL_Vocabulary_Completeness),

                //OWL_DL_Vocabulary_Disjointness
                new RDFOntologyValidatorRule(
                    "OWL_DL_Vocabulary_Disjointness", 
                    "This rule checks for disjointness of vocabulary of classes, properties and facts",
                    RDFSemanticsUtilities.OWL_DL_Vocabulary_Disjointness)                

            };
        }
        #endregion

        #region Methods
        /// <summary>
        /// Analyzes the given ontology and produces a detailed report of found Warning/Error evidences
        /// </summary>
        public RDFOntologyValidatorReport AnalyzeOntology(RDFOntology ontology) {
            var report = new RDFOntologyValidatorReport();

            //Step 1: Create the cache for storing the evidences found by the rules
            var rDict  = new Dictionary<RDFOntologyValidatorRule, List<RDFOntologyValidatorEvidence>>();
            foreach (var rule in this.Rules) {
                rDict.Add(rule, new List<RDFOntologyValidatorEvidence>());
            }

            //Step 2: Parallelize the execution of the rules on the given ontology
            Parallel.ForEach(this.Rules, rule => {
                rDict[rule] = rule.ExecuteRule(ontology);
            });

            //Step 3: Merge the evidences into the validator report
            foreach(var rule in rDict) {
                foreach (var evidence in rule.Value) {
                    report.AddEvidence(evidence);
                }
            }

            return report;
        }
        #endregion

    }

    #region RDFOntologyValidatorRule
    /// <summary>
    /// RDFOntologyValidatorRule represents a rule which analyzes a specific syntactic/semantic aspect of an ontology.
    /// </summary>
    public class RDFOntologyValidatorRule {

        #region Properties
        /// <summary>
        /// Name of the rule
        /// </summary>
        public String RuleName { get; internal set; }

        /// <summary>
        /// Description of the analysis logics applied by the rule
        /// </summary>
        public String RuleDescription { get; internal set; }

        /// <summary>
        /// Delegate for the function which will be executed as body of the rule
        /// </summary>
        internal delegate List<RDFOntologyValidatorEvidence> RuleDelegate(RDFOntology ontology);

        /// <summary>
        /// Function which will be effectively executed as body of the rule
        /// </summary>
        internal RuleDelegate ExecuteRule { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty validator rule with given name, description and delegate
        /// </summary>
        internal RDFOntologyValidatorRule(String ruleName,
                                          String ruleDescription,
                                          RuleDelegate ruleDelegate) {
            this.RuleName        = ruleName;
            this.RuleDescription = ruleDescription;
            this.ExecuteRule     = ruleDelegate;
        }
        #endregion

    }
    #endregion

    #region RDFOntologyValidatorEvidence
    /// <summary>
    /// RDFOntologyValidatorEvidence represents an evidence discovered by a rule during the analysis of an ontology.
    /// </summary>
    public class RDFOntologyValidatorEvidence {

        #region Properties
        /// <summary>
        /// Category of the evidence (Warning / Error)
        /// </summary>
        public String EvidenceCategory { get; internal set; }

        /// <summary>
        /// Rule which has produced this evidence
        /// </summary>
        public String EvidenceProvenance { get; internal set; }

        /// <summary>
        /// Message of the evidence
        /// </summary>
        public String EvidenceMessage { get; internal set; }

        /// <summary>
        /// Proposed action for solving the evidence
        /// </summary>
        public String EvidenceSuggestion { get; internal set; }        
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an evidence with given data
        /// </summary>
        internal RDFOntologyValidatorEvidence(String evidenceCategory,
                                              String evidenceProvenance,
                                              String evidenceMessage,
                                              String evidenceSuggestion) {
            this.EvidenceCategory   = evidenceCategory;
            this.EvidenceProvenance = evidenceProvenance;
            this.EvidenceMessage    = evidenceMessage;
            this.EvidenceSuggestion = evidenceSuggestion;
        }
        #endregion

    }
    #endregion

    #region RDFOntologyValidatorReport
    /// <summary>
    /// RDFOntologyValidatorReport represents a detailed report of an ontology analysis.
    /// </summary>
    public class RDFOntologyValidatorReport {

        #region Properties
        /// <summary>
        /// Counter of the Warnings
        /// </summary>
        public Int32 WarningsCount {
            get { return this.Warnings.Count; }
        }

        /// <summary>
        /// Counter of the Errors
        /// </summary>
        public Int32 ErrorsCount {
            get { return this.Errors.Count; }
        }

        /// <summary>
        /// Gets an enumerator on the Warnings for iteration
        /// </summary>
        public IEnumerator<RDFOntologyValidatorEvidence> WarningsEnumerator {
            get { return this.Warnings.GetEnumerator(); }
        }

        /// <summary>
        /// Gets an enumerator on the Errors for iteration
        /// </summary>
        public IEnumerator<RDFOntologyValidatorEvidence> ErrorsEnumerator {
            get { return this.Errors.GetEnumerator(); }
        }

        /// <summary>
        /// List of Warnings
        /// </summary>
        internal List<RDFOntologyValidatorEvidence> Warnings { get; set; }

        /// <summary>
        /// List of Wrrors
        /// </summary>
        internal List<RDFOntologyValidatorEvidence> Errors { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty report
        /// </summary>
        internal RDFOntologyValidatorReport() {
            this.Warnings = new List<RDFOntologyValidatorEvidence>();
            this.Errors   = new List<RDFOntologyValidatorEvidence>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds the given evidence to the report
        /// </summary>
        internal RDFOntologyValidatorReport AddEvidence(RDFOntologyValidatorEvidence validationEvidence) {
            if (validationEvidence.EvidenceCategory.Equals("Error", StringComparison.Ordinal)) {
                this.Errors.Add(validationEvidence);
            }
            else {
                this.Warnings.Add(validationEvidence);
            }
            return this;
        }
        #endregion

    }
    #endregion

}