﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyClass represents a class definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyClass: RDFOntologyResource {

        #region Properties
        /// <summary>
        /// Assertion metadata of the ontology class
        /// </summary>
        public RDFOntologyClassMetadata Assertions { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an ontology class with the given name
        /// </summary>
        public RDFOntologyClass(RDFResource className) {
            if (className != null) {
                this.Value           = className;
                this.PatternMemberID = className.PatternMemberID;
                this.Assertions      = new RDFOntologyClassMetadata();
            }
            else {
                throw new RDFSemanticsException("Cannot create RDFOntologyClass because given \"className\" parameter is null.");
            }
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Sets this ontology class as subClass of the given one
        /// </summary>
        public RDFOntologyClass AddSubClassOf(RDFOntologyClass superClass) {
            this.Assertions.SubClassOf.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.RDFS.SUB_CLASS_OF), superClass));
            return this;
        }

        /// <summary>
        /// Sets this ontology class as equivalentClass of the given one
        /// </summary>
        public RDFOntologyClass AddEquivalentClass(RDFOntologyClass equivalentClass) {
            this.Assertions.EquivalentClass.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.EQUIVALENT_CLASS), equivalentClass));
            return this;
        }

        /// <summary>
        /// Sets this ontology class as disjointClass of the given one
        /// </summary>
        public RDFOntologyClass AddDisjointWith(RDFOntologyClass disjointWith) {
            this.Assertions.DisjointWith.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.DISJOINT_WITH), disjointWith));
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Unsets this ontology class as subClass of the given one
        /// </summary>
        public RDFOntologyClass RemoveSubClassOf(RDFOntologyClass superClass) {
            this.Assertions.SubClassOf.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.RDFS.SUB_CLASS_OF), superClass));
            return this;
        }

        /// <summary>
        /// Unsets this ontology class as equivalentClass of the given one
        /// </summary>
        public RDFOntologyClass RemoveEquivalentClass(RDFOntologyClass equivalentClass) {
            this.Assertions.EquivalentClass.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.EQUIVALENT_CLASS), equivalentClass));
            return this;
        }

        /// <summary>
        /// Unsets this ontology class as disjointClass of the given one
        /// </summary>
        public RDFOntologyClass RemoveDisjointWith(RDFOntologyClass disjointWith) {
            this.Assertions.DisjointWith.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.DISJOINT_WITH), disjointWith));
            return this;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology class, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result  = base.ToRDFGraph(includeInferences);

            foreach(var t in this.Assertions.SubClassOf.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Assertions.EquivalentClass.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Assertions.DisjointWith.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            if (!this.IsRestrictionClass() && !this.IsDataRangeClass()) {
                result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.CLASS));
            }

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyClassMetadata
    /// <summary>
    /// RDFOntologyClassMetadata represents a collector for assertion metadata describing an ontology class.
    /// </summary>
    public class RDFOntologyClassMetadata {

        #region Properties
        /// <summary>
        /// "rdfs:subClassOf" assertions about this ontology class
        /// </summary>
        public RDFOntologyTaxonomy SubClassOf { get; internal set; }

        /// <summary>
        /// "owl:equivalentClass" assertions about this ontology class
        /// </summary>
        public RDFOntologyTaxonomy EquivalentClass { get; internal set; }

        /// <summary>
        /// "owl:disjointWith" assertions about this ontology class
        /// </summary>
        public RDFOntologyTaxonomy DisjointWith { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology class metadata
        /// </summary>
        internal RDFOntologyClassMetadata() {
            this.SubClassOf      = new RDFOntologyTaxonomy();
            this.EquivalentClass = new RDFOntologyTaxonomy();
            this.DisjointWith    = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}