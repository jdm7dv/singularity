﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyModel represents the model component (T-BOX) of an ontology.
    /// </summary>
    public class RDFOntologyModel {

        #region Properties
        /// <summary>
        /// Submodel containing the ontology classes
        /// </summary>
        public RDFOntologyClassModel ClassModel { get; set; }

        /// <summary>
        /// Submodel containing the ontology properties
        /// </summary>
        public RDFOntologyPropertyModel PropertyModel { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology model
        /// </summary>
        public RDFOntologyModel() {
            this.ClassModel    = new RDFOntologyClassModel();
            this.PropertyModel = new RDFOntologyPropertyModel();
        }
        #endregion

        #region Methods

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology model, eventually including semantic inferences
        /// </summary>
        public RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result   = new RDFGraph();

            foreach (var t in this.ClassModel.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach (var t in this.PropertyModel.ToRDFGraph(includeInferences)) { result.AddTriple(t); }

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyClassModel
    /// <summary>
    /// RDFOntologyClassModel represents the class-oriented model component (T-BOX) of an ontology.
    /// </summary>
    public class RDFOntologyClassModel: IEnumerable<RDFOntologyClass> {

        #region Properties
        /// <summary>
        /// Count of the classes composing the class model
        /// </summary>
        public Int64 ClassesCount {
            get { return this.Classes.Count; }
        }

        /// <summary>
        /// Gets the enumerator on the class model's classes for iteration
        /// </summary>
        public IEnumerator<RDFOntologyClass> ClassesEnumerator {
            get { return this.Classes.Values.GetEnumerator(); }
        }

        /// <summary>
        /// Dictionary of classes composing the class model
        /// </summary>
        internal Dictionary<Int64, RDFOntologyClass> Classes { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty class model
        /// </summary>
        public RDFOntologyClassModel() {
            this.Classes = new Dictionary<Int64, RDFOntologyClass>();

            #region Inizialization
            var owlThing    = new RDFOntologyClass(RDFVocabulary.OWL.THING);
            var owlNothing  = new RDFOntologyClass(RDFVocabulary.OWL.NOTHING);
            var rdfsLiteral = new RDFOntologyClass(RDFVocabulary.RDFS.LITERAL);
            this.AddClass(owlThing);
            this.AddClass(owlNothing);
            this.AddClass(rdfsLiteral);
            
            foreach (var dt in RDFDatatypeRegister.Instance.Where(ns => ns.Prefix.Equals(RDFVocabulary.XSD.PREFIX)  ||
                                                                        ns.Prefix.Equals(RDFVocabulary.RDF.PREFIX)  ||
                                                                        ns.Prefix.Equals(RDFVocabulary.RDFS.PREFIX))) {
                this.AddClass(new RDFOntologyClass(new RDFResource(dt.ToString())).AddSubClassOf(rdfsLiteral));
            }
            #endregion

        }
        #endregion

        #region Interfaces
        /// <summary>
        /// Exposes a typed enumerator on the class model's classes
        /// </summary>
        IEnumerator<RDFOntologyClass> IEnumerable<RDFOntologyClass>.GetEnumerator() {
            return this.Classes.Values.GetEnumerator();
        }

        /// <summary>
        /// Exposes an untyped enumerator on the ontology class model's classes
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator() {
            return this.Classes.Values.GetEnumerator();
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Adds the given class to the ontology class model
        /// </summary>
        public RDFOntologyClassModel AddClass(RDFOntologyClass ontologyClass) {
            if (ontologyClass != null) {
                if (!this.Classes.ContainsKey(ontologyClass.PatternMemberID)) {
                     this.Classes.Add(ontologyClass.PatternMemberID, ontologyClass);
                }
            }
            return this;
        }

        /// <summary>
        /// Adds the given restriction to the ontology class model
        /// </summary>
        public RDFOntologyClassModel AddRestriction(RDFOntologyRestriction ontologyRestriction) {
            if (ontologyRestriction != null) {
                if (!this.Classes.ContainsKey(ontologyRestriction.PatternMemberID)) {
                     this.Classes.Add(ontologyRestriction.PatternMemberID, ontologyRestriction);
                }
            }
            return this;
        }

        /// <summary>
        /// Adds the given datarange class to the ontology class model
        /// </summary>
        public RDFOntologyClassModel AddDataRange(RDFOntologyDataRangeClass ontologyDataRangeClass) {
            if (ontologyDataRangeClass != null) {
                if (!this.Classes.ContainsKey(ontologyDataRangeClass.PatternMemberID)) {
                     this.Classes.Add(ontologyDataRangeClass.PatternMemberID, ontologyDataRangeClass);
                }
            }
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Removes the given class from the ontology class model
        /// </summary>
        public RDFOntologyClassModel RemoveClass(RDFOntologyClass ontologyClass) {
            if (ontologyClass != null) {
                if (this.Classes.ContainsKey(ontologyClass.PatternMemberID)) {
                    this.Classes.Remove(ontologyClass.PatternMemberID);
                }
            }
            return this;
        }

        /// <summary>
        /// Removes the given restriction from the ontology class model
        /// </summary>
        public RDFOntologyClassModel RemoveRestriction(RDFOntologyRestriction ontologyRestriction) {
            if (ontologyRestriction != null) {
                if (this.Classes.ContainsKey(ontologyRestriction.PatternMemberID)) {
                    this.Classes.Remove(ontologyRestriction.PatternMemberID);
                }
            }
            return this;
        }

        /// <summary>
        /// Removes the given datarange from the ontology class model
        /// </summary>
        public RDFOntologyClassModel RemoveDataRange(RDFOntologyDataRangeClass ontologyDataRangeClass) {
            if (ontologyDataRangeClass != null) {
                if (this.Classes.ContainsKey(ontologyDataRangeClass.PatternMemberID)) {
                    this.Classes.Remove(ontologyDataRangeClass.PatternMemberID);
                }
            }
            return this;
        }
        #endregion

        #region Select
        /// <summary>
        /// Selects the ontology class represented by the given string from the ontology class model
        /// </summary>
        public RDFOntologyClass SelectClass(String ontClass) {
            if (ontClass     != null) {
                Int64 classID = RDFModelUtilities.CreateHash(ontClass);
                if (this.Classes.ContainsKey(classID)) {
                    return this.Classes[classID];
                }
            }
            return null;
        }

        /// <summary>
        /// Selects the ontology restriction represented by the given string from the ontology class model
        /// </summary>
        public RDFOntologyRestriction SelectRestriction(String ontRestriction) {
            if (ontRestriction != null) {
                Int64 restrID   = RDFModelUtilities.CreateHash(ontRestriction);
                if (this.Classes.ContainsKey(restrID)) {
                    if (this.Classes[restrID].IsRestrictionClass()) {
                        return (RDFOntologyRestriction)this.Classes[restrID];
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Selects the ontology datarange represented by the given string from the ontology class model
        /// </summary>
        public RDFOntologyDataRangeClass SelectDataRange(String ontDataRangeClass) {
            if (ontDataRangeClass != null) {
                Int64 datarangeID  = RDFModelUtilities.CreateHash(ontDataRangeClass);
                if (this.Classes.ContainsKey(datarangeID)) {
                    if (this.Classes[datarangeID].IsDataRangeClass()) {
                        return (RDFOntologyDataRangeClass)this.Classes[datarangeID];
                    }
                }
            }
            return null;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology class model, eventually including semantic inferences
        /// </summary>
        public RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result   = new RDFGraph();
            foreach (var c  in this) {
                foreach (var t in c.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            }
            return result;
        }
        #endregion

        #endregion

    }
    #endregion

    #region RDFOntologyPropertyModel
    /// <summary>
    /// RDFOntologyPropertyModel represents the property-oriented model component (T-BOX) of an ontology.
    /// </summary>
    public class RDFOntologyPropertyModel: IEnumerable<RDFOntologyProperty> {

        #region Properties
        /// <summary>
        /// Count of the properties composing the property model
        /// </summary>
        public Int64 PropertiesCount {
            get { return this.Properties.Count; }
        }

        /// <summary>
        /// Gets the enumerator on the property model's properties for iteration
        /// </summary>
        public IEnumerator<RDFOntologyProperty> PropertiesEnumerator {
            get { return this.Properties.Values.GetEnumerator(); }
        }

        /// <summary>
        /// Dictionary of properties composing the property model
        /// </summary>
        internal Dictionary<Int64, RDFOntologyProperty> Properties { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty property model
        /// </summary>
        public RDFOntologyPropertyModel() {
            this.Properties = new Dictionary<Int64, RDFOntologyProperty>();
        }
        #endregion

        #region Interfaces
        /// <summary>
        /// Exposes a typed enumerator on the property model's properties
        /// </summary>
        IEnumerator<RDFOntologyProperty> IEnumerable<RDFOntologyProperty>.GetEnumerator() {
            return this.Properties.Values.GetEnumerator();
        }

        /// <summary>
        /// Exposes an untyped enumerator on the property model's properties
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator() {
            return this.Properties.Values.GetEnumerator();
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Adds the given property to the ontology property model
        /// </summary>
        public RDFOntologyPropertyModel AddProperty(RDFOntologyProperty ontologyProperty) {
            if (ontologyProperty != null) {
                if (!this.Properties.ContainsKey(ontologyProperty.PatternMemberID)) {
                     this.Properties.Add(ontologyProperty.PatternMemberID, ontologyProperty);
                }
            }
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Removes the given property from the ontology property model
        /// </summary>
        public RDFOntologyPropertyModel RemoveProperty(RDFOntologyProperty ontologyProperty) {
            if (ontologyProperty != null) {
                if (this.Properties.ContainsKey(ontologyProperty.PatternMemberID)) {
                    this.Properties.Remove(ontologyProperty.PatternMemberID);
                }
            }
            return this;
        }
        #endregion

        #region Select
        /// <summary>
        /// Selects the ontology property represented by the given string from the ontology property model
        /// </summary>
        public RDFOntologyProperty SelectProperty(String ontProperty) {
            if (ontProperty     != null) {
                Int64 propertyID = RDFModelUtilities.CreateHash(ontProperty);
                if (this.Properties.ContainsKey(propertyID)) {
                    return this.Properties[propertyID];
                }
            }
            return null;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology property model, eventually including semantic inferences
        /// </summary>
        public RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result   = new RDFGraph();
            foreach (var p  in this) {
                foreach (var t in p.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            }
            return result;
        }
        #endregion

        #endregion

    }
    #endregion

}