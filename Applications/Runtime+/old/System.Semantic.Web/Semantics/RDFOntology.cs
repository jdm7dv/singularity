﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntology represents an ontology definition.
    /// </summary>
    public class RDFOntology: RDFOntologyResource {

        #region Properties
        /// <summary>
        /// Model of the ontology
        /// </summary>
        public RDFOntologyModel Model { get; internal set; }

        /// <summary>
        /// Data of the ontology
        /// </summary>
        public RDFOntologyData Data { get; internal set; }

        /// <summary>
        /// Annotation metadata of the ontology
        /// </summary>
        internal new RDFOntologyMetadata Annotations { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology with the given name
        /// </summary>
        public RDFOntology(RDFResource ontologyName) {
            if (ontologyName != null) {
                this.Value           = ontologyName;
                this.PatternMemberID = ontologyName.PatternMemberID;
                this.Model           = new RDFOntologyModel();
                this.Data            = new RDFOntologyData();
                this.Annotations     = new RDFOntologyMetadata();
            }
            else {
                throw new RDFSemanticsException("Cannot create RDFOntology because given \"ontologyName\" parameter is null.");
            }
        }

        /// <summary>
        /// Default-ctor to build an ontology from the given resource and the given ontology model
        /// </summary>
        public RDFOntology(RDFResource ontologyName, RDFOntologyModel ontologyModel): this(ontologyName) {
            if (ontologyModel != null) {
                this.Model     = ontologyModel;
            }
        }

        /// <summary>
        /// Default-ctor to build an ontology from the given resource, the given ontology model and the given ontology data
        /// </summary>
        public RDFOntology(RDFResource ontologyName, RDFOntologyModel ontologyModel, RDFOntologyData ontologyData): this(ontologyName, ontologyModel) {
            if (ontologyData  != null) {
                this.Data      = ontologyData;
            }
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Adds the given ontology to the "owl:imports" annotations of this ontology.
        /// </summary>
        public RDFOntology AddImports(RDFOntology ontology) {
            this.Annotations.Imports.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.IMPORTS), ontology));
            return this;
        }

        /// <summary>
        /// Adds the given ontology to the "owl:backwardCompatibleWith" annotations of this ontology.
        /// </summary>
        public RDFOntology AddBackwardCompatibleWith(RDFOntology ontology) {
            this.Annotations.BackwardCompatibleWith.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.BACKWARD_COMPATIBLE_WITH), ontology));
            return this;
        }

        /// <summary>
        /// Adds the given ontology to the "owl:incompatibleWith" annotations of this ontology.
        /// </summary>
        public RDFOntology AddIncompatibleWith(RDFOntology ontology) {
            this.Annotations.IncompatibleWith.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.INCOMPATIBLE_WITH), ontology));
            return this;
        }

        /// <summary>
        /// Adds the given ontology literal to the "owl:priorVersion" annotations of this ontology.
        /// </summary>
        public RDFOntology AddPriorVersion(RDFOntologyLiteral priorVersion) {
            this.Annotations.PriorVersions.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.PRIOR_VERSION), priorVersion));
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Removes the given ontology from the "owl:imports" annotations of this ontology.
        /// </summary>
        public RDFOntology RemoveImports(RDFOntology ontology) {
            this.Annotations.Imports.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.IMPORTS), ontology));
            return this;
        }

        /// <summary>
        /// Removes the given ontology from the "owl:backwardCompatibleWith" annotations of this ontology.
        /// </summary>
        public RDFOntology RemoveBackwardCompatibleWith(RDFOntology ontology) {
            this.Annotations.BackwardCompatibleWith.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.BACKWARD_COMPATIBLE_WITH), ontology));
            return this;
        }

        /// <summary>
        /// Removes the given ontology from the "owl:incompatibleWith" annotations of this ontology.
        /// </summary>
        public RDFOntology RemoveIncompatibleWith(RDFOntologyFact ontology) {
            this.Annotations.IncompatibleWith.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.INCOMPATIBLE_WITH), ontology));
            return this;
        }

        /// <summary>
        /// Removes the given ontology literal from the "owl:priorVersion" annotations of this ontology.
        /// </summary>
        public RDFOntology RemovePriorVersion(RDFOntologyLiteral priorVersion) {
            this.Annotations.PriorVersions.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.PRIOR_VERSION), priorVersion));
            return this;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets an ontology representation of the given graph.
        /// </summary>
        public static RDFOntology FromRDFGraph(RDFGraph ontGraph) {
            return RDFSemanticsUtilities.FromRDFGraph(ontGraph);
        }

        /// <summary>
        /// Gets a graph representation of this ontology, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result = base.ToRDFGraph(includeInferences)
                             .AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.ONTOLOGY));

            foreach (var t in this.Annotations.Imports.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach (var t in this.Annotations.BackwardCompatibleWith.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach (var t in this.Annotations.IncompatibleWith.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach (var t in this.Annotations.PriorVersions.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach (var t in this.Model.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach (var t in this.Data.ToRDFGraph(includeInferences)) { result.AddTriple(t); }

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyMetadata
    /// <summary>
    /// RDFOntologyMetadata represents a collector for annotation metadata describing an ontology.
    /// </summary>
    public class RDFOntologyMetadata: RDFOntologyResourceMetadata {

        #region Properties
        /// <summary>
        /// "owl:imports" annotations about this ontology
        /// </summary>
        public RDFOntologyTaxonomy Imports { get; internal set; }

        /// <summary>
        /// "owl:backwardIncompatibleWith" annotations about this ontology
        /// </summary>
        public RDFOntologyTaxonomy BackwardCompatibleWith { get; internal set; }

        /// <summary>
        /// "owl:incompatibleWith" annotations about this ontology
        /// </summary>
        public RDFOntologyTaxonomy IncompatibleWith { get; internal set; }

        /// <summary>
        /// "owl:priorVersion" annotations about this ontology
        /// </summary>
        public RDFOntologyTaxonomy PriorVersions { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology metadata
        /// </summary>
        internal RDFOntologyMetadata() {
            this.Imports                = new RDFOntologyTaxonomy();
            this.BackwardCompatibleWith = new RDFOntologyTaxonomy();
            this.IncompatibleWith       = new RDFOntologyTaxonomy();
            this.PriorVersions          = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}