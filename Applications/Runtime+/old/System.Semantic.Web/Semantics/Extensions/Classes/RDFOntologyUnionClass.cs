﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics {

    /// <summary>
    /// RDFOntologyUnionClass represents a union class definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyUnionClass: RDFOntologyClass {

        #region Properties
        /// <summary>
        /// Assertion metadata of the ontology union class
        /// </summary>
        public new RDFOntologyUnionClassMetadata Assertions { get; internal set; }

        /// <summary>
        /// Collection to host the union class members when serializing to a graph
        /// </summary>
        internal RDFCollection RepresentativeCollection { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology union class with the given name
        /// </summary>
        public RDFOntologyUnionClass(RDFResource className): base(className) {
            this.Assertions               = new RDFOntologyUnionClassMetadata();
            this.RepresentativeCollection = new RDFCollection(RDFModelEnums.RDFItemTypes.Resource);
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Sets this ontology union class as "owl:unionOf" to the given ontology class
        /// </summary>
        public RDFOntologyUnionClass AddUnionOf(RDFOntologyClass unionOf) {
            this.Assertions.UnionOf.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.UNION_OF), unionOf));
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Unsets this ontology union class as "owl:unionOf" from the given ontology class
        /// </summary>
        public RDFOntologyUnionClass RemoveUnionOf(RDFOntologyClass unionOf) {
            this.Assertions.UnionOf.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.UNION_OF), unionOf));
            return this;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology union class, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result = base.ToRDFGraph(includeInferences);

            this.RepresentativeCollection.ClearItems();
            foreach (var uof in this.Assertions.UnionOf) {
                this.RepresentativeCollection.AddItem((RDFResource)uof.TaxonomyObject.Value);
            }
            result  = result.UnionWith(this.RepresentativeCollection.ReifyCollection());
            result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.OWL.UNION_OF, this.RepresentativeCollection.ReificationSubject));

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyUnionClassMetadata
    /// <summary>
    /// RDFOntologyUnionClassMetadata represents a collector for assertion metadata describing an ontology union class.
    /// </summary>
    public class RDFOntologyUnionClassMetadata: RDFOntologyClassMetadata {

        #region Properties
        /// <summary>
        /// "owl:unionOf" assertions about this ontology union class
        /// </summary>
        public RDFOntologyTaxonomy UnionOf { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology union class metadata
        /// </summary>
        internal RDFOntologyUnionClassMetadata() {
            this.UnionOf = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}