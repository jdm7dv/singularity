﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyEnumerateClass represents an enumerate class definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyEnumerateClass: RDFOntologyClass {

        #region Properties
        /// <summary>
        /// Assertion metadata of the ontology enumerate class
        /// </summary>
        public new RDFOntologyEnumerateClassMetadata Assertions { get; internal set; }

        /// <summary>
        /// Collection to host the enumerate members when serializing to a graph
        /// </summary>
        internal RDFCollection RepresentativeCollection { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology enumerate class with the given name
        /// </summary>
        public RDFOntologyEnumerateClass(RDFResource className): base(className) {
            this.Assertions               = new RDFOntologyEnumerateClassMetadata();
            this.RepresentativeCollection = new RDFCollection(RDFModelEnums.RDFItemTypes.Resource);
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Adds the given ontology fact to the members of this ontology datarange
        /// </summary>
        public RDFOntologyEnumerateClass AddOneOf(RDFOntologyFact oneOfMember) {
            this.Assertions.OneOf.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.ONE_OF), oneOfMember));
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Removes the given ontology fact from the members of this ontology datarange
        /// </summary>
        public RDFOntologyEnumerateClass RemoveOneOf(RDFOntologyFact oneOfMember) {
            this.Assertions.OneOf.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.ONE_OF), oneOfMember));
            return this;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology enumerate class, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result = base.ToRDFGraph(includeInferences);

            this.RepresentativeCollection.ClearItems();
            foreach (var em in this.Assertions.OneOf) {
                this.RepresentativeCollection.AddItem((RDFResource)em.TaxonomyObject.Value);
            }
            result  = result.UnionWith(this.RepresentativeCollection.ReifyCollection());
            result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.OWL.ONE_OF, this.RepresentativeCollection.ReificationSubject));

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyEnumerateClassMetadata
    /// <summary>
    /// RDFOntologyEnumerateClassMetadata represents a collector for assertion metadata describing an ontology enumerate class.
    /// </summary>
    public class RDFOntologyEnumerateClassMetadata: RDFOntologyClassMetadata {

        #region Properties
        /// <summary>
        /// "owl:oneOf" assertions about this ontology enumerate class
        /// </summary>
        public RDFOntologyTaxonomy OneOf { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology enumerate class metadata
        /// </summary>
        internal RDFOntologyEnumerateClassMetadata() {
            this.OneOf = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}