﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics {

    /// <summary>
    /// RDFOntologyDataRangeClass represents a datarange class definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyDataRangeClass: RDFOntologyClass {

        #region Properties
        /// <summary>
        /// Assertion metadata of the ontology datarange class
        /// </summary>
        public new RDFOntologyDataRangeClassMetadata Assertions { get; internal set; }

        /// <summary>
        /// Collection to host the enumerate members when serializing to a graph
        /// </summary>
        internal RDFCollection RepresentativeCollection { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology datarange class with the given name
        /// </summary>
        public RDFOntologyDataRangeClass(RDFResource className): base(className) {
            this.Assertions               = new RDFOntologyDataRangeClassMetadata();
            this.RepresentativeCollection = new RDFCollection(RDFModelEnums.RDFItemTypes.Literal);
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Adds the given ontology literal to the members of this ontology datarange
        /// </summary>
        public RDFOntologyDataRangeClass AddOneOf(RDFOntologyLiteral oneOfMember) {
            this.Assertions.OneOf.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.ONE_OF), oneOfMember));
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Removes the given ontology literal from the members of this ontology datarange
        /// </summary>
        public RDFOntologyDataRangeClass RemoveOneOf(RDFOntologyLiteral oneOfMember) {
            this.Assertions.OneOf.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.ONE_OF), oneOfMember));
            return this;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology datarange class, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result = base.ToRDFGraph(includeInferences);

            this.RepresentativeCollection.ClearItems();
            foreach (var em in this.Assertions.OneOf) {
                this.RepresentativeCollection.AddItem((RDFLiteral)em.TaxonomyObject.Value);
            }
            result  = result.UnionWith(this.RepresentativeCollection.ReifyCollection());
            result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.DATA_RANGE));
            result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.OWL.ONE_OF, this.RepresentativeCollection.ReificationSubject));

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyDataRangeClassMetadata
    /// <summary>
    /// RDFOntologyDataRangeClassMetadata represents a collector for assertion metadata describing an ontology datarange class.
    /// </summary>
    public class RDFOntologyDataRangeClassMetadata: RDFOntologyClassMetadata {

        #region Properties
        /// <summary>
        /// "owl:oneOf" assertions about this ontology datarange class
        /// </summary>
        public RDFOntologyTaxonomy OneOf { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology datarange class metadata
        /// </summary>
        internal RDFOntologyDataRangeClassMetadata() {
            this.OneOf = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}