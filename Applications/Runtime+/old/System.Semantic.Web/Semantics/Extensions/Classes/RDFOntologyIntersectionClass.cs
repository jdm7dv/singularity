﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics {

    /// <summary>
    /// RDFOntologyIntersectionClass represents an intersection class definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyIntersectionClass: RDFOntologyClass {

        #region Properties
        /// <summary>
        /// Assertion metadata of the ontology intersection class
        /// </summary>
        public new RDFOntologyIntersectionClassMetadata Assertions { get; internal set; }

        /// <summary>
        /// Collection to host the intersection class members when serializing to a graph
        /// </summary>
        internal RDFCollection RepresentativeCollection { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology intersection class with the given className
        /// </summary>
        public RDFOntologyIntersectionClass(RDFResource className): base(className) {
            this.Assertions               = new RDFOntologyIntersectionClassMetadata();
            this.RepresentativeCollection = new RDFCollection(RDFModelEnums.RDFItemTypes.Resource);
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Sets this ontology intersection class as "owl:intersectionOf" to the given ontology class
        /// </summary>
        public RDFOntologyIntersectionClass AddIntersectionOf(RDFOntologyClass intersectionOf) {
            this.Assertions.IntersectionOf.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.INTERSECTION_OF), intersectionOf));
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Unsets this ontology intersection class as "owl:intersectionOf" from the given ontology class
        /// </summary>
        public RDFOntologyIntersectionClass RemoveIntersectionOf(RDFOntologyClass intersectionOf) {
            this.Assertions.IntersectionOf.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.INTERSECTION_OF), intersectionOf));
            return this;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology intersection class, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result = base.ToRDFGraph(includeInferences);

            this.RepresentativeCollection.ClearItems();
            foreach (var uof in this.Assertions.IntersectionOf) {
                this.RepresentativeCollection.AddItem((RDFResource)uof.TaxonomyObject.Value);
            }
            result  = result.UnionWith(this.RepresentativeCollection.ReifyCollection());
            result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.OWL.INTERSECTION_OF, this.RepresentativeCollection.ReificationSubject));

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyIntersectionClassMetadata
    /// <summary>
    /// RDFOntologyIntersectionClassMetadata represents a collector for assertion metadata describing an ontology intersection class.
    /// </summary>
    public class RDFOntologyIntersectionClassMetadata: RDFOntologyClassMetadata {

        #region Properties
        /// <summary>
        /// "owl:intersectionOf" assertions about this ontology intersection class
        /// </summary>
        public RDFOntologyTaxonomy IntersectionOf { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology intersection class metadata
        /// </summary>
        internal RDFOntologyIntersectionClassMetadata() {
            this.IntersectionOf = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}