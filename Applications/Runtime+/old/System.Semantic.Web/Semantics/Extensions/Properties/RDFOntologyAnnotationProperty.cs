﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics {

    /// <summary>
    /// RDFOntologyAnnotationProperty represents an annotation property definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyAnnotationProperty: RDFOntologyProperty {

        #region Ctors
        /// <summary>
        /// Default-ctor to build an ontology annotation property from the given non-blank resource
        /// </summary>
        public RDFOntologyAnnotationProperty(RDFResource propertyName): base(propertyName) { }
        #endregion

        #region Methods

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology annotation property, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            return base.ToRDFGraph(includeInferences)
                       .AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.ANNOTATION_PROPERTY));
        }
        #endregion

        #endregion

    }

}