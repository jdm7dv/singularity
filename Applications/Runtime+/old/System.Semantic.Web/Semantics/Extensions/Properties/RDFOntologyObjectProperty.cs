﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics {

    /// <summary>
    /// RDFOntologyObjectProperty represents an object property definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyObjectProperty: RDFOntologyProperty {

        #region Properties
        /// <summary>
        /// Flag indicating that the ontology property is "owl:SymmetricProperty"
        /// </summary>
        public Boolean Symmetric { get; internal set; }

        /// <summary>
        /// Flag indicating that the ontology property is "owl:TransitiveProperty"
        /// </summary>
        public Boolean Transitive { get; internal set; }

        /// <summary>
        /// Flag indicating that the ontology property is "owl:InverseFunctionalProperty"
        /// </summary>
        public Boolean InverseFunctional { get; internal set; }

        /// <summary>
        /// Assertion metadata of the ontology object property
        /// </summary>
        public new RDFOntologyObjectPropertyMetadata Assertions { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an ontology object property from the given non-blank resource
        /// </summary>
        public RDFOntologyObjectProperty(RDFResource propertyName): base(propertyName) {
            this.Assertions = new RDFOntologyObjectPropertyMetadata();
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Sets or unsets this ontology object property as "owl:SymmetricProperty"
        /// </summary>
        public RDFOntologyObjectProperty SetSymmetric(Boolean symmetric) {
            this.Symmetric  = symmetric;
            return this;
        }

        /// <summary>
        /// Sets or unsets this ontology object property as "owl:TransitiveProperty"
        /// </summary>
        public RDFOntologyObjectProperty SetTransitive(Boolean transitive) {
            this.Transitive = transitive;
            return this;
        }

        /// <summary>
        /// Sets or unsets this ontology object property as "owl:InverseFunctionalProperty"
        /// </summary>
        public RDFOntologyObjectProperty SetInverseFunctional(Boolean inverseFunctional) {
            this.InverseFunctional = inverseFunctional;
            return this;
        }

        /// <summary>
        /// Sets this ontology object property as inverse property of the given one
        /// </summary>
        public RDFOntologyObjectProperty AddInverseOf(RDFOntologyObjectProperty inverseOf) {
            this.Assertions.InverseOf.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.INVERSE_OF), inverseOf));
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Unsets this ontology object property from inverse property of the given one
        /// </summary>
        public RDFOntologyObjectProperty RemoveInverseOf(RDFOntologyObjectProperty inverseOf) {
            this.Assertions.InverseOf.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.INVERSE_OF), inverseOf));
            return this;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology object property, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result  = base.ToRDFGraph(includeInferences);

            foreach(var t in this.Assertions.InverseOf.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.OBJECT_PROPERTY));
            if (this.Symmetric) {
                result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.SYMMETRIC_PROPERTY));
            }
            if (this.Transitive) {
                result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.TRANSITIVE_PROPERTY));
            }
            if (this.InverseFunctional) {
                result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.INVERSE_FUNCTIONAL_PROPERTY));
            }

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyObjectPropertyMetadata
    /// <summary>
    /// RDFOntologyPropertyMetadata represents a collector for assertion metadata describing an ontology object property.
    /// </summary>
    public class RDFOntologyObjectPropertyMetadata: RDFOntologyPropertyMetadata {

        #region Properties
        /// <summary>
        /// "owl:inverseOf" assertions about this ontology object property
        /// </summary>
        public RDFOntologyTaxonomy InverseOf { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology object property metadata
        /// </summary>
        internal RDFOntologyObjectPropertyMetadata() {
            this.InverseOf = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}