﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyAllValuesFromRestriction represents an "owl:AllValuesFrom" restriction class definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyAllValuesFromRestriction: RDFOntologyRestriction {

        #region Properties
        /// <summary>
        /// Ontology class representing the accepted class type of facts/literals belonging to this restriction
        /// </summary>
        public RDFOntologyClass FromClass { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an "owl:AllValuesFrom" ontology restriction with the given name on the given property and the given fromClass
        /// </summary>
        public RDFOntologyAllValuesFromRestriction(RDFResource restrictionName, RDFOntologyProperty onProperty, RDFOntologyClass fromClass): base(restrictionName, onProperty) {
            if (fromClass     != null) {
                this.FromClass = fromClass;
            }
            else {
                throw new RDFSemanticsException("Cannot create RDFOntologyAllValuesFromRestriction because given \"fromClass\" parameter is null.");
            }
        }
        #endregion

        #region Methods

        #region Convert
        /// <summary>
        /// Gets a graph representation of this "owl:AllValuesFrom" ontology restriction, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            return base.ToRDFGraph(includeInferences)
                       .AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.OWL.ALL_VALUES_FROM, (RDFResource)this.FromClass.Value));
        }
        #endregion

        #endregion

    }

}