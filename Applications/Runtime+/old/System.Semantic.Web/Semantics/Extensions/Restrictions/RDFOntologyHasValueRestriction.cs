﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyHasValueRestriction represents an "owl:HasValue" restriction class definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyHasValueRestriction: RDFOntologyRestriction {

        #region Properties
        /// <summary>
        /// Ontology resource representing the accepted fact/literal belonging to this restriction
        /// </summary>
        public RDFOntologyResource RequiredValue { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an "owl:HasValue" ontology restriction with the given name on the given property and the given requiredValue
        /// </summary>
        public RDFOntologyHasValueRestriction(RDFResource restrictionName, RDFOntologyProperty onProperty, RDFOntologyFact requiredValue): base(restrictionName, onProperty) {
            if (requiredValue     != null) {
                this.RequiredValue = requiredValue;
            }
            else {
                throw new RDFSemanticsException("Cannot create RDFOntologyHasValueRestriction because given \"requiredValue\" parameter is null.");
            }
        }

        /// <summary>
        /// Default-ctor to build an "owl:HasValue" ontology restriction with the given name on the given property and the given required value
        /// </summary>
        public RDFOntologyHasValueRestriction(RDFResource restrictionName, RDFOntologyProperty onProperty, RDFOntologyLiteral requiredValue): base(restrictionName, onProperty) {
            if (requiredValue     != null) {
                this.RequiredValue = requiredValue;
            }
            else {
                throw new RDFSemanticsException("Cannot create RDFOntologyHasValueRestriction because given \"requiredValue\" parameter is null.");
            }
        }
        #endregion

        #region Methods

        #region Convert
        /// <summary>
        /// Gets a graph representation of this "owl:HasValue" ontology restriction, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            return (this.RequiredValue.IsLiteral() ? 
                        base.ToRDFGraph(includeInferences).AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.OWL.HAS_VALUE, (RDFLiteral) this.RequiredValue.Value)) :
                        base.ToRDFGraph(includeInferences).AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.OWL.HAS_VALUE, (RDFResource)this.RequiredValue.Value)));
        }
        #endregion

        #endregion

    }

}