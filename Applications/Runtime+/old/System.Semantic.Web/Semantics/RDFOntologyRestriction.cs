﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyRestriction represents a restriction class definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyRestriction: RDFOntologyClass {

        #region Properties
        /// <summary>
        /// Ontology property on which the ontology restriction is applied
        /// </summary>
        public RDFOntologyProperty OnProperty { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an ontology restriction with the given name on the given ontology property
        /// </summary>
        internal RDFOntologyRestriction (RDFResource restrictionName, RDFOntologyProperty onProperty): base(restrictionName) {
            if (onProperty     != null) {
                this.OnProperty = onProperty;
            }
            else {
                throw new RDFSemanticsException("Cannot create RDFOntologyRestriction because given \"onProperty\" parameter is null.");
            }
        }
        #endregion

        #region Methods

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology restriction, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result = base.ToRDFGraph(includeInferences);

            result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.RESTRICTION));
            result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.OWL.ON_PROPERTY, (RDFResource)this.OnProperty.Value));

            return result;
        }
        #endregion

        #endregion

    }

}