﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyFact represents a class instance in the context of an ontology data.
    /// </summary>
    public class RDFOntologyFact: RDFOntologyResource {

        #region Properties
        /// <summary>
        /// Assertion metadata of the ontology fact
        /// </summary>
        public RDFOntologyFactMetadata Assertions { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an ontology fact with the given name
        /// </summary>
        public RDFOntologyFact(RDFResource factName) {
            if (factName  != null) {
                this.Value           = factName;
                this.PatternMemberID = factName.PatternMemberID;
                this.Assertions      = new RDFOntologyFactMetadata();
            }
            else {
                throw new RDFSemanticsException("Cannot create RDFOntologyFact because given \"factName\" parameter is null.");
            }
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Sets this ontology fact as instance of the given ontology class
        /// </summary>
        public RDFOntologyFact AddClassType(RDFOntologyClass classType) {
            this.Assertions.ClassType.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.RDF.TYPE), classType));
            return this;
        }

        /// <summary>
        /// Sets this ontology fact as same to the given one
        /// </summary>
        public RDFOntologyFact AddSameAs(RDFOntologyFact sameAs) {
            this.Assertions.SameAs.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.SAME_AS), sameAs));
            return this;
        }

        /// <summary>
        /// Sets this ontology fact as different from the given one
        /// </summary>
        public RDFOntologyFact AddDifferentFrom(RDFOntologyFact differentFrom) {
            this.Assertions.DifferentFrom.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.DIFFERENT_FROM), differentFrom));
            return this;
        }

        /// <summary>
        /// Adds the given "property -> value" attribute to this ontology fact
        /// </summary>
        public RDFOntologyFact AddAttribute(RDFOntologyObjectProperty attributeProperty,
                                            RDFOntologyFact attributeValue) {
            this.Assertions.Attributes.AddEntry(new RDFOntologyTaxonomyEntry(this, attributeProperty, attributeValue));
            return this;
        }

        /// <summary>
        /// Adds the given "property -> value" attribute to this ontology fact
        /// </summary>
        public RDFOntologyFact AddAttribute(RDFOntologyDatatypeProperty attributeProperty,
                                            RDFOntologyLiteral attributeLiteral) {
            this.Assertions.Attributes.AddEntry(new RDFOntologyTaxonomyEntry(this, attributeProperty, attributeLiteral));
            return this;
        }

        /// <summary>
        /// Adds the given "property -> value" attribute to this ontology fact
        /// </summary>
        public RDFOntologyFact AddAttribute(RDFOntologyDatatypeProperty attributeProperty,
                                            RDFOntologyDataRangeClass attributeDataRange) {
            this.Assertions.Attributes.AddEntry(new RDFOntologyTaxonomyEntry(this, attributeProperty, attributeDataRange));
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Unsets this ontology fact as instance of the given ontology class
        /// </summary>
        public RDFOntologyFact RemoveClassType(RDFOntologyClass classType) {
            this.Assertions.ClassType.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.RDF.TYPE), classType));
            return this;
        }

        /// <summary>
        /// Unsets this ontology fact as same to the given one
        /// </summary>
        public RDFOntologyFact RemoveSameAs(RDFOntologyFact sameAs) {
            this.Assertions.SameAs.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.SAME_AS), sameAs));
            return this;
        }

        /// <summary>
        /// Unsets this ontology fact as different from the given one
        /// </summary>
        public RDFOntologyFact RemoveDifferentFrom(RDFOntologyFact differentFrom) {
            this.Assertions.DifferentFrom.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.DIFFERENT_FROM), differentFrom));
            return this;
        }

        /// <summary>
        /// Removes the given "property -> value" attribute from this ontology fact
        /// </summary>
        public RDFOntologyFact RemoveAttribute(RDFOntologyObjectProperty attributeProperty,
                                               RDFOntologyFact attributeValue) {
            this.Assertions.Attributes.RemoveEntry(new RDFOntologyTaxonomyEntry(this, attributeProperty, attributeValue));
            return this;
        }

        /// <summary>
        /// Removes the given "property -> value" attribute from this ontology fact
        /// </summary>
        public RDFOntologyFact RemoveAttribute(RDFOntologyDatatypeProperty attributeProperty,
                                               RDFOntologyLiteral attributeLiteral) {
            this.Assertions.Attributes.RemoveEntry(new RDFOntologyTaxonomyEntry(this, attributeProperty, attributeLiteral));
            return this;
        }

        /// <summary>
        /// Removes the given "property -> value" attribute from this ontology fact
        /// </summary>
        public RDFOntologyFact RemoveAttribute(RDFOntologyDatatypeProperty attributeProperty,
                                               RDFOntologyDataRangeClass attributeDataRange) {
            this.Assertions.Attributes.RemoveEntry(new RDFOntologyTaxonomyEntry(this, attributeProperty, attributeDataRange));
            return this;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology fact, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result  = base.ToRDFGraph(includeInferences);

            foreach(var t in this.Assertions.ClassType.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Assertions.SameAs.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Assertions.DifferentFrom.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Assertions.Attributes.ToRDFGraph(includeInferences)) { result.AddTriple(t); }

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyFactMetadata
    /// <summary>
    /// RDFOntologyFactMetadata represents a collector for assertion metadata describing an ontology fact.
    /// </summary>
    public class RDFOntologyFactMetadata {

        #region Properties
        /// <summary>
        /// "rdf:type" assertions about this ontology fact
        /// </summary>
        public RDFOntologyTaxonomy ClassType { get; internal set; }

        /// <summary>
        /// "owl:sameAs" assertions about this ontology fact
        /// </summary>
        public RDFOntologyTaxonomy SameAs { get; internal set; }

        /// <summary>
        /// "owl:differentFrom" assertions about this ontology fact
        /// </summary>
        public RDFOntologyTaxonomy DifferentFrom { get; internal set; }

        /// <summary>
        /// "property -> value" assertions about this ontology fact
        /// </summary>
        public RDFOntologyTaxonomy Attributes { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology fact metadata
        /// </summary>
        internal RDFOntologyFactMetadata() {
            this.ClassType     = new RDFOntologyTaxonomy();
            this.SameAs        = new RDFOntologyTaxonomy();
            this.DifferentFrom = new RDFOntologyTaxonomy();
            this.Attributes    = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}