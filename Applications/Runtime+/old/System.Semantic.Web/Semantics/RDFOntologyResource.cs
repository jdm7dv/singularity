﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyResource represents a generic resource definition in the context of an ontology.
    /// </summary>
    public class RDFOntologyResource: RDFPatternMember {

        #region Properties
        /// <summary>
        /// Value of the ontology resource
        /// </summary>
        public RDFPatternMember Value { get; internal set; }

        /// <summary>
        /// Annotation metadata of the ontology resource
        /// </summary>
        public RDFOntologyResourceMetadata Annotations { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology resource
        /// </summary>
        internal RDFOntologyResource() {
            this.Annotations = new RDFOntologyResourceMetadata();
        }
        #endregion

        #region Interfaces
        /// <summary>
        /// Gets the string representation of the ontology resource
        /// </summary>
        public override String ToString() {
            return this.Value.ToString();
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Adds the given literal to the "owl:versionInfo" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource AddVersionInfo(RDFOntologyLiteral versionInfo) {
            if (!this.IsLiteral()) {
                 this.Annotations.VersionInfo.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.OWL.VERSION_INFO), versionInfo));
            }
            return this;
        }

        /// <summary>
        /// Adds the given literal to the "rdfs:comment" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource AddComment(RDFOntologyLiteral comment) {
            if (!this.IsLiteral()) {
                 this.Annotations.Comment.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.RDFS.COMMENT), comment));
            }
            return this;
        }

        /// <summary>
        /// Adds the given literal to the "rdfs:label" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource AddLabel(RDFOntologyLiteral label) {
            if (!this.IsLiteral()) {
                 this.Annotations.Label.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.RDFS.LABEL), label));
            }
            return this;
        }

        /// <summary>
        /// Adds the given resource to the "rdfs:seeAlso" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource AddSeeAlso(RDFOntologyResource seeAlso) {
            if (!this.IsLiteral()) {
                 this.Annotations.SeeAlso.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.RDFS.SEE_ALSO), seeAlso));
            }
            return this;
        }

        /// <summary>
        /// Adds the given resource to the "rdfs:isDefinedBy" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource AddIsDefinedBy(RDFOntologyResource isDefinedBy) {
            if (!this.IsLiteral()) {
                 this.Annotations.IsDefinedBy.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.RDFS.IS_DEFINED_BY), isDefinedBy));
            }
            return this;
        }

        /// <summary>
        /// Adds the given custom annotation to this ontology resource
        /// </summary>
        public RDFOntologyResource AddCustomAnnotation(RDFOntologyAnnotationProperty annotationProperty, 
                                                       RDFOntologyResource annotationResource) {
            if (!this.IsLiteral()) {
                 this.Annotations.CustomAnnotations.AddEntry(new RDFOntologyTaxonomyEntry(this, annotationProperty, annotationResource));
            }
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Removes the given literal from the "owl:versionInfo" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource RemoveVersionInfo(RDFOntologyLiteral versionInfo) {
            this.Annotations.VersionInfo.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.OWL.VERSION_INFO), versionInfo));
            return this;
        }

        /// <summary>
        /// Removes the given literal from the "rdfs:comment" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource RemoveComment(RDFOntologyLiteral comment) {
            this.Annotations.Comment.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.RDFS.COMMENT), comment));
            return this;
        }

        /// <summary>
        /// Removes the given literal from the "rdfs:label" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource RemoveLabel(RDFOntologyLiteral label) {
            this.Annotations.Label.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.RDFS.LABEL), label));
            return this;
        }

        /// <summary>
        /// Removes the given resource from the "rdfs:seeAlso" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource RemoveSeeAlso(RDFOntologyResource seeAlso) {
            this.Annotations.SeeAlso.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.RDFS.SEE_ALSO), seeAlso));
            return this;
        }

        /// <summary>
        /// Removes the given resource from the "rdfs:isDefinedBy" annotations about this ontology resource
        /// </summary>
        public RDFOntologyResource RemoveIsDefinedBy(RDFOntologyResource isDefinedBy) {
            this.Annotations.IsDefinedBy.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyAnnotationProperty(RDFVocabulary.RDFS.IS_DEFINED_BY), isDefinedBy));
            return this;
        }

        /// <summary>
        /// Removes the given custom annotation from this ontology resource
        /// </summary>
        public RDFOntologyResource RemoveCustomAnnotation(RDFOntologyAnnotationProperty annotationProperty,
                                                          RDFOntologyResource annotationResource) {
            this.Annotations.CustomAnnotations.RemoveEntry(new RDFOntologyTaxonomyEntry(this, annotationProperty, annotationResource));
            return this;
        }
        #endregion

        #region Check
        /// <summary>
        /// Checks if this ontology resource represents an ontology class
        /// </summary>
        public Boolean IsClass() {
            return (this is RDFOntologyClass);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology restriction class
        /// </summary>
        public Boolean IsRestrictionClass() {
            return (this is RDFOntologyRestriction);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology composite class (union/intersection/complement)
        /// </summary>
        public Boolean IsCompositeClass() {
            return (this is RDFOntologyUnionClass        ||
                    this is RDFOntologyIntersectionClass ||
                    this is RDFOntologyComplementClass);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology enumerate class
        /// </summary>
        public Boolean IsEnumerateClass() {
            return (this is RDFOntologyEnumerateClass);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology datarange class
        /// </summary>
        public Boolean IsDataRangeClass() {
            return (this is RDFOntologyDataRangeClass);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology property
        /// </summary>
        public Boolean IsProperty() {
            return (this is RDFOntologyProperty);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology annotation property
        /// </summary>
        public Boolean IsAnnotationProperty() {
            return (this is RDFOntologyAnnotationProperty);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology datatype property
        /// </summary>
        public Boolean IsDatatypeProperty() {
            return (this is RDFOntologyDatatypeProperty);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology object property
        /// </summary>
        public Boolean IsObjectProperty() {
            return (this is RDFOntologyObjectProperty);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology fact
        /// </summary>
        public Boolean IsFact() {
            return (this is RDFOntologyFact);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology literal
        /// </summary>
        public Boolean IsLiteral() {
            return (this is RDFOntologyLiteral);
        }

        /// <summary>
        /// Checks if this ontology resource represents an ontology
        /// </summary>
        public Boolean IsOntology() {
            return (this is RDFOntology);
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology resource, eventually including semantic inferences
        /// </summary>
        public virtual RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result  = new RDFGraph();
            
            foreach(var t in this.Annotations.VersionInfo.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Annotations.Comment.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Annotations.Label.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Annotations.SeeAlso.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Annotations.IsDefinedBy.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Annotations.CustomAnnotations.ToRDFGraph(includeInferences)) { result.AddTriple(t); }

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyResourceMetadata
    /// <summary>
    /// RDFOntologyResourceMetadata represents a collector for annotation metadata describing an ontology resource.
    /// </summary>
    public class RDFOntologyResourceMetadata {

        #region Properties
        /// <summary>
        /// "owl:versionInfo" annotations about this ontology resource
        /// </summary>
        public RDFOntologyTaxonomy VersionInfo { get; internal set; }

        /// <summary>
        /// "rdfs:comment" annotations about this ontology resource
        /// </summary>
        public RDFOntologyTaxonomy Comment { get; internal set; }

        /// <summary>
        /// "rdfs:label" annotations about this ontology resource
        /// </summary>
        public RDFOntologyTaxonomy Label { get; internal set; }

        /// <summary>
        /// "rdfs:seeAlso" annotations about this ontology resource
        /// </summary>
        public RDFOntologyTaxonomy SeeAlso { get; internal set; }

        /// <summary>
        /// "rdfs:isDefinedBy" annotations about this ontology resource
        /// </summary>
        public RDFOntologyTaxonomy IsDefinedBy { get; internal set; }

        /// <summary>
        /// Custom annotations about this ontology resource
        /// </summary>
        public RDFOntologyTaxonomy CustomAnnotations { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology resource metadata
        /// </summary>
        internal RDFOntologyResourceMetadata() {
            this.VersionInfo       = new RDFOntologyTaxonomy();
            this.Comment           = new RDFOntologyTaxonomy();
            this.Label             = new RDFOntologyTaxonomy();
            this.SeeAlso           = new RDFOntologyTaxonomy();
            this.IsDefinedBy       = new RDFOntologyTaxonomy();
            this.CustomAnnotations = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}