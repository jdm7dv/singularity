﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyData represents the data component (A-BOX) of an ontology.
    /// </summary>
    public class RDFOntologyData: IEnumerable<RDFOntologyFact> {

        #region Properties
        /// <summary>
        /// Count of the facts composing the data
        /// </summary>
        public Int64 FactsCount {
            get { return this.Facts.Count; }
        }

        /// <summary>
        /// Gets the enumerator on the facts of the data for iteration
        /// </summary>
        public IEnumerator<RDFOntologyFact> FactsEnumerator {
            get { return this.Facts.Values.GetEnumerator(); }
        }

        /// <summary>
        /// Dictionary of facts composing the data
        /// </summary>
        internal Dictionary<Int64, RDFOntologyFact> Facts { get; set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology data
        /// </summary>
        public RDFOntologyData() {
            this.Facts = new Dictionary<Int64, RDFOntologyFact>();
        }
        #endregion

        #region Interfaces
        /// <summary>
        /// Exposes a typed enumerator on the data's facts
        /// </summary>
        IEnumerator<RDFOntologyFact> IEnumerable<RDFOntologyFact>.GetEnumerator() {
            return this.Facts.Values.GetEnumerator();
        }

        /// <summary>
        /// Exposes an untyped enumerator on the data's facts
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator() {
            return this.Facts.Values.GetEnumerator();
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Adds the given fact to the data
        /// </summary>
        public RDFOntologyData AddFact(RDFOntologyFact ontologyFact) {
            if (ontologyFact != null) {
                if (!this.Facts.ContainsKey(ontologyFact.PatternMemberID)) {
                     this.Facts.Add(ontologyFact.PatternMemberID, ontologyFact);
                }
            }
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Removes the given fact from the data
        /// </summary>
        public RDFOntologyData RemoveFact(RDFOntologyFact ontologyFact) {
            if (ontologyFact != null) {
                if (this.Facts.ContainsKey(ontologyFact.PatternMemberID)) {
                    this.Facts.Remove(ontologyFact.PatternMemberID);
                }
            }
            return this;
        }
        #endregion

        #region Select
        /// <summary>
        /// Selects the fact represented by the given string from the data
        /// </summary>
        public RDFOntologyFact SelectFact(String fact) {
            if (fact        != null) {
                Int64 factID = RDFModelUtilities.CreateHash(fact);
                if (this.Facts.ContainsKey(factID)) {
                    return this.Facts[factID];
                }
            }
            return null;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology data, eventually including semantic inferences
        /// </summary>
        public RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result   = new RDFGraph();
            foreach (var f  in this) {
                foreach (var t in f.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            }
            return result;
        }
        #endregion

        #endregion

    }

}