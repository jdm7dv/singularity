﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Semantic.Model;
using System.Web.Semantic.Query;
using System.Web.Semantic.Store;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFSemanticsUtilities is a collector of reusable utility methods for RDF ontology management
    /// </summary>
    public static class RDFSemanticsUtilities {

        #region FromRDFGraph
        /// <summary>
        /// Gets an ontology representation of the given graph.
        /// </summary>
        internal static RDFOntology FromRDFGraph(RDFGraph ontGraph) {
            var ontology  = new RDFOntology(new RDFResource(ontGraph.Context));
            if (ontGraph != null) {

                //Prefetch annotation properties and rdf:type taxonomies
                var versionInfo = ontGraph.SelectTriplesByPredicate(RDFVocabulary.OWL.VERSION_INFO);
                var comment     = ontGraph.SelectTriplesByPredicate(RDFVocabulary.RDFS.COMMENT);
                var label       = ontGraph.SelectTriplesByPredicate(RDFVocabulary.RDFS.LABEL);
                var seeAlso     = ontGraph.SelectTriplesByPredicate(RDFVocabulary.RDFS.SEE_ALSO);
                var isDefinedBy = ontGraph.SelectTriplesByPredicate(RDFVocabulary.RDFS.IS_DEFINED_BY);
                var rdfType     = ontGraph.SelectTriplesByPredicate(RDFVocabulary.RDF.TYPE);

                #region Ontology
                if (!ontGraph.ContainsTriple(new RDFTriple((RDFResource)ontology.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.ONTOLOGY))) {
                    var ont  = rdfType.SelectTriplesByObject(RDFVocabulary.OWL.ONTOLOGY)
                                      .FirstOrDefault();
                    if (ont != null) {
                        ontology.Value           = ont.Subject;
                        ontology.PatternMemberID = ontology.Value.PatternMemberID;
                    }
                }
                #endregion


                #region OntologyModel

                #region PropertyModel

                #region AnnotationProperty
                foreach (var ap in rdfType.SelectTriplesByObject(RDFVocabulary.OWL.ANNOTATION_PROPERTY)) {
                    var anp  = new RDFOntologyAnnotationProperty((RDFResource)ap.Subject);
                    ontology.Model.PropertyModel.AddProperty(anp);
                }
                #endregion

                #region DatatypeProperty
                foreach (var dp in rdfType.SelectTriplesByObject(RDFVocabulary.OWL.DATATYPE_PROPERTY)) {
                    var dtp  = new RDFOntologyDatatypeProperty((RDFResource)dp.Subject);
                    ontology.Model.PropertyModel.AddProperty(dtp);

                    #region FunctionalProperty
                    if (ontGraph.ContainsTriple(new RDFTriple((RDFResource)dtp.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.FUNCTIONAL_PROPERTY))) {
                        dtp.SetFunctional(true);
                    }
                    #endregion

                }
                #endregion

                #region ObjectProperty
                foreach (var op in rdfType.SelectTriplesByObject(RDFVocabulary.OWL.OBJECT_PROPERTY)) {
                    var obp = new RDFOntologyObjectProperty((RDFResource)op.Subject);
                    ontology.Model.PropertyModel.AddProperty(obp);

                    #region FunctionalProperty
                    if (ontGraph.ContainsTriple(new RDFTriple((RDFResource)obp.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.FUNCTIONAL_PROPERTY))) {
                        obp.SetFunctional(true);
                    }
                    #endregion

                    #region SymmetricProperty
                    if (ontGraph.ContainsTriple(new RDFTriple((RDFResource)obp.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.SYMMETRIC_PROPERTY))) {
                        obp.SetSymmetric(true);
                    }
                    #endregion

                    #region TransitiveProperty
                    if (ontGraph.ContainsTriple(new RDFTriple((RDFResource)obp.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.TRANSITIVE_PROPERTY))) {
                        obp.SetTransitive(true);
                    }
                    #endregion

                    #region InverseFunctionalProperty
                    if (ontGraph.ContainsTriple(new RDFTriple((RDFResource)obp.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.INVERSE_FUNCTIONAL_PROPERTY))) {
                        obp.SetInverseFunctional(true);
                    }
                    #endregion

                }
                foreach (var sp in rdfType.SelectTriplesByObject(RDFVocabulary.OWL.SYMMETRIC_PROPERTY)) {
                    var syp  = ontology.Model.PropertyModel.SelectProperty(sp.Subject.ToString());
                    if (syp == null) {
                        syp  = new RDFOntologyObjectProperty((RDFResource)sp.Subject);
                        ontology.Model.PropertyModel.AddProperty(syp);

                        #region FunctionalProperty
                        if (ontGraph.ContainsTriple(new RDFTriple((RDFResource)syp.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.FUNCTIONAL_PROPERTY))) {
                            syp.SetFunctional(true);
                        }
                        #endregion

                    }
                    ((RDFOntologyObjectProperty)syp).SetSymmetric(true);
                }
                foreach (var tp in rdfType.SelectTriplesByObject(RDFVocabulary.OWL.TRANSITIVE_PROPERTY)) {
                    var trp  = ontology.Model.PropertyModel.SelectProperty(tp.Subject.ToString());
                    if (trp == null) {
                        trp  = new RDFOntologyObjectProperty((RDFResource)tp.Subject);
                        ontology.Model.PropertyModel.AddProperty(trp);

                        #region FunctionalProperty
                        if (ontGraph.ContainsTriple(new RDFTriple((RDFResource)trp.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.FUNCTIONAL_PROPERTY))) {
                            trp.SetFunctional(true);
                        }
                        #endregion

                    }
                    ((RDFOntologyObjectProperty)trp).SetTransitive(true);
                }
                foreach (var ip in rdfType.SelectTriplesByObject(RDFVocabulary.OWL.INVERSE_FUNCTIONAL_PROPERTY)) {
                    var ifp  = ontology.Model.PropertyModel.SelectProperty(ip.Subject.ToString());
                    if (ifp == null) {
                        ifp  = new RDFOntologyObjectProperty((RDFResource)ip.Subject);
                        ontology.Model.PropertyModel.AddProperty(ifp);

                        #region FunctionalProperty
                        if (ontGraph.ContainsTriple(new RDFTriple((RDFResource)ifp.Value, RDFVocabulary.RDF.TYPE, RDFVocabulary.OWL.FUNCTIONAL_PROPERTY))) {
                            ifp.SetFunctional(true);
                        }
                        #endregion

                    }
                    ((RDFOntologyObjectProperty)ifp).SetInverseFunctional(true);
                }
                #endregion

                #region Assertions
                foreach (var p in ontology.Model.PropertyModel) {

                    #region SubPropertyOf
                    foreach(var spof in ontGraph.SelectTriplesBySubject((RDFResource)p.Value)
                                                .SelectTriplesByPredicate(RDFVocabulary.RDFS.SUB_PROPERTY_OF)) {
                        if (spof.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                            var superProp      = ontology.Model.PropertyModel.SelectProperty(spof.Object.ToString());
                            if (superProp     != null) {
                                p.AddSubPropertyOf(superProp);
                            }
                        }
                    }
                    #endregion

                    #region EquivalentProperty
                    foreach (var eqpr in ontGraph.SelectTriplesBySubject((RDFResource)p.Value)
                                                 .SelectTriplesByPredicate(RDFVocabulary.OWL.EQUIVALENT_PROPERTY)) {
                        if (eqpr.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                            var equivProp      = ontology.Model.PropertyModel.SelectProperty(eqpr.Object.ToString());
                            if (equivProp     != null) {
                                p.AddEquivalentProperty(equivProp);
                            }
                        }
                    }
                    #endregion

                    #region InverseOf
                    if (p.IsObjectProperty()) {
                        foreach (var inof in ontGraph.SelectTriplesBySubject((RDFResource)p.Value)
                                                     .SelectTriplesByPredicate(RDFVocabulary.OWL.INVERSE_OF)) {
                            if (inof.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                                var invProp        = ontology.Model.PropertyModel.SelectProperty(inof.Object.ToString());
                                if (invProp       != null && invProp.IsObjectProperty()) {
                                    ((RDFOntologyObjectProperty)p).AddInverseOf((RDFOntologyObjectProperty)invProp);
                                }
                            }
                        }
                    }
                    #endregion

                }
                #endregion

                #endregion

                #region ClassModel

                #region Class
                foreach (var c in rdfType.SelectTriplesByObject(RDFVocabulary.OWL.CLASS)) {
                    var oc   = new RDFOntologyClass((RDFResource)c.Subject);
                    ontology.Model.ClassModel.AddClass(oc);
                }
                #endregion

                #region Restriction
                foreach (var r in rdfType.SelectTriplesByObject(RDFVocabulary.OWL.RESTRICTION)) {

                    #region OnProperty
                    var op   = ontGraph.SelectTriplesBySubject((RDFResource)r.Subject)
                                       .SelectTriplesByPredicate(RDFVocabulary.OWL.ON_PROPERTY)
                                       .FirstOrDefault();
                    if (op  != null) {
                        var onProp     = ontology.Model.PropertyModel.SelectProperty(op.Object.ToString());
                        if (onProp    != null) {
                            var restr  = new RDFOntologyRestriction((RDFResource)r.Subject, onProp);
                            ontology.Model.ClassModel.AddRestriction(restr);
                        }
                    }
                    #endregion

                }
                #endregion

                #region DataRange
                foreach (var d in rdfType.SelectTriplesByObject(RDFVocabulary.OWL.DATA_RANGE)) {
                    var dr   = new RDFOntologyDataRangeClass((RDFResource)d.Subject);
                    ontology.Model.ClassModel.AddDataRange(dr);
                }
                #endregion

                #region Composite

                #region Union
                foreach (var u in ontGraph.SelectTriplesByPredicate(RDFVocabulary.OWL.UNION_OF)) {
                    if  (u.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var uc  = ontology.Model.ClassModel.SelectClass(u.Subject.ToString());
                        if (uc != null) {

                            #region ClassToUnionClass
                            if (!(uc is RDFOntologyUnionClass)) {
                                uc = new RDFOntologyUnionClass((RDFResource)u.Subject);
                                ontology.Model.ClassModel.Classes[uc.PatternMemberID] = uc;
                            }
                            #endregion

                            #region DeserializeUnionCollection
                            var nilFound   = false;
                            var itemRest   = (RDFResource)u.Object;
                            while (!nilFound) {

                                #region rdf:first
                                var first  = ontGraph.SelectTriplesBySubject(itemRest)
                                                     .SelectTriplesByPredicate(RDFVocabulary.RDF.FIRST)
                                                     .FirstOrDefault();
                                if (first != null && first.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                                    var compClass    = ontology.Model.ClassModel.SelectClass(first.Object.ToString());
                                    if (compClass   != null) {
                                        ((RDFOntologyUnionClass)uc).AddUnionOf(compClass);
                                    }

                                    #region rdf:rest
                                    var rest         = ontGraph.SelectTriplesBySubject(itemRest)
                                                               .SelectTriplesByPredicate(RDFVocabulary.RDF.REST)
                                                               .FirstOrDefault();
                                    if (rest        != null) {
                                        if (rest.Object.Equals(RDFVocabulary.RDF.NIL)) {
                                            nilFound = true;
                                        }
                                        else {
                                            itemRest = (RDFResource)rest.Object;
                                        }
                                    }
                                    #endregion

                                }
                                else {
                                    nilFound = true;
                                }
                                #endregion

                            }
                            #endregion

                        }
                    }                    
                }
                #endregion

                #region Intersection
                foreach (var i in ontGraph.SelectTriplesByPredicate(RDFVocabulary.OWL.INTERSECTION_OF)) {
                    if  (i.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var ic  = ontology.Model.ClassModel.SelectClass(i.Subject.ToString());
                        if (ic != null) {

                            #region ClassToIntersectionClass
                            if (!(ic is RDFOntologyIntersectionClass)) {
                                ic = new RDFOntologyIntersectionClass((RDFResource)i.Subject);
                                ontology.Model.ClassModel.Classes[ic.PatternMemberID] = ic;
                            }
                            #endregion

                            #region DeserializeIntersectionCollection
                            var nilFound   = false;
                            var itemRest   = (RDFResource)i.Object;
                            while (!nilFound) {

                                #region rdf:first
                                var first  = ontGraph.SelectTriplesBySubject(itemRest)
                                                     .SelectTriplesByPredicate(RDFVocabulary.RDF.FIRST)
                                                     .FirstOrDefault();
                                if (first != null   && first.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                                    var compClass    = ontology.Model.ClassModel.SelectClass(first.Object.ToString());
                                    if (compClass   != null) {
                                        ((RDFOntologyIntersectionClass)ic).AddIntersectionOf(compClass);
                                    }

                                    #region rdf:rest
                                    var rest         = ontGraph.SelectTriplesBySubject(itemRest)
                                                               .SelectTriplesByPredicate(RDFVocabulary.RDF.REST)
                                                               .FirstOrDefault();
                                    if (rest        != null) {
                                        if (rest.Object.Equals(RDFVocabulary.RDF.NIL)) {
                                            nilFound = true;
                                        }
                                        else {
                                            itemRest = (RDFResource)rest.Object;
                                        }
                                    }
                                    #endregion

                                }
                                else {
                                    nilFound = true;
                                }
                                #endregion

                            }
                            #endregion

                        }
                    }
                }
                #endregion

                #region Complement
                foreach (var c in ontGraph.SelectTriplesByPredicate(RDFVocabulary.OWL.COMPLEMENT_OF)) {
                    if (c.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) { 
                        var cc  = ontology.Model.ClassModel.SelectClass(c.Subject.ToString());
                        if (cc != null) {
                            var compClass  = ontology.Model.ClassModel.SelectClass(c.Object.ToString());
                            if (compClass != null) {
                                cc         = new RDFOntologyComplementClass((RDFResource)c.Subject, compClass);
                                ontology.Model.ClassModel.Classes[cc.PatternMemberID] = cc;
                            }
                        }
                    }
                }
                #endregion

                #endregion

                #region Assertions
                foreach (var c in ontology.Model.ClassModel) {

                    #region SubClassOf
                    foreach (var scof in ontGraph.SelectTriplesBySubject((RDFResource)c.Value)
                                                 .SelectTriplesByPredicate(RDFVocabulary.RDFS.SUB_CLASS_OF)) {
                        if  (scof.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                            var superClass       = ontology.Model.ClassModel.SelectClass(scof.Object.ToString());
                            if (superClass      != null) {
                                c.AddSubClassOf(superClass);
                            }
                        }
                    }
                    #endregion

                    #region EquivalentClass
                    foreach (var eqcl in ontGraph.SelectTriplesBySubject((RDFResource)c.Value)
                                                 .SelectTriplesByPredicate(RDFVocabulary.OWL.EQUIVALENT_CLASS)) {
                        if  (eqcl.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                            var equivClass       = ontology.Model.ClassModel.SelectClass(eqcl.Object.ToString());
                            if (equivClass      != null) {
                                c.AddEquivalentClass(equivClass);
                            }
                        }
                    }
                    #endregion

                    #region DisjointWith
                    foreach (var djwt in ontGraph.SelectTriplesBySubject((RDFResource)c.Value)
                                                 .SelectTriplesByPredicate(RDFVocabulary.OWL.DISJOINT_WITH)) {
                        if  (djwt.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                            var disjWith         = ontology.Model.ClassModel.SelectClass(djwt.Object.ToString());
                            if (disjWith        != null) {
                                c.AddDisjointWith(disjWith);
                            }
                        }
                    }
                    #endregion

                }
                #endregion

                #endregion

                #endregion


                #region OntologyData

                #region Fact
                foreach (var c  in ontology.Model.ClassModel) {
                    foreach (var t in rdfType.SelectTriplesByObject((RDFResource)c.Value)) {
                        var f    = ontology.Data.SelectFact(t.Subject.ToString());
                        if (f   == null) {
                            f    = new RDFOntologyFact((RDFResource)t.Subject);
                            ontology.Data.AddFact(f);
                        }
                        f.AddClassType(c);
                    }
                }
                #endregion

                #region Assertions

                #region SameAs
                foreach (var t in ontGraph.SelectTriplesByPredicate(RDFVocabulary.OWL.SAME_AS)) {
                    if  (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var subjFct       = ontology.Data.SelectFact(t.Subject.ToString());
                        if (subjFct      != null) {
                            var objFct    = ontology.Data.SelectFact(t.Object.ToString());
                            if (objFct   != null) {
                                subjFct.AddSameAs(objFct);
                            }
                        }
                    }
                }
                #endregion

                #region DifferentFrom
                foreach (var t in ontGraph.SelectTriplesByPredicate(RDFVocabulary.OWL.DIFFERENT_FROM)) {
                    if  (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var subjFct       = ontology.Data.SelectFact(t.Subject.ToString());
                        if (subjFct      != null) {
                            var objFct    = ontology.Data.SelectFact(t.Object.ToString());
                            if (objFct   != null) {
                                subjFct.AddDifferentFrom(objFct);
                            }
                        }
                    }
                }

                #endregion

                #region Attribute
                foreach (var p in ontology.Model.PropertyModel) {
                    foreach (var t in ontGraph.SelectTriplesByPredicate((RDFResource)p.Value)) {
                        var subjFct  = ontology.Data.SelectFact(t.Subject.ToString());

                        //Reject attribute if subjFact is not found in ontology data
                        if (subjFct != null) {
                            if (p.IsObjectProperty() && t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                                var objFct  = ontology.Data.SelectFact(t.Object.ToString());

                                //Reject attribute if objFact is not found in ontology data, since classtype would be unknown
                                if (objFct != null) {
                                    subjFct.AddAttribute((RDFOntologyObjectProperty)p, objFct);
                                }

                            }
                            else {
                                if (p.IsDatatypeProperty() && t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                                    subjFct.AddAttribute((RDFOntologyDatatypeProperty)p, new RDFOntologyLiteral((RDFLiteral)t.Object));
                                }
                            }
                        }

                    }
                }
                #endregion

                #endregion

                #endregion


                #region Finalization

                #region Restriction
                var restrictions = ontology.Model.ClassModel.Where(c => c.IsRestrictionClass()).ToList();
                foreach (var   r in restrictions) {

                    #region Cardinality
                    Int32 exC = 0;
                    var  crEx = ontGraph.SelectTriplesBySubject((RDFResource)r.Value)
                                         .SelectTriplesByPredicate(RDFVocabulary.OWL.CARDINALITY)
                                         .FirstOrDefault();
                    if (crEx != null    && crEx.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                        if (crEx.Object is RDFPlainLiteral) {
                            if (Regex.IsMatch(crEx.Object.ToString(), @"^[0-9]+$")) {
                                exC      = Int32.Parse(crEx.Object.ToString());
                            }
                        }
                        else {
                            if (((RDFTypedLiteral)crEx.Object).Datatype.Category == RDFModelEnums.RDFDatatypeCategory.Numeric) {
                                if (Regex.IsMatch(((RDFTypedLiteral)crEx.Object).Value, @"^[0-9]+$")) {
                                    exC  = Int32.Parse(((RDFTypedLiteral)crEx.Object).Value);
                                }
                            }
                        }
                    }
                    if (exC > 0) {
                        var cardRestr    = new RDFOntologyCardinalityRestriction((RDFResource)r.Value, ((RDFOntologyRestriction)r).OnProperty, exC, exC);
                        ontology.Model.ClassModel.Classes[r.PatternMemberID] = cardRestr;
                        continue;
                    }

                    Int32 minC = 0;
                    var  crMin = ontGraph.SelectTriplesBySubject((RDFResource)r.Value)
                                         .SelectTriplesByPredicate(RDFVocabulary.OWL.MIN_CARDINALITY)
                                         .FirstOrDefault();
                    if (crMin != null   && crMin.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                        if (crMin.Object is RDFPlainLiteral) {
                            if (Regex.IsMatch(crMin.Object.ToString(), @"^[0-9]+$")) {
                                minC     = Int32.Parse(crMin.Object.ToString());
                            }
                        }
                        else {
                            if (((RDFTypedLiteral)crMin.Object).Datatype.Category == RDFModelEnums.RDFDatatypeCategory.Numeric) {
                                if (Regex.IsMatch(((RDFTypedLiteral)crMin.Object).Value, @"^[0-9]+$")) {
                                    minC = Int32.Parse(((RDFTypedLiteral)crMin.Object).Value);
                                }
                            }
                        }
                    }

                    Int32 maxC = 0;
                    var  crMax = ontGraph.SelectTriplesBySubject((RDFResource)r.Value)
                                         .SelectTriplesByPredicate(RDFVocabulary.OWL.MAX_CARDINALITY)
                                         .FirstOrDefault();
                    if (crMax != null   && crMax.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                        if (crMax.Object is RDFPlainLiteral) {
                            if (Regex.IsMatch(crMax.Object.ToString(), @"^[0-9]+$")) {
                                maxC     = Int32.Parse(crMax.Object.ToString());
                            }
                        }
                        else {
                            if (((RDFTypedLiteral)crMax.Object).Datatype.Category == RDFModelEnums.RDFDatatypeCategory.Numeric) {
                                if (Regex.IsMatch(((RDFTypedLiteral)crMax.Object).Value, @"^[0-9]+$")) {
                                    maxC = Int32.Parse(((RDFTypedLiteral)crMax.Object).Value);
                                }
                            }
                        }
                    }
                    if (minC > 0 || maxC > 0) {
                        var cardRestr    = new RDFOntologyCardinalityRestriction((RDFResource)r.Value, ((RDFOntologyRestriction)r).OnProperty, minC, maxC);
                        ontology.Model.ClassModel.Classes[r.PatternMemberID] = cardRestr;
                        continue;
                    }
                    #endregion

                    #region HasValue
                    var hvRes  = ontGraph.SelectTriplesBySubject((RDFResource)r.Value)
                                         .SelectTriplesByPredicate(RDFVocabulary.OWL.HAS_VALUE)
                                         .FirstOrDefault();
                    if (hvRes != null) {
                        if (hvRes.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                            var hvFct    = ontology.Data.SelectFact(hvRes.Object.ToString());
                            if (hvFct   != null) {
                                var hasvalueRestr = new RDFOntologyHasValueRestriction((RDFResource)r.Value, ((RDFOntologyRestriction)r).OnProperty, hvFct);
                                ontology.Model.ClassModel.Classes[r.PatternMemberID] = hasvalueRestr;
                                continue;
                            }
                        }
                        else {
                            var hasvalueRestr     = new RDFOntologyHasValueRestriction((RDFResource)r.Value, ((RDFOntologyRestriction)r).OnProperty, new RDFOntologyLiteral((RDFLiteral)hvRes.Object));
                            ontology.Model.ClassModel.Classes[r.PatternMemberID] = hasvalueRestr;
                            continue;
                        }
                    }
                    #endregion

                    #region AllValuesFrom
                    var avfRes  = ontGraph.SelectTriplesBySubject((RDFResource)r.Value)
                                          .SelectTriplesByPredicate(RDFVocabulary.OWL.ALL_VALUES_FROM)
                                          .FirstOrDefault();
                    if (avfRes != null   && avfRes.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var avfCls  = ontology.Model.ClassModel.SelectClass(avfRes.Object.ToString());
                        if (avfCls != null) {
                            var allvaluesfromRestr = new RDFOntologyAllValuesFromRestriction((RDFResource)r.Value, ((RDFOntologyRestriction)r).OnProperty, avfCls);
                            ontology.Model.ClassModel.Classes[r.PatternMemberID] = allvaluesfromRestr;
                            continue;
                        }
                    }
                    #endregion

                    #region SomeValuesFrom
                    var svfRes  = ontGraph.SelectTriplesBySubject((RDFResource)r.Value)
                                          .SelectTriplesByPredicate(RDFVocabulary.OWL.SOME_VALUES_FROM)
                                          .FirstOrDefault();
                    if (svfRes != null   && svfRes.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var svfCls  = ontology.Model.ClassModel.SelectClass(svfRes.Object.ToString());
                        if (svfCls != null) {
                            var somevaluesfromRestr = new RDFOntologySomeValuesFromRestriction((RDFResource)r.Value, ((RDFOntologyRestriction)r).OnProperty, svfCls);
                            ontology.Model.ClassModel.Classes[r.PatternMemberID] = somevaluesfromRestr;
                            continue;
                        }
                    }
                    #endregion

                }
                #endregion

                #region Enumerate
                foreach (var e in ontGraph.SelectTriplesByPredicate(RDFVocabulary.OWL.ONE_OF)) {
                    if  (e.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var ec  = ontology.Model.ClassModel.SelectClass(e.Subject.ToString());
                        if (ec != null && !ec.IsDataRangeClass()) {

                            #region ClassToEnumerateClass
                            if (!ec.IsEnumerateClass()) {
                                ec = new RDFOntologyEnumerateClass((RDFResource)e.Subject);
                                ontology.Model.ClassModel.Classes[ec.PatternMemberID] = ec;
                            }
                            #endregion

                            #region DeserializeEnumerateCollection
                            var nilFound   = false;
                            var itemRest   = (RDFResource)e.Object;
                            while (!nilFound) {

                                #region rdf:first
                                var first  = ontGraph.SelectTriplesBySubject(itemRest)
                                                     .SelectTriplesByPredicate(RDFVocabulary.RDF.FIRST)
                                                     .FirstOrDefault();
                                if (first != null   && first.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                                    var enumMember   = ontology.Data.SelectFact(first.Object.ToString());
                                    if (enumMember  != null) {
                                        ((RDFOntologyEnumerateClass)ec).AddOneOf(enumMember);
                                    }

                                    #region rdf:rest
                                    var rest         = ontGraph.SelectTriplesBySubject(itemRest)
                                                               .SelectTriplesByPredicate(RDFVocabulary.RDF.REST)
                                                               .FirstOrDefault();
                                    if (rest        != null) {
                                        if (rest.Object.Equals(RDFVocabulary.RDF.NIL)) {
                                            nilFound = true;
                                        }
                                        else {
                                            itemRest = (RDFResource)rest.Object;
                                        }
                                    }
                                    #endregion

                                }
                                else {
                                    nilFound = true;
                                }
                                #endregion

                            }
                            #endregion

                        }
                    }
                }
                #endregion

                #region DataRange
                foreach (var d in ontGraph.SelectTriplesByPredicate(RDFVocabulary.OWL.ONE_OF)) {
                    if  (d.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var dr  = ontology.Model.ClassModel.SelectClass(d.Subject.ToString());
                        if (dr != null && !dr.IsEnumerateClass()) {

                            #region ClassToDataRangeClass
                            if (!dr.IsDataRangeClass()) {
                                dr = new RDFOntologyDataRangeClass((RDFResource)d.Subject);
                                ontology.Model.ClassModel.Classes[dr.PatternMemberID] = dr;
                            }
                            #endregion

                            #region DeserializeDataRangeCollection
                            var nilFound   = false;
                            var itemRest   = (RDFResource)d.Object;
                            while (!nilFound) {

                                #region rdf:first
                                var first  = ontGraph.SelectTriplesBySubject(itemRest)
                                                     .SelectTriplesByPredicate(RDFVocabulary.RDF.FIRST)
                                                     .FirstOrDefault();
                                if (first != null && first.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                                    ((RDFOntologyDataRangeClass)dr).AddOneOf(new RDFOntologyLiteral((RDFLiteral)first.Object));

                                    #region rdf:rest
                                    var rest         = ontGraph.SelectTriplesBySubject(itemRest)
                                                               .SelectTriplesByPredicate(RDFVocabulary.RDF.REST)
                                                               .FirstOrDefault();
                                    if (rest != null) {
                                        if (rest.Object.Equals(RDFVocabulary.RDF.NIL)) {
                                            nilFound = true;
                                        }
                                        else {
                                            itemRest = (RDFResource)rest.Object;
                                        }
                                    }
                                    #endregion

                                }
                                else {
                                    nilFound = true;
                                }
                                #endregion

                            }
                            #endregion

                        }
                    }
                }
                #endregion

                #region Domain/Range
                foreach (var p in ontology.Model.PropertyModel) {

                    #region Domain
                    var d  = ontGraph.SelectTriplesBySubject((RDFResource)p.Value)
                                     .SelectTriplesByPredicate(RDFVocabulary.RDFS.DOMAIN)
                                     .FirstOrDefault();
                    if (d != null && d.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var domainClass  = ontology.Model.ClassModel.SelectClass(d.Object.ToString());
                        if (domainClass != null) {
                            p.SetDomain(domainClass);
                        }
                    }
                    #endregion

                    #region Range
                    var r  = ontGraph.SelectTriplesBySubject((RDFResource)p.Value)
                                     .SelectTriplesByPredicate(RDFVocabulary.RDFS.RANGE)
                                     .FirstOrDefault();
                    if (r != null && r.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPO) {
                        var rangeClass  = ontology.Model.ClassModel.SelectClass(r.Object.ToString());
                        if (rangeClass != null) {
                            p.SetRange(rangeClass);
                        }
                    }
                    #endregion

                }
                #endregion

                #region Annotations

                #region Ontology

                #region VersionInfo
                foreach (var t in versionInfo.SelectTriplesBySubject((RDFResource)ontology.Value)) {
                    if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                        ontology.AddVersionInfo(new RDFOntologyLiteral((RDFLiteral)t.Object));
                    }
                }
                #endregion

                #region Comment
                foreach (var t in comment.SelectTriplesBySubject((RDFResource)ontology.Value)) {
                    if (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPL) {
                        ontology.AddComment(new RDFOntologyLiteral((RDFLiteral)t.Object));
                    }
                }
                #endregion

                #region Label
                foreach (var t in label.SelectTriplesBySubject((RDFResource)ontology.Value)) {
                    if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                        ontology.AddLabel(new RDFOntologyLiteral((RDFLiteral)t.Object));
                    }
                }
                #endregion

                #region SeeAlso
                foreach (var t in seeAlso.SelectTriplesBySubject((RDFResource)ontology.Value)) {
                    if (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                        ontology.AddSeeAlso(new RDFOntologyLiteral((RDFLiteral)t.Object));
                    }
                    else {
                        RDFOntologyResource resource = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                        if (resource         == null) {
                            resource          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                            if (resource     == null) {
                                resource      = ontology.Data.SelectFact(t.Object.ToString());
                                if (resource == null) {
                                    resource                 = new RDFOntologyResource();
                                    resource.Value           = t.Object;
                                    resource.PatternMemberID = t.Object.PatternMemberID;
                                }
                            }
                        }
                        ontology.AddSeeAlso(resource);
                    }
                }
                #endregion

                #region IsDefinedBy
                foreach (var t in isDefinedBy.SelectTriplesBySubject((RDFResource)ontology.Value)) {
                    if (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                        ontology.AddIsDefinedBy(new RDFOntologyLiteral((RDFLiteral)t.Object));
                    }
                    else {
                        RDFOntologyResource isDefBy = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                        if (isDefBy         == null) {
                            isDefBy          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                            if (isDefBy     == null) {
                                isDefBy      = ontology.Data.SelectFact(t.Object.ToString());
                                if (isDefBy == null) {
                                    isDefBy                 = new RDFOntologyResource();
                                    isDefBy.Value           = t.Object;
                                    isDefBy.PatternMemberID = t.Object.PatternMemberID;
                                }
                            }
                        }
                        ontology.AddIsDefinedBy(isDefBy);
                    }
                }
                #endregion

                #region Imports
                foreach (var t in ontGraph.SelectTriplesBySubject((RDFResource)ontology.Value)
                                          .SelectTriplesByPredicate(RDFVocabulary.OWL.IMPORTS)) {
                    if  (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                        ontology.AddImports(new RDFOntology((RDFResource)t.Object));
                    }
                }
                #endregion

                #region BackwardCompatibleWith
                foreach (var t in ontGraph.SelectTriplesBySubject((RDFResource)ontology.Value)
                                          .SelectTriplesByPredicate(RDFVocabulary.OWL.BACKWARD_COMPATIBLE_WITH)) {
                    if  (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                        ontology.AddBackwardCompatibleWith(new RDFOntology((RDFResource)t.Object));
                    }
                }
                #endregion

                #region IncompatibleWith
                foreach (var t in ontGraph.SelectTriplesBySubject((RDFResource)ontology.Value)
                                          .SelectTriplesByPredicate(RDFVocabulary.OWL.INCOMPATIBLE_WITH)) {
                    if  (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPO) {
                        ontology.AddIncompatibleWith(new RDFOntology((RDFResource)t.Object));
                    }
                }
                #endregion

                #region PriorVersion
                foreach (var t in ontGraph.SelectTriplesBySubject((RDFResource)ontology.Value)
                                          .SelectTriplesByPredicate(RDFVocabulary.OWL.PRIOR_VERSION)) {
                    if (t.TripleFlavor   == RDFModelEnums.RDFTripleFlavors.SPL) {
                        ontology.AddPriorVersion(new RDFOntologyLiteral((RDFLiteral)t.Object));
                    }
                }
                #endregion

                #region CustomAnnotations
                var annotProps   = ontology.Model.PropertyModel.Where(p => p.IsAnnotationProperty());
                foreach (var ap in annotProps) {
                    foreach (var t in ontGraph.SelectTriplesBySubject((RDFResource)ontology.Value)
                                              .SelectTriplesByPredicate((RDFResource)ap.Value)) {
                        if  (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPL) {
                            ontology.AddCustomAnnotation((RDFOntologyAnnotationProperty)ap, new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                        else {
                            RDFOntologyResource custAnn = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                            if (custAnn         == null) {
                                custAnn          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                if (custAnn     == null) {
                                    custAnn      = ontology.Data.SelectFact(t.Object.ToString());
                                    if (custAnn == null) {
                                        custAnn                 = new RDFOntologyResource();
                                        custAnn.Value           = t.Object;
                                        custAnn.PatternMemberID = t.Object.PatternMemberID;
                                    }
                                }
                            }
                            ontology.AddCustomAnnotation((RDFOntologyAnnotationProperty)ap, custAnn);
                        }
                        
                    }
                }
                #endregion

                #endregion

                #region Classes
                foreach (var c in ontology.Model.ClassModel) {

                    #region VersionInfo
                    foreach (var t in versionInfo.SelectTriplesBySubject((RDFResource)c.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            c.AddVersionInfo(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                    }
                    #endregion

                    #region Comment
                    foreach (var t in comment.SelectTriplesBySubject((RDFResource)c.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            c.AddComment(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                    }
                    #endregion

                    #region Label
                    foreach (var t in label.SelectTriplesBySubject((RDFResource)c.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            c.AddLabel(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                    }
                    #endregion

                    #region SeeAlso
                    foreach (var t in seeAlso.SelectTriplesBySubject((RDFResource)c.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            c.AddSeeAlso(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                        else {
                            RDFOntologyResource resource = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                            if (resource         == null) {
                                resource          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                if (resource     == null) {
                                    resource      = ontology.Data.SelectFact(t.Object.ToString());
                                    if (resource == null) {
                                        resource                 = new RDFOntologyResource();
                                        resource.Value           = t.Object;
                                        resource.PatternMemberID = t.Object.PatternMemberID;
                                    }
                                }
                            }
                            c.AddSeeAlso(resource);
                        }
                    }
                    #endregion

                    #region IsDefinedBy
                    foreach (var t in isDefinedBy.SelectTriplesBySubject((RDFResource)c.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            c.AddIsDefinedBy(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                        else {
                            RDFOntologyResource isDefBy = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                            if (isDefBy         == null) {
                                isDefBy          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                if (isDefBy     == null) {
                                    isDefBy      = ontology.Data.SelectFact(t.Object.ToString());
                                    if (isDefBy == null) {
                                        isDefBy                 = new RDFOntologyResource();
                                        isDefBy.Value           = t.Object;
                                        isDefBy.PatternMemberID = t.Object.PatternMemberID;
                                    }
                                }
                            }
                            c.AddIsDefinedBy(isDefBy);
                        }
                    }
                    #endregion

                    #region CustomAnnotations
                    foreach (var ap in annotProps) {
                        foreach (var t in ontGraph.SelectTriplesBySubject((RDFResource)c.Value)
                                                  .SelectTriplesByPredicate((RDFResource)ap.Value)) {
                            if  (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPL) {
                                c.AddCustomAnnotation((RDFOntologyAnnotationProperty)ap, new RDFOntologyLiteral((RDFLiteral)t.Object));
                            }
                            else {
                                RDFOntologyResource custAnn = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                                if (custAnn         == null) {
                                    custAnn          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                    if (custAnn     == null) {
                                        custAnn      = ontology.Data.SelectFact(t.Object.ToString());
                                        if (custAnn == null) {
                                            custAnn                 = new RDFOntologyResource();
                                            custAnn.Value           = t.Object;
                                            custAnn.PatternMemberID = t.Object.PatternMemberID;
                                        }
                                    }
                                }
                                c.AddCustomAnnotation((RDFOntologyAnnotationProperty)ap, custAnn);
                            }

                        }
                    }
                    #endregion

                }
                #endregion

                #region Properties
                foreach (var p in ontology.Model.PropertyModel) {

                    #region VersionInfo
                    foreach (var t in versionInfo.SelectTriplesBySubject((RDFResource)p.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            p.AddVersionInfo(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                    }
                    #endregion

                    #region Comment
                    foreach (var t in comment.SelectTriplesBySubject((RDFResource)p.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            p.AddComment(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                    }
                    #endregion

                    #region Label
                    foreach (var t in label.SelectTriplesBySubject((RDFResource)p.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            p.AddLabel(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                    }
                    #endregion

                    #region SeeAlso
                    foreach (var t in seeAlso.SelectTriplesBySubject((RDFResource)p.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            p.AddSeeAlso(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                        else {
                            RDFOntologyResource resource = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                            if (resource         == null) {
                                resource          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                if (resource     == null) {
                                    resource      = ontology.Data.SelectFact(t.Object.ToString());
                                    if (resource == null) {
                                        resource                 = new RDFOntologyResource();
                                        resource.Value           = t.Object;
                                        resource.PatternMemberID = t.Object.PatternMemberID;
                                    }
                                }
                            }
                            p.AddSeeAlso(resource);
                        }
                    }
                    #endregion

                    #region IsDefinedBy
                    foreach (var t in isDefinedBy.SelectTriplesBySubject((RDFResource)p.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            p.AddIsDefinedBy(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                        else {
                            RDFOntologyResource isDefBy = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                            if (isDefBy         == null) {
                                isDefBy          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                if (isDefBy     == null) {
                                    isDefBy      = ontology.Data.SelectFact(t.Object.ToString());
                                    if (isDefBy == null) {
                                        isDefBy                 = new RDFOntologyResource();
                                        isDefBy.Value           = t.Object;
                                        isDefBy.PatternMemberID = t.Object.PatternMemberID;
                                    }
                                }
                            }
                            p.AddIsDefinedBy(isDefBy);
                        }
                    }
                    #endregion

                    #region CustomAnnotations
                    foreach (var ap in annotProps) {
                        foreach (var t in ontGraph.SelectTriplesBySubject((RDFResource)p.Value)
                                                  .SelectTriplesByPredicate((RDFResource)ap.Value)) {
                            if (t.TripleFlavor   == RDFModelEnums.RDFTripleFlavors.SPL) {
                                p.AddCustomAnnotation((RDFOntologyAnnotationProperty)ap, new RDFOntologyLiteral((RDFLiteral)t.Object));
                            }
                            else {
                                RDFOntologyResource custAnn = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                                if (custAnn         == null) {
                                    custAnn          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                    if (custAnn     == null) {
                                        custAnn      = ontology.Data.SelectFact(t.Object.ToString());
                                        if (custAnn == null) {
                                            custAnn                 = new RDFOntologyResource();
                                            custAnn.Value           = t.Object;
                                            custAnn.PatternMemberID = t.Object.PatternMemberID;
                                        }
                                    }
                                }
                                p.AddCustomAnnotation((RDFOntologyAnnotationProperty)ap, custAnn);
                            }

                        }
                    }
                    #endregion

                }
                #endregion

                #region Facts
                foreach (var f in ontology.Data) {

                    #region VersionInfo
                    foreach (var t in versionInfo.SelectTriplesBySubject((RDFResource)f.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                             f.AddVersionInfo(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                    }
                    #endregion

                    #region Comment
                    foreach (var t in comment.SelectTriplesBySubject((RDFResource)f.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            f.AddComment(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                    }
                    #endregion

                    #region Label
                    foreach (var t in label.SelectTriplesBySubject((RDFResource)f.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            f.AddLabel(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                    }
                    #endregion

                    #region SeeAlso
                    foreach (var t in seeAlso.SelectTriplesBySubject((RDFResource)f.Value)) {
                        if  (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPL) {
                            f.AddSeeAlso(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                        else {
                            RDFOntologyResource resource = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                            if (resource         == null) {
                                resource          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                if (resource     == null) {
                                    resource      = ontology.Data.SelectFact(t.Object.ToString());
                                    if (resource == null) {
                                        resource                 = new RDFOntologyResource();
                                        resource.Value           = t.Object;
                                        resource.PatternMemberID = t.Object.PatternMemberID;
                                    }
                                }
                            }
                            f.AddSeeAlso(resource);
                        }
                    }
                    #endregion

                    #region IsDefinedBy
                    foreach (var t in isDefinedBy.SelectTriplesBySubject((RDFResource)f.Value)) {
                        if  (t.TripleFlavor == RDFModelEnums.RDFTripleFlavors.SPL) {
                            f.AddIsDefinedBy(new RDFOntologyLiteral((RDFLiteral)t.Object));
                        }
                        else {
                            RDFOntologyResource isDefBy = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                            if (isDefBy         == null) {
                                isDefBy          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                if (isDefBy     == null) {
                                    isDefBy      = ontology.Data.SelectFact(t.Object.ToString());
                                    if (isDefBy == null) {
                                        isDefBy                 = new RDFOntologyResource();
                                        isDefBy.Value           = t.Object;
                                        isDefBy.PatternMemberID = t.Object.PatternMemberID;
                                    }
                                }
                            }
                            f.AddIsDefinedBy(isDefBy);
                        }
                    }
                    #endregion

                    #region CustomAnnotations
                    foreach (var ap in annotProps) {
                        foreach (var t in ontGraph.SelectTriplesBySubject((RDFResource)f.Value)
                                                  .SelectTriplesByPredicate((RDFResource)ap.Value)) {
                            if  (t.TripleFlavor  == RDFModelEnums.RDFTripleFlavors.SPL) {
                                f.AddCustomAnnotation((RDFOntologyAnnotationProperty)ap, new RDFOntologyLiteral((RDFLiteral)t.Object));
                            }
                            else {
                                RDFOntologyResource custAnn = ontology.Model.ClassModel.SelectClass(t.Object.ToString());
                                if (custAnn         == null) {
                                    custAnn          = ontology.Model.PropertyModel.SelectProperty(t.Object.ToString());
                                    if (custAnn     == null) {
                                        custAnn      = ontology.Data.SelectFact(t.Object.ToString());
                                        if (custAnn == null) {
                                            custAnn                 = new RDFOntologyResource();
                                            custAnn.Value           = t.Object;
                                            custAnn.PatternMemberID = t.Object.PatternMemberID;
                                        }
                                    }
                                }
                                f.AddCustomAnnotation((RDFOntologyAnnotationProperty)ap, custAnn);
                            }

                        }
                    }
                    #endregion

                }
                #endregion

                #endregion

                #endregion


            }
            return ontology;
        }
        #endregion

        #region Validation

        #region Rule:OWL_DL_Vocabulary_Completeness
        /// <summary>
        /// Validation rule checking for completeness of vocabulary of classes, properties and facts
        /// </summary>
        internal static List<RDFOntologyValidatorEvidence> OWL_DL_Vocabulary_Completeness(RDFOntology ontology) {
            var evidences = new List<RDFOntologyValidatorEvidence>();

            #region Classes
            foreach (var  c in ontology.Model.ClassModel) {

                #region SubClassOf
                foreach (var sc in c.Assertions.SubClassOf) {
                    if  (!ontology.Model.ClassModel.Classes.ContainsKey(sc.TaxonomyObject.Value.PatternMemberID)) {
                          evidences.Add(new RDFOntologyValidatorEvidence(
                              "Warning",
                              "OWL_DL_Vocabulary_Completeness",
                              String.Format("SubClassOf taxonomy of ontology class '{0}' is incomplete because linked ontology class '{1}' has not been found in the ontology class model.", sc.TaxonomySubject, sc.TaxonomyObject),
                              String.Format("Guarantee completeness of SubClassOf taxonomy by adding ontology class '{0}' to the ontology class model.", sc.TaxonomyObject)
                          ));
                    }
                }
                #endregion

                #region EquivalentClass
                foreach (var ec in c.Assertions.EquivalentClass) {
                    if  (!ontology.Model.ClassModel.Classes.ContainsKey(ec.TaxonomyObject.Value.PatternMemberID)) {
                          evidences.Add(new RDFOntologyValidatorEvidence(
                              "Warning",
                              "OWL_DL_Vocabulary_Completeness",
                              String.Format("EquivalentClass taxonomy of ontology class '{0}' is incomplete because linked ontology class '{1}' has not been found in the ontology class model.", ec.TaxonomySubject, ec.TaxonomyObject),
                              String.Format("Guarantee completeness of EquivalentClass taxonomy by adding ontology class '{0}' to the ontology class model.", ec.TaxonomyObject)
                          ));
                    }
                }
                #endregion

                #region DisjointWith
                foreach (var dw in c.Assertions.DisjointWith) {
                    if  (!ontology.Model.ClassModel.Classes.ContainsKey(dw.TaxonomyObject.Value.PatternMemberID)) {
                          evidences.Add(new RDFOntologyValidatorEvidence(
                              "Warning",
                              "OWL_DL_Vocabulary_Completeness",
                              String.Format("DisjointWith taxonomy of ontology class '{0}' is incomplete because linked ontology class '{1}' has not been found in the ontology class model.", dw.TaxonomySubject, dw.TaxonomyObject),
                              String.Format("Guarantee completeness of DisjointWith taxonomy by adding ontology class '{0}' to the ontology class model.", dw.TaxonomyObject)
                          ));
                    }
                }
                #endregion

            }
            #endregion

            return evidences;
        }
        #endregion

        #region Rule:OWL_DL_Vocabulary_Disjointness
        /// <summary>
        /// Validation rule checking for disjointness of vocabulary of classes, properties and facts
        /// </summary>
        internal static List<RDFOntologyValidatorEvidence> OWL_DL_Vocabulary_Disjointness(RDFOntology ontology) {
            var evidences = new List<RDFOntologyValidatorEvidence>();

            #region Classes -> Properties/Facts
            foreach (var  c in ontology.Model.ClassModel) {
                if  (ontology.Model.PropertyModel.Properties.ContainsKey(c.PatternMemberID)) {
                    evidences.Add(new RDFOntologyValidatorEvidence(
                        "Error",
                        "OWL_DL_Vocabulary_Disjointness",
                        String.Format("Disjointess of ontology class model and ontology property model is violated because the resource name '{0}' is associated to an ontology class but also to an ontology property.", c),
                        String.Format("Guarantee disjointess of ontology class model and ontology property model by removing, or renaming, one of the two entities.")
                    ));
                }
                if (ontology.Data.Facts.ContainsKey(c.PatternMemberID)) {
                    evidences.Add(new RDFOntologyValidatorEvidence(
                        "Error",
                        "OWL_DL_Vocabulary_Disjointness",
                        String.Format("Disjointess of ontology class model and ontology data is violated because the resource name '{0}' is associated to an ontology class but also to an ontology fact.", c),
                        String.Format("Guarantee disjointess of ontology class model and ontology data by removing, or renaming, one of the two entities.")
                    ));
                }
            }
            #endregion

            #region Properties -> Facts
            foreach (var p in ontology.Model.PropertyModel) {
                if (ontology.Data.Facts.ContainsKey(p.PatternMemberID)) {
                    evidences.Add(new RDFOntologyValidatorEvidence(
                        "Error",
                        "OWL_DL_Vocabulary_Disjointness",
                        String.Format("Disjointess of ontology property model and ontology data is violated because the resource name '{0}' is associated to an ontology property but also to an ontology fact.", p),
                        String.Format("Guarantee disjointess of ontology property model and ontology data by removing, or renaming, one of the two entities.")
                    ));
                }
            }
            #endregion

            return evidences;
        }
        #endregion

        #endregion

    }

}