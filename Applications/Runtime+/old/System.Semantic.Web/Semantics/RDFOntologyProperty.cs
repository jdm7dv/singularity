﻿/*
   Copyright 2012-2015 Marco De Salvo

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Semantic.Model;
using System.Web.Semantic.Store;
using System.Web.Semantic.Query;

namespace System.Web.Semantic.Semantics
{

    /// <summary>
    /// RDFOntologyProperty represents a property definition in the context of an ontology model.
    /// </summary>
    public class RDFOntologyProperty: RDFOntologyResource {

        #region Properties
        /// <summary>
        /// Flag indicating that this ontology property is "owl:FunctionalProperty"
        /// </summary>
        public Boolean Functional { get; internal set; }

        /// <summary>
        /// Domain class of the ontology property
        /// </summary>
        public RDFOntologyClass Domain { get; internal set; }

        /// <summary>
        /// Range class of the ontology property
        /// </summary>
        public RDFOntologyClass Range { get; internal set; }

        /// <summary>
        /// Assertion metadata of the ontology property
        /// </summary>
        public RDFOntologyPropertyMetadata Assertions { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an ontology property with the given non-blank name
        /// </summary>
        internal RDFOntologyProperty(RDFResource propertyName) {
            if (propertyName != null) {
                if (!propertyName.IsBlank) {
                    this.Value           = propertyName;
                    this.PatternMemberID = propertyName.PatternMemberID;
                    this.Assertions      = new RDFOntologyPropertyMetadata();
                }
                else {
                    throw new RDFSemanticsException("Cannot create RDFOntologyProperty because given \"propertyName\" parameter is a blank resource.");
                }
            }
            else {
                throw new RDFSemanticsException("Cannot create RDFOntologyProperty because given \"propertyName\" parameter is null.");
            }
        }
        #endregion

        #region Methods

        #region Add
        /// <summary>
        /// Sets the domain of this ontology property to the given ontology class
        /// </summary>
        public RDFOntologyProperty SetDomain(RDFOntologyClass domainClass) {
            this.Domain = domainClass;
            return this;
        }

        /// <summary>
        /// Sets the range of this ontology property to the given ontology class
        /// </summary>
        public RDFOntologyProperty SetRange(RDFOntologyClass rangeClass) {
            this.Range  = rangeClass;
            return this;
        }

        /// <summary>
        /// Sets or unsets this ontology property as "owl:FunctionalProperty"
        /// </summary>
        public RDFOntologyProperty SetFunctional(Boolean functional) {
            this.Functional = functional;
            return this;
        }

        /// <summary>
        /// Sets this ontology property as subProperty of the given one
        /// </summary>
        public RDFOntologyProperty AddSubPropertyOf(RDFOntologyProperty superProperty) {
            this.Assertions.SubPropertyOf.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.RDFS.SUB_PROPERTY_OF), superProperty));
            return this;
        }

        /// <summary>
        /// Sets this ontology property as equivalentProperty of the given one
        /// </summary>
        public RDFOntologyProperty AddEquivalentProperty(RDFOntologyProperty equivalentProperty) {
            this.Assertions.EquivalentProperty.AddEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.EQUIVALENT_PROPERTY), equivalentProperty));
            return this;
        }
        #endregion

        #region Remove
        /// <summary>
        /// Unsets this ontology property as subProperty of the given one
        /// </summary>
        public RDFOntologyProperty RemoveSubPropertyOf(RDFOntologyProperty superProperty) {
            this.Assertions.SubPropertyOf.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.RDFS.SUB_PROPERTY_OF), superProperty));
            return this;
        }

        /// <summary>
        /// Unsets this ontology property as equivalentProperty of the given one
        /// </summary>
        public RDFOntologyProperty RemoveEquivalentProperty(RDFOntologyProperty equivalentProperty) {
            this.Assertions.EquivalentProperty.RemoveEntry(new RDFOntologyTaxonomyEntry(this, new RDFOntologyObjectProperty(RDFVocabulary.OWL.EQUIVALENT_PROPERTY), equivalentProperty));
            return this;
        }
        #endregion

        #region Convert
        /// <summary>
        /// Gets a graph representation of this ontology property, eventually including semantic inferences
        /// </summary>
        public override RDFGraph ToRDFGraph(Boolean includeInferences) {
            var result  = base.ToRDFGraph(includeInferences);

            foreach(var t in this.Assertions.SubPropertyOf.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            foreach(var t in this.Assertions.EquivalentProperty.ToRDFGraph(includeInferences)) { result.AddTriple(t); }
            if (this.Functional) {
                result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDF.TYPE,    RDFVocabulary.OWL.FUNCTIONAL_PROPERTY));
            }
            if (this.Domain != null) {
                result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDFS.DOMAIN, (RDFResource)this.Domain.Value));
            }
            if (this.Range  != null) {
                result.AddTriple(new RDFTriple((RDFResource)this.Value, RDFVocabulary.RDFS.RANGE,  (RDFResource)this.Range.Value));
            }

            return result;
        }
        #endregion

        #endregion

    }

    #region RDFOntologyPropertyMetadata
    /// <summary>
    /// RDFOntologyPropertyMetadata represents a collector for assertion metadata describing an ontology property.
    /// </summary>
    public class RDFOntologyPropertyMetadata {

        #region Properties
        /// <summary>
        /// "rdfs:subPropertyOf" assertions about this ontology property
        /// </summary>
        public RDFOntologyTaxonomy SubPropertyOf { get; internal set; }

        /// <summary>
        /// "owl:equivalentProperty" assertions about this ontology property
        /// </summary>
        public RDFOntologyTaxonomy EquivalentProperty { get; internal set; }
        #endregion

        #region Ctors
        /// <summary>
        /// Default-ctor to build an empty ontology property metadata
        /// </summary>
        internal RDFOntologyPropertyMetadata() {
            this.SubPropertyOf      = new RDFOntologyTaxonomy();
            this.EquivalentProperty = new RDFOntologyTaxonomy();
        }
        #endregion

    }
    #endregion

}