
using System;

namespace System.Web.Semantic.Owl
{
	/// <summary>
	/// Represents a OWL Node of type owl:DataRange
	/// </summary>
	public interface IOwlDataRange : IOwlResource
	{

	}
}
