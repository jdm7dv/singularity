﻿// <copyright file="AssemblyInfo.cs" company="DotNetLib">
//
// .netlib: http://www.codeplex.com/DotNetLib
//
// This code is released under the New BSD License.
// A copy of this license is available at:
// http://www.codeplex.com/DotNetLib/license
//
// </copyright>

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Web.UI;

[assembly: AssemblyTitle("DotNetLib")]
[assembly: AssemblyDescription("DotNetLib")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("DotNetLib")]
[assembly: AssemblyProduct("DotNetLib")]
[assembly: AssemblyCopyright("Copyright © DotNetLib 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]

[assembly: CLSCompliant(true)]

// [assembly: Guid("58567f28-3f21-4f17-8e57-0f0d9b2ac5b8")]
[assembly: ComVisible(false)]

[assembly: WebResource("DotNetLib.Web.UI.WebControls.Validators.js", "application/x-javascript")]

[assembly: AssemblyVersion("0.0.0.5")]
[assembly: AssemblyFileVersion("0.0.0.5")]
