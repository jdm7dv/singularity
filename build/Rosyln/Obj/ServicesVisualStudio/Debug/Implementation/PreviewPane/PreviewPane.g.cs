﻿#pragma checksum "Implementation\PreviewPane\PreviewPane.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "93EA70F2A375B4E3AFD345609F6189C8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.VisualStudio.PlatformUI;
using Microsoft.VisualStudio.Shell;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Microsoft.VisualStudio.LanguageServices.Implementation.PreviewPane {
    
    
    internal partial class PreviewPane : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 64 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel HeaderStackPanel;
        
        #line default
        #line hidden
        
        
        #line 72 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TitleTextBlock;
        
        #line default
        #line hidden
        
        
        #line 73 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton ExpanderToggleButton;
        
        #line default
        #line hidden
        
        
        #line 76 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border SeverityIconBorder;
        
        #line default
        #line hidden
        
        
        #line 77 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock IdTextBlock;
        
        #line default
        #line hidden
        
        
        #line 78 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Hyperlink IdHyperlink;
        
        #line default
        #line hidden
        
        
        #line 80 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Run TitleRun;
        
        #line default
        #line hidden
        
        
        #line 85 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel DescriptionDockPanel;
        
        #line default
        #line hidden
        
        
        #line 90 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Paragraph DescriptionParagraph;
        
        #line default
        #line hidden
        
        
        #line 95 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LearnMoreTextBlock;
        
        #line default
        #line hidden
        
        
        #line 98 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Documents.Hyperlink LearnMoreHyperlink;
        
        #line default
        #line hidden
        
        
        #line 101 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Separator HeaderSeparator;
        
        #line default
        #line hidden
        
        
        #line 103 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel PreviewDockPanel;
        
        #line default
        #line hidden
        
        
        #line 104 "Implementation\PreviewPane\PreviewPane.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer PreviewScrollViewer;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Microsoft.VisualStudio.LanguageServices;V42.42.42.42;component/implementation/pr" +
                    "eviewpane/previewpane.xaml", System.UriKind.Relative);
            
            #line 1 "Implementation\PreviewPane\PreviewPane.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.HeaderStackPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.TitleTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.ExpanderToggleButton = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            
            #line 75 "Implementation\PreviewPane\PreviewPane.xaml"
            this.ExpanderToggleButton.Checked += new System.Windows.RoutedEventHandler(this.ExpanderToggleButton_CheckedChanged);
            
            #line default
            #line hidden
            
            #line 75 "Implementation\PreviewPane\PreviewPane.xaml"
            this.ExpanderToggleButton.Unchecked += new System.Windows.RoutedEventHandler(this.ExpanderToggleButton_CheckedChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.SeverityIconBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 5:
            this.IdTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.IdHyperlink = ((System.Windows.Documents.Hyperlink)(target));
            
            #line 78 "Implementation\PreviewPane\PreviewPane.xaml"
            this.IdHyperlink.RequestNavigate += new System.Windows.Navigation.RequestNavigateEventHandler(this.LearnMoreHyperlink_RequestNavigate);
            
            #line default
            #line hidden
            return;
            case 7:
            this.TitleRun = ((System.Windows.Documents.Run)(target));
            return;
            case 8:
            this.DescriptionDockPanel = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 9:
            this.DescriptionParagraph = ((System.Windows.Documents.Paragraph)(target));
            return;
            case 10:
            this.LearnMoreTextBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.LearnMoreHyperlink = ((System.Windows.Documents.Hyperlink)(target));
            
            #line 98 "Implementation\PreviewPane\PreviewPane.xaml"
            this.LearnMoreHyperlink.RequestNavigate += new System.Windows.Navigation.RequestNavigateEventHandler(this.LearnMoreHyperlink_RequestNavigate);
            
            #line default
            #line hidden
            return;
            case 12:
            this.HeaderSeparator = ((System.Windows.Controls.Separator)(target));
            return;
            case 13:
            this.PreviewDockPanel = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 14:
            this.PreviewScrollViewer = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

